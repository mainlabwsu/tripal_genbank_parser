<?php

namespace Drupal\tripal_genbank_parser\Fetcher;

/**
 * Class GBFetcher
 *
 * The entry point for the fetcher. The GBFetcher invokes the NCBI
 * E-utilities to download the Genbank files
 */
class NCBIFetcher {
  public $eutils = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils";
  /**
   *
   * @param string $keyword, the keyword used for search.
   * @param string $output, the output file. If not provided, only count the matched records.
   * @param string $days, days since record modified.
   * @param string $period, date ranges since record modified.
   * @param string $db, NCBI databases.
   * @param string $report, result format
   * @param number $max, max limit for the reocrds to be downloaded
   * @return array <'count', 'download'> indicating the number of record matched and the number record downloaded.
   */
  public function eSearch($keyword, $output = NULL, $days = NULL, $period = NULL, $db = 'nucleotide', $report = 'gb', $max = 0) {

    // use esearch util
    $util_esearch = $this->eutils . '/esearch.fcgi?';

    // create a new query
    $query = "$util_esearch" . "db=$db&retmax=0&usehistory=y";

    // set limits for dates if provided
    if ($days) {
      $query .= "&datetype=pdat&reldate=$days";
    }
    else if ($period) {
      $dates = explode(':', $period);
      $query .= "&datetype=pdat&mindate=$dates[0]&maxdate=$dates[1]";
    }

    // set keyword
    $query .= "&term=" . urlencode($keyword);
    // send query to NCBI
    $fh = fopen($query, "r");
    $esearch_result = '';
    while (! feof($fh)) {
      $esearch_result .= fread($fh, 255);
    }
    fclose($fh);

    // parse Count/QueryKey/WebEnv for eFetch
    preg_match('/<Count>(\d+)<\/Count>.*<QueryKey>(\d+)<\/QueryKey>.*<WebEnv>(\S+)<\/WebEnv>/', $esearch_result, $matches);
    $count = $matches [1]? $matches [1] : 0;
    $downloaded = 0;
    $queryKey = $matches [2];
    $webEnv = $matches [3];

    // fetch the sequences if -c argument is not set
    if ($output) {
      if ($max) {
        $this->limitedFetch($output, $db, $count, $queryKey, $webEnv, $report, $max);
      }
      else {
        $this->eFetch($output, $db, $count, $queryKey, $webEnv, $report);
      }
      $downloaded = $max ? $max < $count ? $max : $count : $count;
    }
    return array ('count' => $count, 'download' => $downloaded);
  }

  // For limited record download
  private function limitedFetch($outfile, $db, $count, $queryKey, $webEnv, $report, $max) {
    // report
    $retmax = 500;
    $iteration = 1;
    $out = fopen($outfile, 'w') or die("Can not open file $outfile.");
    for ($retstart = 0; $retstart < $count && $iteration * $retmax <= $max; $retstart += $retmax) {
      $efetch = $this->eutils . '/efetch.fcgi?' . "rettype=$report&retmode=text&retstart=$retstart&retmax=$retmax&" . "db=$db&query_key=$queryKey&WebEnv=$webEnv";
      $fh = fopen($efetch, 'r');
      $efetch_result = '';
      while (! feof($fh)) {
        $efetch_result .= fread($fh, 255);
      }
      fwrite($out, "$efetch_result\n");
      $iteration ++;
    }
    if ($max % $retmax != 0) {
      $remining = $max - $retmax * ($iteration - 1);
      $efetch = $this->eutils . '/efetch.fcgi?' . "rettype=$report&retmode=text&retstart=$retstart&retmax=$remining&" . "db=$db&query_key=$queryKey&WebEnv=$webEnv";
      $fh = fopen($efetch, 'r');
      $efetch_result = '';
      while (! feof($fh)) {
        $efetch_result .= fread($fh, 255);
      }
      fwrite($out, "$efetch_result\n");
    }
    fclose($out);
  }

  // For unlimited download
  private function eFetch($outfile, $db, $count, $queryKey, $webEnv, $report, $retmax = 500) {

    // report
    $out = fopen($outfile, 'w') or die("Can not open file $outfile.");
    for ($retstart = 0; $retstart < $count; $retstart += $retmax) {
      if ($db == 'probe') {
        $efetch = $this->eutils . '/efetch.fcgi?' . "rettype=docsum&retmode=xml&retstart=$retstart&retmax=$retmax&" . "db=$db&query_key=$queryKey&WebEnv=$webEnv";
      }
      elseif ($report == 'unknown') {
        $efetch = $this->eutils . '/efetch.fcgi?' . "retstart=$retstart&retmax=$retmax&" . "db=$db&query_key=$queryKey&WebEnv=$webEnv";
      } else {
        $efetch = $this->eutils . '/efetch.fcgi?' . "rettype=$report&retmode=text&retstart=$retstart&retmax=$retmax&" . "db=$db&query_key=$queryKey&WebEnv=$webEnv";
      }
      $fh = fopen($efetch, 'r');
      $efetch_result = '';
      while (! feof($fh)) {
        $efetch_result .= fread($fh, 255);
      }
      fwrite($out, "$efetch_result\n");
    }
    fclose($out);
  }
}
