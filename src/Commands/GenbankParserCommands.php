<?php

namespace Drupal\tripal_genbank_parser\Commands;

use Drush\Commands\DrushCommands;

use Drupal\Core\File\FileSystemInterface;

/**
 * Drush commands
 */
class GenbankParserCommands extends DrushCommands {

  /**
   * Run Tripal GenbankParser Pipeline.
   *
   * @command tripal-genbank_parser:genbank-pipeline
   * @aliases trp-gbpipe
   * @options keyword
   *   The keyword to search the NCBI nucleotide database. e.g. 'Malus x domestica [ORGN]'.
   * @options days
   *   Days since record modified.
   * @options db
   *   The NCBI Entrez database to search against, currently supporting 'nucleotde', 'est', or 'gss' databases.
   * @options chado
   *   Load the parsing results into the Chado database.
   * @usage genbank-pipeline --keyword="Malus [ORGN]" --days=30 --db=nucleotide --chado=1
   *   Download and parse records for the organism 'Malus' in last 30 days. Load results into the Chado database thereafter.
   */
  public function runPipeline($options = ['keyword' => NULL, 'days' => NULL, 'db' => NULL, 'chado' => 0]) {
    $keyword = $options['keyword'];
    $days = $options['days'];
    $db = $options['db'];
    $chado = $options['chado'];

    if (!$keyword) {
      print "--keyword is required. Type 'drush trp-gbpipe --help' to show the usage. \n";
    }
    else if (!$db) {
      print "--db is required. Type 'drush trp-gbpipe --help' to show the usage. \n";
    }
    else {
      $file = $this->prepareFile($db);
      tripal_genbank_parser_job_download ($keyword, $days, $db, $file);
      tripal_genbank_parser_job_parse($file . ".genbank", $file);
      if ($chado) {
        tripal_genbank_parser_job_fastload ($file, $db);
      }
    }
  }

  public function prepareFile($db) {
    $dir = \Drupal::service('file_system')->realpath('public://tripal_genbank_parser/' . date('Y-m-d'));
    if (!\Drupal::service('file_system')->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
        \Drupal::messenger()->addError(t("Cannot create download directory 'tripal_genbank_parser'."));
    }
    // Use the time as the output file prefix
    $prefix = date('H-i-s');

    $file = $dir . '/' . $db .  "_" . $prefix;
    return $file;
  }

    /**
   * Start a Tripal GenbankParser Job.
   *
   * @command tripal-genbank_parser:genbank-job
   * @aliases trp-gbjob
   * @usage genbank-job
   *   Start the oldest job with 'Created' status from the job queue
   */
  public function runJob($options = []) {
    $database = \Drupal::database();
    $sql = "SELECT job_id, num_records, keyword, days, db, status, load, timestamp, uid, path
      FROM tripal_genbank_jobs WHERE status = 1 ORDER BY job_id
      LIMIT 1
      ";
    $job = $database->query($sql, array())->fetchObject();

    if ($job) {
      $job_id = $job->job_id;
      $keyword = $job->keyword;
      $days = $job->days;
      $db = $job->db;
      $chado = $job->load;

      print "Starting Job...\n";
      print "  Job ID = " . $job_id . "\n";
      print "  Keyword = " . $keyword . "\n";
      print "  Days = " . $days . "\n";
      print "  EntrezDB = " . $db . "\n";

      $file = $this->prepareFile($db);
      tripal_genbank_parser_job_set_path($job_id, $file);

      tripal_genbank_parser_job_download ($keyword, $days, $db, $file);
      tripal_genbank_parser_job_set_status($job_id, 2);
      tripal_genbank_parser_job_parse($file . ".genbank", $file);
      tripal_genbank_parser_job_set_status($job_id, 3);
      if ($chado) {
        tripal_genbank_parser_job_fastload ($file, $db);
        tripal_genbank_parser_job_set_status($job_id, 4);
      }
    }
    else {
      print "No job to run.\n";
    }

  }

  /**
   * Load data for a Tripal GenbankParser Job.
   *
   * @command tripal-genbank_parser:genbank-load
   * @aliases trp-gbload
   * @options job_id
   *   For loading data for a job that has been downloaded and parsed, specify the job_id.
   * @options force
   *   Force load the data even if it was loaded before.
   * @options only
   *   Only load specific type of data. Allowed values are 'locus', 'generic', 'gene', 'mrna', 'cds', 'protein', and 'misc'.
   * @usage genbank-load --job_id=1
   *   Load the data for job with job_id = 1
   */
  public function loadJob($options = ['job_id' => NULL, 'force' => NULL, 'only' => NULL]) {
    $job_id = $options['job_id'];
    $force = $options['force'];
    $only = $options['only'];
    $sql_status = '';
    if ($force != 1) {
      $sql_status = 'AND status = 3';
    }
    $database = \Drupal::database();
    $sql = "SELECT job_id, num_records, keyword, days, db, status, load, timestamp, uid, path
      FROM tripal_genbank_jobs WHERE job_id = :job_id $sql_status
      ";
    $job = $database->query($sql, array(':job_id' => $job_id))->fetchObject();

    if ($job) {

      print "[Job Summary]\n";
      print "  Job ID = " . $job_id . "\n";
      print "  Keyword = " . $job->keyword . "\n";
      print "  Days = " . $job->days . "\n";
      print "  EntrezDB = " . $job->db . "\n";
      $file = $job->path;
      if ($file) {
        tripal_genbank_parser_job_fastload ($file, $job->db, $only);
        tripal_genbank_parser_job_set_status($job_id, 4);
      }
      else {
        print "[Error:] File path missing for job_id = " . $job_id . "\n.";
      }
    }
    else {
      print "No data to load. Make sure the job is already downloaded, parsed, but not already loaded.\n";
    }

  }

}