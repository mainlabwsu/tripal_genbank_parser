<?php

namespace Drupal\tripal_genbank_parser\Loader;

use Drupal\tripal_genbank_parser\Parser\Format\Header;

use Drupal\tripal_genbank_parser\Loader\Fast\FastLocusLoader;
use Drupal\tripal_genbank_parser\Loader\Fast\FastGenericGeneLoader;
use Drupal\tripal_genbank_parser\Loader\Fast\FastGeneLoader;
use Drupal\tripal_genbank_parser\Loader\Fast\FastMRNALoader;
use Drupal\tripal_genbank_parser\Loader\Fast\FastCDSLoader;
use Drupal\tripal_genbank_parser\Loader\Fast\FastProteinLoader;
use Drupal\tripal_genbank_parser\Loader\Fast\FastMiscFeatureLoader;

class FastLoader {
  public function main ($file_prefix, $db, $only = NULL) {
    // Prepare return values
    $exit = new \stdClass();

    // Read parsing results
    $header = new Header();
    $locus = $file_prefix . $header->files->locus;
    $generic = $file_prefix . $header->files->generic;
    $gene = $file_prefix . $header->files->gene;
    $mrna = $file_prefix . $header->files->mrna;
    $cds = $file_prefix . $header->files->cds;
    $protein = $file_prefix . $header->files->protein;
    $misc = $file_prefix . $header->files->misc;

    // Load into Chado
    if (($only == NULL || $only == 'locus') && file_exists($locus)) {
      $loader = new FastLocusLoader ($locus, $db);
      $exit->Locus = $loader->loadData();
    }
    // Stop loading if the db type is not nucleotide
    if ($db != 'nucleotide') {
      return $exit;
    }
    if (($only == NULL || $only == 'generic') && file_exists($generic)) {
      $loader = new FastGenericGeneLoader ($generic);
      $exit->Generic = $loader->loadData();
    }

    if (($only == NULL || $only == 'gene') && file_exists($gene)) {
      $loader = new FastGeneLoader ($gene);
      $exit->Gene = $loader->loadData();
    }

    if (($only == NULL || $only == 'mrna') && file_exists($mrna)) {
      $loader = new FastMRNALoader ($mrna);
      $exit->mRNA = $loader->loadData();
    }

    if (($only == NULL || $only == 'cds') && file_exists($cds)) {
      $loader = new FastCDSLoader ($cds);
      $exit->CDS = $loader->loadData();
    }

    if (($only == NULL || $only == 'protein') && file_exists($protein)) {
      $loader = new FastProteinLoader ($protein);
      $exit->Protein = $loader->loadData();
    }

    if (($only == NULL || $only == 'misc') && file_exists($misc)) {
      $loader = new FastMiscFeatureLoader ($misc);
      $exit->Misc = $loader->loadData();
    }

    return $exit;
  }
}