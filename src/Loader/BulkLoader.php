<?php

namespace Drupal\tripal_genbank_parser\Loader;

class BulkLoader {
  public $template;
  public $file;

  public function loadBulkData() {
    $input = $this->file;
    $template = $this->template;
    // ensure no timeout
    set_time_limit(0);

    print "Template: " . $template->name . "\n";

    $handle = fopen($input, "r");
    $total_lines = 0;
    while (! feof($handle)) {
      $line = fgets($handle);
      $total_lines ++;
    }

    fclose($handle);

    print "File: " . $input . " (" . $total_lines . " lines)\n";

    print "Preparing to load...\n";
    $loaded_without_errors = TRUE;

    // Generate default values array
    $default_data = array ();
    $field2column = array ();
    $record2priority = array ();
    $tables = array ();
    $template_array = $template->template_array;

    // first build the record2priority array
    foreach ($template_array as $priority => $record_array) {
      $record2priority [$record_array ['record_id']] = $priority;
    }

    //
    foreach ($template_array as $priority => $record_array) {
      if (! is_array($record_array)) {
        continue;
      }

      // Add tables being inserted into to a list to be treated differently
      // this is used to acquire locks on these tables
      if (preg_match('/insert/', $record_array ['mode'])) {
        $tables [$record_array ['table']] = $record_array ['table'];
      }

      // iterate through each of the fiels for the current record and
      // set the default_data array
      foreach ($record_array ['fields'] as $field_index => $field_array) {

        $default_data [$priority] ['table'] = $record_array ['table'];
        $default_data [$priority] ['mode'] = ($record_array ['mode']) ? $record_array ['mode'] : 'insert';
        $default_data [$priority] ['select_if_duplicate'] = ($record_array ['select_if_duplicate']) ? $record_array ['select_if_duplicate'] : 0;
        $default_data [$priority] ['update_if_duplicate'] = ($record_array ['update_if_duplicate']) ? $record_array ['update_if_duplicate'] : 0;
        $default_data [$priority] ['disabled'] = ($record_array ['disable']) ? $record_array ['disable'] : 0;
        $default_data [$priority] ['optional'] = ($record_array ['optional']) ? $record_array ['optional'] : 0;
        $default_data [$priority] ['select_optional'] = ($record_array ['select_optional']) ? $record_array ['select_optional'] : 0;
        $default_data [$priority] ['record_id'] = $record_array ['record_id'];
        $default_data [$priority] ['required'] [$field_array ['field']] = $field_array ['required'];

        $one = $default_data [$priority];
        if (isset($field_array ['regex'])) {
          $default_data [$priority] ['regex_transform'] [$field_array ['field']] = $field_array ['regex'];
        }

        $two = $default_data [$priority];

        if (preg_match('/table field/', $field_array ['type'])) {
          $default_data [$priority] ['values_array'] [$field_array ['field']] = '';
          $default_data [$priority] ['need_further_processing'] = TRUE;
          $field2column [$priority] [$field_array ['field']] = $field_array ['spreadsheet column'];
        }
        elseif (preg_match('/constant/', $field_array ['type'])) {
          $default_data [$priority] ['values_array'] [$field_array ['field']] = $field_array ['constant value'];
        }
        elseif (preg_match('/foreign key/', $field_array ['type'])) {
          $default_data [$priority] ['values_array'] [$field_array ['field']] = array ();
          $default_data [$priority] ['need_further_processing'] = TRUE;
          $default_data [$priority] ['values_array'] [$field_array ['field']] ['foreign record'] ['record'] = $field_array ['foreign key'];

          // Add in the FK / Referral table
          $fk_priority = $record2priority [$field_array ['foreign key']];
          $fk_table = $template_array [$fk_priority] ['table'];
          $default_data [$priority] ['values_array'] [$field_array ['field']] ['foreign record'] ['table'] = $fk_table;

          // Add in the FK / Referral field
          // for backwards compatibility we need to get the FK relationship to find
          // out what field we're joining on. For templates created using a
          // previous version it was assumed that the FK field was always the field to join
          if (! array_key_exists('foreign field', $field_array)) {
            $tbl_description = chado_get_schema($record_array ['table']);
            foreach ($tbl_description ['foreign keys'] as $key_table => $key_array) {
              if ($key_table == $fk_table) {
                foreach ($key_array ['columns'] as $left_field => $right_field) {
                  if ($left_field == $field_array ['field']) {
                    $field_array ['foreign field'] = $right_field;
                  }
                }
              }
            }
          }
          $default_data [$priority] ['values_array'] [$field_array ['field']] ['foreign record'] ['field'] = $field_array ['foreign field'];
        }
        else {
          print 'WARNING: Unsupported type: ' . $field_array ['type'] . ' for ' . $table . '.' . $field_array ['field'] . "!\n";
        }
        $three = $default_data [$priority];
      } // end of foreach field
    } // end of foreach record

    // /////////////////////////////////////////////
    // For each set of constants
    // /////////////////////////////////////////////
    //print "Loading...\n";
    $original_default_data = $default_data;
    $group_index = 0;
    $const = array(array());
    $total_num_groups = sizeof($const);
    foreach ($const as $group_id => $set) {
      // revert default data array for next set of constants
      $default_data = $original_default_data;
      $group_index ++;

      // Add constants
      if (! empty($set)) {
        print "Constants:\n";
        foreach ($set as $priority => $record) {
          foreach ($record as $field_id => $field) {

            print "\t- " . $field ['chado_table'] . '.' . $field ['chado_field'] . ' = ' . $field ['value'] . "\n";

            if ($default_data [$priority] ['table'] == $field ['chado_table']) {
              if (isset($default_data [$priority] ['values_array'] [$field ['chado_field']])) {
                if (isset($field2column [$priority] [$field ['chado_field']])) {
                  $field2column [$priority] [$field ['chado_field']] = $field ['value'];
                }
                else {
                  $default_data [$priority] ['values_array'] [$field ['chado_field']] = $field ['value'];
                }
              }
              else {
                print "ERROR: Template has changed after constants were assigned!\n";
                exit(1);
              }
            }
            else {
              print "ERROR: Template has changed after constants were assigned!\n";
              exit(1);
            }
          }
        }
      }

      // Open File
      print "\tPreparing to load the current constant set...\n";
      print "\t\tOpen File...\n";
      try {
        $file = new SplFileObject($input, 'r');
      }
      catch (Exception $e) {
        print "Can not open file '$input'";
        return;
      }

      // Set defaults
      $header = '';
      $file_has_header = 1;
      if (preg_match('/(t|true|1)/', $file_has_header)) {
        $file->next();
        $header = $file->current();
      }
      $num_records = 0;
      $num_lines = 0;
      $num_errors = 0;
      $interval = intval($total_lines * 0.0001);
      if ($interval == 0) {
        $interval = 1;
      }

      // Disable triggers
      $triggers_disabled = FALSE;

      print "\tLoading the current constant set...\n";
      tripal_bulk_loader_progress_bar(0, $total_lines);
      while (! $file->eof()) {
        $file->next();
        $raw_line = $file->current();
        $raw_line = trim($raw_line);
        if (empty($raw_line)) {
          continue;
        } // skips blank lines
        $line = explode("\t", $raw_line);
        $num_lines ++;

        // update the job status every 1% of lines processed for the current group
        if ($num_lines % $interval == 0) {

          // percentage of lines processed for the current group
          $group_progress = round(($num_lines / $total_lines) * 100);
          tripal_bulk_loader_progress_bar($num_lines, $total_lines);

          // percentage of lines processed for all groups
          // <previous group index> * 100 + <current group progress>
          // --------------------------------------------------------
          // <total number of groups>
          // For example, if you were in the third group of 3 constant sets
          // and had a group percentage of 50% then the job progress would be
          // (2*100 + 50%) / 3 = 250%/3 = 83%
          $job_progress = round(((($group_index - 1) * 100) + $group_progress) / $total_num_groups);
        }

        $data = $default_data;

        // iterate through each record and process the line
        $data_keys = array_keys($data);
        foreach ($data_keys as $priority) {
          $options = array (
              'field2column' => $field2column,
              'record2priority' => $record2priority,
              'line' => $line,
              'line_num' => $num_lines,
              'group_index' => $group_index,
          );

          // execute all records that are not disabled
          $no_errors = FALSE;
          if (array_key_exists($priority, $data) and array_key_exists('disabled', $data [$priority]) and $data [$priority] ['disabled'] == 0) {
            $no_errors = process_data_array_for_line($priority, $data, $default_data, $options);
          }
          else {
            // set status to true for skipped records
            $no_errors = TRUE;
          }
          $failed = FALSE;
          if (! $no_errors) {
            // Encountered an error
            $failed = TRUE;
            break;
          }
        } // end of foreach table in default data array

        if ($failed) {
          break;
        }
        else {
          // Row inserted successfully
          // Set savepoint if supplied
        }
      } // end of foreach line of file

      if ($failed) {
        $loaded_without_errors = FALSE;
        break;
      }

      tripal_bulk_loader_progress_bar($total_lines, $total_lines);
    } // end of foreach constant set

    // set the status of the job (in the node not the tripal jobs)
    if ($loaded_without_errors) {
      $status = 'Loading Completed Successfully';
    }
    else {
      $status = 'Errors Encountered';
    }
    return $status;
  }
}
