<?php

namespace Drupal\tripal_genbank_parser\Loader;

/**
 * Class CDSLoader loads CDS results into a chado database
 *
 */

use Drupal\tripal_genbank_parser\Loader\Template\CDSTemplate;

class CDSLoader extends BulkLoader{

  public function __construct($file) {
    $this->template = new CDSTemplate();
    $this->file = $file;
  }

}