<?php

namespace Drupal\tripal_genbank_parser\Loader\Template;

class CDSTemplate{
  public $name;
  public $template_array;
  public function __construct() {
    $this->name = 'tripal_genbank_parser_cds_template';
    $this->template_array = array (
        0 => array (
            'table' => 'cv',
            'record_id' => 'Sequence Ontology CV',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'Name',
                    'field' => 'name',
                    'required' => 0,
                    'constant value' => 'sequence',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'select',
            'select_if_duplicate' => 0,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        1 => array (
            'table' => 'cvterm',
            'record_id' => 'Feature Type CV Term',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'Name',
                    'field' => 'name',
                    'required' => 1,
                    'spreadsheet column' => '3',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '1',
                    'field_index' => '0'
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'CV ID',
                    'field' => 'cv_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Sequence Ontology CV',
                    'foreign field' => 'cv_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '1',
                    'field_index' => '1'
                )
            ),
            'mode' => 'select',
            'select_if_duplicate' => 0,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        2 => array (
            'table' => 'feature',
            'record_id' => 'Source Feature',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'Uniquename',
                    'field' => 'uniquename',
                    'required' => 1,
                    'spreadsheet column' => '4',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '2',
                    'field_index' => '0'
                )
            ),
            'mode' => 'select',
            'select_if_duplicate' => 0,
            'update_if_duplicate' => NULL,
            'select_optional' => 1,
            'disable' => 0,
            'optional' => 0
        ),
        3 => array (
            'table' => 'feature',
            'record_id' => 'Feature',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'Uniquename',
                    'field' => 'uniquename',
                    'required' => 1,
                    'spreadsheet column' => '1',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '2',
                    'field_index' => '0'
                ),
                1 => array (
                    'type' => 'table field',
                    'title' => 'Name',
                    'field' => 'name',
                    'required' => 1,
                    'spreadsheet column' => '2',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '2',
                    'field_index' => '1'
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'Organism ID',
                    'field' => 'organism_id',
                    'show_all_records' => 1,
                    'foreign key' => 'Source Feature',
                    'foreign field' => 'organism_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                3 => array (
                    'type' => 'foreign key',
                    'title' => 'Type ID',
                    'field' => 'type_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Feature Type CV Term',
                    'foreign field' => 'cvterm_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        ),
        4 => array (
            'table' => 'featureloc',
            'record_id' => 'Featureloc',
            'fields' => array (
                0 => array (
                    'type' => 'foreign key',
                    'title' => 'feature_id',
                    'field' => 'feature_id',
                    'required' => 1,
                    'spreadsheet column' => '',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '5',
                    'field_index' => '0',
                    'show_all_records' => 1,
                    'foreign key' => 'Feature',
                    'foreign field' => 'feature_id'
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'srcfeature_id',
                    'field' => 'srcfeature_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Source Feature',
                    'foreign field' => 'feature_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'table field',
                    'title' => 'fmin',
                    'field' => 'fmin',
                    'required' => 1,
                    'spreadsheet column' => '5',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/.*\\D+.*/'
                        ),
                        'replace' => array (
                            0 => ''
                        )
                    ),
                    'priority' => '4',
                    'field_index' => '2'
                ),
                3 => array (
                    'type' => 'table field',
                    'title' => 'fmax',
                    'field' => 'fmax',
                    'required' => 1,
                    'spreadsheet column' => '6',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/.*\\D+.*/'
                        ),
                        'replace' => array (
                            0 => ''
                        )
                    ),
                    'priority' => '4',
                    'field_index' => '3'
                ),
                4 => array (
                    'type' => 'table field',
                    'title' => 'is_fmin_partial',
                    'field' => 'is_fmin_partial',
                    'required' => 0,
                    'spreadsheet column' => '7',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/TRUE/',
                            1 => '/FALSE/'
                        ),
                        'replace' => array (
                            0 => 'TRUE',
                            1 => 'FALSE'
                        )
                    ),
                    'priority' => '4',
                    'field_index' => '4'
                ),
                5 => array (
                    'type' => 'table field',
                    'title' => 'is_fmax_partial',
                    'field' => 'is_fmax_partial',
                    'required' => 0,
                    'spreadsheet column' => '8',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/TRUE/',
                            1 => '/FALSE/'
                        ),
                        'replace' => array (
                            0 => 'TRUE',
                            1 => 'FALSE'
                        )
                    ),
                    'priority' => '4',
                    'field_index' => '5'
                ),
                6 => array (
                    'type' => 'table field',
                    'title' => 'strand',
                    'field' => 'strand',
                    'required' => 1,
                    'spreadsheet column' => '9',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '7',
                    'field_index' => '6'
                ),
                7 => array (
                    'type' => 'table field',
                    'title' => 'Phase',
                    'field' => 'phase',
                    'required' => 1,
                    'spreadsheet column' => '10',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => 0,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        ),
        5 => array (
            'table' => 'db',
            'record_id' => 'Tripal Genbank Parser DB',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'name',
                    'field' => 'name',
                    'required' => 1,
                    'constant value' => 'tripal_genbank_parser',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '5',
                    'field_index' => '0'
                )
            ),
            'mode' => 'select',
            'select_if_duplicate' => 0,
            'update_if_duplicate' => 0,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        6 => array (
            'table' => 'cv',
            'record_id' => 'Tripal Genbank Parser CV',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'name',
                    'field' => 'name',
                    'required' => 1,
                    'constant value' => 'tripal_genbank_parser',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '6',
                    'field_index' => '0'
                )
            ),
            'mode' => 'select',
            'select_if_duplicate' => 0,
            'update_if_duplicate' => 0,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        7 => array (
            'table' => 'cvterm',
            'record_id' => 'mRNA CVterm',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'name',
                    'field' => 'name',
                    'required' => 0,
                    'constant value' => 'mRNA',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'cv_id',
                    'field' => 'cv_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Sequence Ontology CV',
                    'foreign field' => 'cv_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'select',
            'select_if_duplicate' => 0,
            'update_if_duplicate' => 0,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        8 => array (
            'table' => 'feature',
            'record_id' => 'Object Feature',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'uniquename',
                    'field' => 'uniquename',
                    'required' => 1,
                    'spreadsheet column' => '11',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/^\\.$/'
                        ),
                        'replace' => array (
                            0 => ''
                        )
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'organism_id',
                    'field' => 'organism_id',
                    'show_all_records' => 1,
                    'foreign key' => 'Source Feature',
                    'foreign field' => 'organism_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'type_id',
                    'field' => 'type_id',
                    'show_all_records' => 0,
                    'foreign key' => 'mRNA CVterm',
                    'foreign field' => 'cvterm_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'select',
            'select_if_duplicate' => 0,
            'update_if_duplicate' => NULL,
            'select_optional' => 1,
            'disable' => 0,
            'optional' => 0
        ),
        9 => array (
            'table' => 'cv',
            'record_id' => 'Rel CV',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'name',
                    'field' => 'name',
                    'required' => 0,
                    'constant value' => 'relationship',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'select',
            'select_if_duplicate' => 0,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        10 => array (
            'table' => 'cvterm',
            'record_id' => 'Rel CV term',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'name',
                    'field' => 'name',
                    'required' => 1,
                    'spreadsheet column' => '12',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/^\\.$/'
                        ),
                        'replace' => array (
                            0 => ''
                        )
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'CV ID',
                    'field' => 'cv_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Rel CV',
                    'foreign field' => 'cv_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '9',
                    'field_index' => '1'
                )
            ),
            'mode' => 'select',
            'select_if_duplicate' => 0,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        11 => array (
            'table' => 'feature_relationship',
            'record_id' => 'Feature Relationship',
            'fields' => array (
                0 => array (
                    'type' => 'foreign key',
                    'title' => 'Subject ID',
                    'field' => 'subject_id',
                    'required' => 1,
                    'spreadsheet column' => '',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '13',
                    'field_index' => '0',
                    'show_all_records' => 0,
                    'foreign key' => 'Feature',
                    'foreign field' => 'feature_id'
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'Object ID',
                    'field' => 'object_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Object Feature',
                    'foreign field' => 'feature_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'Type ID',
                    'field' => 'type_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Rel CV term',
                    'foreign field' => 'cvterm_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        ),
        12 => array (
            'table' => 'db',
            'record_id' => 'Nucleotide DB',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'name',
                    'field' => 'name',
                    'required' => 1,
                    'constant value' => 'nuccore',
                    'exposed' => 0,
                    'exposed_validate' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'constant',
                    'title' => 'URL',
                    'field' => 'url',
                    'required' => 0,
                    'constant value' => 'http://www.ncbi.nlm.nih.gov/nuccore',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'constant',
                    'title' => '',
                    'field' => 'urlprefix',
                    'required' => 0,
                    'constant value' => 'http://www.ncbi.nlm.nih.gov/nuccore/',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                3 => array (
                    'type' => 'constant',
                    'title' => 'description',
                    'field' => 'description',
                    'required' => 0,
                    'constant value' => 'The Nucleotide database is a collection of sequences from several sources',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        13 => array (
            'table' => 'dbxref',
            'record_id' => 'Nucleotide DB xref',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'accession',
                    'field' => 'accession',
                    'required' => 1,
                    'spreadsheet column' => '4',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/^(.*?)-.*$/'
                        ),
                        'replace' => array (
                            0 => '\\1'
                        )
                    ),
                    'priority' => '12',
                    'field_index' => '0'
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'DB ID',
                    'field' => 'db_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Nucleotide DB',
                    'foreign field' => 'db_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '18',
                    'field_index' => '1'
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        ),
        14 => array (
            'table' => 'feature_dbxref',
            'record_id' => 'Feature Reference (nuccore)',
            'fields' => array (
                0 => array (
                    'type' => 'foreign key',
                    'title' => 'Feature ID',
                    'field' => 'feature_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Feature',
                    'foreign field' => 'feature_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'DB xref ID',
                    'field' => 'dbxref_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Nucleotide DB xref',
                    'foreign field' => 'dbxref_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        ),
        15 => array (
            'table' => 'cvterm',
            'record_id' => 'Source CV term',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'name',
                    'field' => 'name',
                    'required' => 1,
                    'constant value' => 'source',
                    'exposed' => 0,
                    'exposed_validate' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'CV ID',
                    'field' => 'cv_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Tripal Genbank Parser CV',
                    'foreign field' => 'cv_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'select',
            'select_if_duplicate' => 0,
            'update_if_duplicate' => 0,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        16 => array (
            'table' => 'featureprop',
            'record_id' => 'Feature Property (source)',
            'fields' => array (
                0 => array (
                    'type' => 'foreign key',
                    'title' => 'Feature ID',
                    'field' => 'feature_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Feature',
                    'foreign field' => 'feature_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'CV term ID',
                    'field' => 'type_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Source CV term',
                    'foreign field' => 'cvterm_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'table field',
                    'title' => 'value',
                    'field' => 'value',
                    'required' => 1,
                    'spreadsheet column' => '13',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '22',
                    'field_index' => '2'
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        ),
        17 => array (
            'table' => 'analysis',
            'record_id' => 'Analysis',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'program',
                    'field' => 'program',
                    'required' => 1,
                    'constant value' => 'Tripal Genbank Parser',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '17',
                    'field_index' => '0'
                ),
                1 => array (
                    'type' => 'constant',
                    'title' => 'programversion',
                    'field' => 'programversion',
                    'required' => 1,
                    'constant value' => '1.0',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '17',
                    'field_index' => '1'
                ),
                2 => array (
                    'type' => 'constant',
                    'title' => 'sourcename',
                    'field' => 'sourcename',
                    'required' => 1,
                    'constant value' => 'NCBI',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'select',
            'select_if_duplicate' => 0,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        18 => array (
            'table' => 'analysisfeature',
            'record_id' => 'Analysis Feature',
            'fields' => array (
                0 => array (
                    'type' => 'foreign key',
                    'title' => 'Analysis ID',
                    'field' => 'analysis_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Analysis',
                    'foreign field' => 'analysis_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'Feature ID',
                    'field' => 'feature_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Feature',
                    'foreign field' => 'feature_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        )
    );
  }
}