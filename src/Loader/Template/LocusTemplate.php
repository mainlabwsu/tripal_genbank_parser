<?php

namespace Drupal\tripal_genbank_parser\Loader\Template;

class LocusTemplate {
  public $name;
  public $template_array;
  public function __construct($db) {
    $this->name = 'tripal_genbank_parser_locus_template';
    $temp = $this->nucleotideTemplate();
    if ($db == 'nucest') {
      $est_temp = $this->nucestTemplate();
      $temp = array_merge($temp, $est_temp);
    }
    if ($db == 'nucgss') {
      $gss_temp = $this->nucgssTemplate();
      $temp = array_merge($temp, $gss_temp);
    }
    $this->template_array = $temp;
  }
  private function nucleotideTemplate() {
    $template = array (
        0 => array (
            'table' => 'db',
            'record_id' => 'Tripal Genbank Parser DB',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'name',
                    'field' => 'name',
                    'required' => 1,
                    'constant value' => 'tripal_genbank_parser',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '0',
                    'field_index' => '0'
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => 0,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        1 => array (
            'table' => 'cv',
            'record_id' => 'Tripal Genbank Parser CV',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'name',
                    'field' => 'name',
                    'required' => 0,
                    'constant value' => 'tripal_genbank_parser',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '1',
                    'field_index' => '0'
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => 0,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        2 => array (
            'table' => 'organism',
            'record_id' => 'Organism',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'Genus',
                    'field' => 'genus',
                    'required' => 0,
                    'spreadsheet column' => '5',
                    'exposed' => 0,
                    'exposed_description' => 'Genus',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/^(.*?)\\s+.*?$/'
                        ),
                        'replace' => array (
                            0 => '\\1'
                        )
                    ),
                    'priority' => '2',
                    'field_index' => '0',
                    'constant value' => '',
                    'exposed_validate' => 0
                ),
                1 => array (
                    'type' => 'table field',
                    'title' => 'Species',
                    'field' => 'species',
                    'required' => 0,
                    'spreadsheet column' => '5',
                    'exposed' => 0,
                    'exposed_description' => 'Species',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/^.*?\\s+(.*?)$/'
                        ),
                        'replace' => array (
                            0 => '\\1'
                        )
                    ),
                    'priority' => '2',
                    'field_index' => '1',
                    'constant value' => '',
                    'exposed_validate' => 0
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        3 => array (
            'table' => 'cv',
            'record_id' => 'Sequence Ontology CV',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'Feature CV',
                    'field' => 'name',
                    'required' => 0,
                    'constant value' => 'sequence',
                    'exposed' => 0,
                    'exposed_validate' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'select',
            'select_if_duplicate' => 0,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        4 => array (
            'table' => 'cvterm',
            'record_id' => 'Feature Type Term',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'Feature CV Term',
                    'field' => 'name',
                    'required' => 1,
                    'spreadsheet column' => '16',
                    'exposed' => 0,
                    'exposed_description' => 'Feature Type Name',
                    'regex' => array (
                        'pattern' => array (
                            1 => '/^GSS$/'
                        ),
                        'replace' => array (
                            1 => 'biological_region'
                        )
                    ),
                    'priority' => '4',
                    'field_index' => '0'
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'Feature Type CV',
                    'field' => 'cv_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Sequence Ontology CV',
                    'foreign field' => 'cv_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'select',
            'select_if_duplicate' => 0,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        5 => array (
            'table' => 'analysis',
            'record_id' => 'Analysis',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'Analysis Program',
                    'field' => 'program',
                    'required' => 1,
                    'constant value' => 'Tripal Genbank Parser',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '5',
                    'field_index' => '0'
                ),
                1 => array (
                    'type' => 'constant',
                    'title' => 'Program Version',
                    'field' => 'programversion',
                    'required' => 1,
                    'constant value' => '1.0',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '5',
                    'field_index' => '1'
                ),
                2 => array (
                    'type' => 'constant',
                    'title' => 'Analysis Source Name',
                    'field' => 'sourcename',
                    'required' => 1,
                    'constant value' => 'NCBI',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '5',
                    'field_index' => '4'
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        6 => array (
            'table' => 'feature',
            'record_id' => 'Feature',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'Feature',
                    'field' => 'uniquename',
                    'required' => 1,
                    'spreadsheet column' => '1',
                    'exposed' => 0,
                    'exposed_description' => 'Feature Unique Name',
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '6',
                    'field_index' => '0'
                ),
                1 => array (
                    'type' => 'table field',
                    'title' => 'Feature Name',
                    'field' => 'name',
                    'required' => 0,
                    'spreadsheet column' => '2',
                    'exposed' => 0,
                    'exposed_description' => 'Feature Name',
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '6',
                    'field_index' => '1'
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'Organism',
                    'field' => 'organism_id',
                    'foreign key' => 'Organism',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '5',
                    'field_index' => '2',
                    'show_all_records' => 0,
                    'foreign field' => 'organism_id'
                ),
                3 => array (
                    'type' => 'table field',
                    'title' => 'Residues',
                    'field' => 'residues',
                    'required' => 0,
                    'spreadsheet column' => '18',
                    'exposed' => 0,
                    'exposed_description' => 'Sequence',
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '6',
                    'field_index' => '3'
                ),
                4 => array (
                    'type' => 'foreign key',
                    'title' => 'Feature Type ID',
                    'field' => 'type_id',
                    'foreign key' => 'Feature Type Term',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '6',
                    'field_index' => '4'
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => 0,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        7 => array (
            'table' => 'analysisfeature',
            'record_id' => 'Analysis Feature',
            'fields' => array (
                0 => array (
                    'type' => 'foreign key',
                    'title' => 'Analysis ID',
                    'field' => 'analysis_id',
                    'foreign key' => 'Analysis',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'Feature ID',
                    'field' => 'feature_id',
                    'foreign key' => 'Feature',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        8 => array (
            'table' => 'dbxref',
            'record_id' => 'Keywords DB xref',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'Keywords DB xref',
                    'field' => 'accession',
                    'required' => 1,
                    'constant value' => 'keywords',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'db_id',
                    'field' => 'db_id',
                    'foreign key' => 'Tripal Genbank Parser DB',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '8',
                    'field_index' => '1',
                    'show_all_records' => 0,
                    'foreign field' => 'db_id'
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        9 => array (
            'table' => 'cvterm',
            'record_id' => 'Keywords CV term',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'Keywords CV term',
                    'field' => 'name',
                    'required' => 0,
                    'constant value' => 'keywords',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'CV featureprop_type',
                    'field' => 'cv_id',
                    'foreign key' => 'Tripal Genbank Parser CV',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '11',
                    'field_index' => '1',
                    'show_all_records' => 0,
                    'foreign field' => 'cv_id'
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'Keywords DB xref',
                    'field' => 'dbxref_id',
                    'foreign key' => 'Keywords DB xref',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        10 => array (
            'table' => 'featureprop',
            'record_id' => 'Feature Property (keywords)',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'Property Value',
                    'field' => 'value',
                    'required' => 1,
                    'spreadsheet column' => '4',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/^\\.$/'
                        ),
                        'replace' => array (
                            0 => ''
                        )
                    ),
                    'priority' => '12',
                    'field_index' => '0'
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'Keywords CV term',
                    'field' => 'type_id',
                    'foreign key' => 'Keywords CV term',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'Feature ID',
                    'field' => 'feature_id',
                    'foreign key' => 'Feature',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        ),
        11 => array (
            'table' => 'dbxref',
            'record_id' => 'Definition DBX ref',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'Definition DBX ref',
                    'field' => 'accession',
                    'required' => 1,
                    'constant value' => 'genbank_definition',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'DB ID',
                    'field' => 'db_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Tripal Genbank Parser DB',
                    'foreign field' => 'db_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '13',
                    'field_index' => '1'
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        12 => array (
            'table' => 'cvterm',
            'record_id' => 'Definition CV Term',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'name',
                    'field' => 'name',
                    'required' => 1,
                    'constant value' => 'genbank_description',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'dbxref ID',
                    'field' => 'dbxref_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Definition DBX ref',
                    'foreign field' => 'dbxref_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'CV ID',
                    'field' => 'cv_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Tripal Genbank Parser CV',
                    'foreign field' => 'cv_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '14',
                    'field_index' => '2'
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        13 => array (
            'table' => 'featureprop',
            'record_id' => 'Feature Property (genbank_definition)',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'property value',
                    'field' => 'value',
                    'required' => 1,
                    'spreadsheet column' => '3',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/^\\.$/'
                        ),
                        'replace' => array (
                            0 => ''
                        )
                    ),
                    'priority' => '15',
                    'field_index' => '0'
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'type_id',
                    'field' => 'type_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Definition CV Term',
                    'foreign field' => 'cvterm_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '13',
                    'field_index' => '1'
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'Feature ID',
                    'field' => 'feature_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Feature',
                    'foreign field' => 'feature_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        ),
        14 => array (
            'table' => 'dbxref',
            'record_id' => 'Organelle DB xref',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'accession',
                    'field' => 'accession',
                    'required' => 0,
                    'spreadsheet column' => '',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '22',
                    'field_index' => '0',
                    'constant value' => 'organelle',
                    'exposed_validate' => 0
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'DB ID',
                    'field' => 'db_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Tripal Genbank Parser DB',
                    'foreign field' => 'db_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        15 => array (
            'table' => 'cvterm',
            'record_id' => 'Organelle CV term',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'name',
                    'field' => 'name',
                    'required' => 0,
                    'spreadsheet column' => '',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '22',
                    'field_index' => '0',
                    'constant value' => 'organelle',
                    'exposed_validate' => 0
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'DB xref ID',
                    'field' => 'dbxref_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Organelle DB xref',
                    'foreign field' => 'dbxref_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'cv_id',
                    'field' => 'cv_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Tripal Genbank Parser CV',
                    'foreign field' => 'cv_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '15',
                    'field_index' => '2'
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        16 => array (
            'table' => 'featureprop',
            'record_id' => 'Feature Property (organelle)',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'name',
                    'field' => 'value',
                    'required' => 1,
                    'spreadsheet column' => '9',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/^\\.$/'
                        ),
                        'replace' => array (
                            0 => ''
                        )
                    ),
                    'priority' => '23',
                    'field_index' => '0'
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'Type ID',
                    'field' => 'type_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Organelle CV term',
                    'foreign field' => 'cvterm_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'Feature ID',
                    'field' => 'feature_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Feature',
                    'foreign field' => 'feature_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        ),
        17 => array (
            'table' => 'dbxref',
            'record_id' => 'Source DB Xref',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'accession',
                    'field' => 'accession',
                    'required' => 0,
                    'constant value' => 'source',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'DB ID',
                    'field' => 'db_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Tripal Genbank Parser DB',
                    'foreign field' => 'db_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        18 => array (
            'table' => 'cvterm',
            'record_id' => 'Source CV term',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'name',
                    'field' => 'name',
                    'required' => 0,
                    'constant value' => 'source',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'CV ID',
                    'field' => 'cv_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Tripal Genbank Parser CV',
                    'foreign field' => 'cv_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'Source DB xref ID',
                    'field' => 'dbxref_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Source DB Xref',
                    'foreign field' => 'dbxref_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        19 => array (
            'table' => 'featureprop',
            'record_id' => 'Feature Property (source)',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'value',
                    'field' => 'value',
                    'required' => 1,
                    'constant value' => '',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array (
                            0 => '/^\\.$/'
                        ),
                        'replace' => array (
                            0 => ''
                        )
                    ),
                    'priority' => '30',
                    'field_index' => '0',
                    'spreadsheet column' => '17',
                    'exposed_description' => ''
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'Feature ID',
                    'field' => 'feature_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Feature',
                    'foreign field' => 'feature_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'Source CV term',
                    'field' => 'type_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Source CV term',
                    'foreign field' => 'cvterm_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        ),
        20 => array (
            'table' => 'dbxref',
            'record_id' => 'Cultivar DB Xref',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'accession',
                    'field' => 'accession',
                    'required' => 0,
                    'constant value' => 'cultivar',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'db_id',
                    'field' => 'db_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Tripal Genbank Parser DB',
                    'foreign field' => 'db_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '17',
                    'field_index' => '2'
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => 0,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        21 => array (
            'table' => 'cvterm',
            'record_id' => 'Cultivar CV term',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'name',
                    'field' => 'name',
                    'required' => 0,
                    'constant value' => 'cultivar',
                    'exposed' => 0,
                    'exposed_validate' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'cv_id',
                    'field' => 'cv_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Tripal Genbank Parser CV',
                    'foreign field' => 'cv_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '18',
                    'field_index' => '1'
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'dbxref_id',
                    'field' => 'dbxref_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Cultivar DB Xref',
                    'foreign field' => 'dbxref_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => 0,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        22 => array (
            'table' => 'stock',
            'record_id' => 'Stock',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'stock unique name',
                    'field' => 'uniquename',
                    'required' => 1,
                    'spreadsheet column' => '10',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/^\\.$/',
                            1 => '/\\sx\\s/',
                            2 => '/\\sX\\s/'
                        ),
                        'replace' => array (
                            0 => '',
                            1 => '_x_',
                            2 => '_X_'
                        )
                    ),
                    'priority' => '20',
                    'field_index' => '0'
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'organism_id',
                    'field' => 'organism_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Organism',
                    'foreign field' => 'organism_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'type_id',
                    'field' => 'type_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Cultivar CV term',
                    'foreign field' => 'cvterm_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => 0,
            'select_optional' => 1,
            'disable' => 0,
            'optional' => 1
        ),
        23 => array (
            'table' => 'dbxref',
            'record_id' => 'Clone DB xref',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'accession',
                    'field' => 'accession',
                    'required' => 0,
                    'spreadsheet column' => '',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/^\\.$/'
                        ),
                        'replace' => array (
                            0 => ''
                        )
                    ),
                    'priority' => '27',
                    'field_index' => '0',
                    'constant value' => 'clone',
                    'exposed_validate' => 0
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'DB ID',
                    'field' => 'db_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Tripal Genbank Parser DB',
                    'foreign field' => 'db_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        24 => array (
            'table' => 'cvterm',
            'record_id' => 'Clone CV term',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'name',
                    'field' => 'name',
                    'required' => 0,
                    'constant value' => 'clone',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'CV ID',
                    'field' => 'cv_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Tripal Genbank Parser CV',
                    'foreign field' => 'cv_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'Clone DB xref ID',
                    'field' => 'dbxref_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Clone DB xref',
                    'foreign field' => 'dbxref_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        25 => array (
            'table' => 'library',
            'record_id' => 'Library',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'Library Unique name',
                    'field' => 'uniquename',
                    'required' => 1,
                    'spreadsheet column' => '12',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/^"*(LIB.*?_\\d+)+\\s+.*"*$/',
                            1 => '/^\\.$/'
                        ),
                        'replace' => array (
                            0 => '\\1',
                            1 => ''
                        )
                    ),
                    'priority' => '25',
                    'field_index' => '0'
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'organism_id',
                    'field' => 'organism_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Organism',
                    'foreign field' => 'organism_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'type_id',
                    'field' => 'type_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Clone CV term',
                    'foreign field' => 'cvterm_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => 0,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        ),
        26 => array (
            'table' => 'library_feature',
            'record_id' => 'Library Feature',
            'fields' => array (
                0 => array (
                    'type' => 'foreign key',
                    'title' => 'Feature ID',
                    'field' => 'feature_id',
                    'foreign key' => 'Feature',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '9',
                    'field_index' => '0'
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'Library ID',
                    'field' => 'library_id',
                    'foreign key' => 'Library',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '9',
                    'field_index' => '1'
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => 0,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        ),
        27 => array (
            'table' => 'featureprop',
            'record_id' => 'Feature Property (clone)',
            'fields' => array (
                0 => array (
                    'type' => 'foreign key',
                    'title' => 'Feature ID',
                    'field' => 'feature_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Feature',
                    'foreign field' => 'feature_id',
                    'required' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'table field',
                    'title' => 'value',
                    'field' => 'value',
                    'required' => 1,
                    'spreadsheet column' => '13',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/^\\.$/'
                        ),
                        'replace' => array (
                            0 => ''
                        )
                    ),
                    'priority' => '26',
                    'field_index' => '1'
                ),
                2 => array (
                    'type' => 'foreign key',
                    'title' => 'Type ID',
                    'field' => 'type_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Clone CV term',
                    'foreign field' => 'cvterm_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        ),
        28 => array (
            'table' => 'db',
            'record_id' => 'PubMed DB',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'name',
                    'field' => 'name',
                    'required' => 1,
                    'constant value' => 'PMID',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert_once',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        29 => array (
            'table' => 'dbxref',
            'record_id' => 'PubMed DB xref',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'accession',
                    'field' => 'accession',
                    'required' => 1,
                    'spreadsheet column' => '7',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array (
                            0 => '/^\\.$/',
                            1 => '/^Ref1: (\\d+)$/',
                            2 => '/^Ref1: \\d+ \\| Ref2: \\d+$/'
                        ),
                        'replace' => array (
                            0 => '',
                            1 => '\\1',
                            2 => ''
                        )
                    ),
                    'priority' => '31',
                    'field_index' => '0'
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'DB ID',
                    'field' => 'db_id',
                    'show_all_records' => 0,
                    'foreign key' => 'PubMed DB',
                    'foreign field' => 'db_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => 0,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        ),
        30 => array (
            'table' => 'pub_dbxref',
            'record_id' => 'Pub_Dbxref',
            'fields' => array (
                0 => array (
                    'type' => 'foreign key',
                    'title' => 'dbxref_id',
                    'field' => 'dbxref_id',
                    'show_all_records' => 0,
                    'foreign key' => 'PubMed DB xref',
                    'foreign field' => 'dbxref_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'select',
            'select_if_duplicate' => 0,
            'update_if_duplicate' => 0,
            'select_optional' => 1,
            'disable' => 0,
            'optional' => 0
        ),
        31 => array (
            'table' => 'feature_pub',
            'record_id' => 'Feature Pub',
            'fields' => array (
                0 => array (
                    'type' => 'foreign key',
                    'title' => 'Feature ID',
                    'field' => 'feature_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Feature',
                    'foreign field' => 'feature_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '31',
                    'field_index' => '0'
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'Pub ID',
                    'field' => 'pub_id',
                    'show_all_records' => 1,
                    'foreign key' => 'Pub_Dbxref',
                    'foreign field' => 'pub_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    ),
                    'priority' => '31',
                    'field_index' => '1'
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => 0,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 1
        )
    );
    return $template;
  }

  private function nucestTemplate() {
    $template = array (
        32 => array (
            'table' => 'db',
            'record_id' => 'NCBI EST DB',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'DB name',
                    'field' => 'name',
                    'required' => 1,
                    'constant value' => 'EST',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'constant',
                    'title' => 'Description',
                    'field' => 'description',
                    'required' => 1,
                    'constant value' => 'The EST database is a collection of short single-read transcript sequences from GenBank.',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'constant',
                    'title' => 'URL',
                    'field' => 'url',
                    'required' => 1,
                    'constant value' => 'http://www.ncbi.nlm.nih.gov/nucest/',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                3 => array (
                    'type' => 'constant',
                    'title' => '',
                    'field' => 'urlprefix',
                    'required' => 1,
                    'constant value' => 'http://www.ncbi.nlm.nih.gov/nucest/',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        33 => array (
            'table' => 'dbxref',
            'record_id' => 'NCBI EST DB xref',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'Accession',
                    'field' => 'accession',
                    'required' => 1,
                    'spreadsheet column' => '2',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => '',
                    'field' => 'db_id',
                    'show_all_records' => 0,
                    'foreign key' => 'NCBI EST DB',
                    'foreign field' => 'db_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        34 => array (
            'table' => 'feature_dbxref',
            'record_id' => 'Feature EST DB xref',
            'fields' => array (
                0 => array (
                    'type' => 'foreign key',
                    'title' => 'Feature ID',
                    'field' => 'feature_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Feature',
                    'foreign field' => 'feature_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'DB xref ID',
                    'field' => 'dbxref_id',
                    'show_all_records' => 0,
                    'foreign key' => 'NCBI EST DB xref',
                    'foreign field' => 'dbxref_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        )
    );
    return $template;
  }

  private function nucgssTemplate() {
    $template = array (
        32 => array (
            'table' => 'db',
            'record_id' => 'NCBI GSS DB',
            'fields' => array (
                0 => array (
                    'type' => 'constant',
                    'title' => 'DB name',
                    'field' => 'name',
                    'required' => 1,
                    'constant value' => 'GSS',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'constant',
                    'title' => 'Description',
                    'field' => 'description',
                    'required' => 1,
                    'constant value' => 'The GSS database is a collection of GSS sequences from GenBank.',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                2 => array (
                    'type' => 'constant',
                    'title' => 'URL',
                    'field' => 'url',
                    'required' => 1,
                    'constant value' => 'http://www.ncbi.nlm.nih.gov/nucgss/',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                3 => array (
                    'type' => 'constant',
                    'title' => '',
                    'field' => 'urlprefix',
                    'required' => 1,
                    'constant value' => 'http://www.ncbi.nlm.nih.gov/nucgss/',
                    'exposed' => 0,
                    'exposed_validate' => 0,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        33 => array (
            'table' => 'dbxref',
            'record_id' => 'NCBI GSS DB xref',
            'fields' => array (
                0 => array (
                    'type' => 'table field',
                    'title' => 'Accession',
                    'field' => 'accession',
                    'required' => 1,
                    'spreadsheet column' => '2',
                    'exposed' => 0,
                    'exposed_description' => '',
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => '',
                    'field' => 'db_id',
                    'show_all_records' => 0,
                    'foreign key' => 'NCBI GSS DB',
                    'foreign field' => 'db_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        ),
        34 => array (
            'table' => 'feature_dbxref',
            'record_id' => 'Feature GSS DB xref',
            'fields' => array (
                0 => array (
                    'type' => 'foreign key',
                    'title' => 'Feature ID',
                    'field' => 'feature_id',
                    'show_all_records' => 0,
                    'foreign key' => 'Feature',
                    'foreign field' => 'feature_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                ),
                1 => array (
                    'type' => 'foreign key',
                    'title' => 'DB xref ID',
                    'field' => 'dbxref_id',
                    'show_all_records' => 0,
                    'foreign key' => 'NCBI GSS DB xref',
                    'foreign field' => 'dbxref_id',
                    'required' => 1,
                    'regex' => array (
                        'pattern' => array ()
                    )
                )
            ),
            'mode' => 'insert',
            'select_if_duplicate' => 1,
            'update_if_duplicate' => NULL,
            'select_optional' => 0,
            'disable' => 0,
            'optional' => 0
        )
    );
    return $template;
  }
}