<?php

namespace Drupal\tripal_genbank_parser\Loader\Template;

class MiscFeatureTemplate {
  public $name;
  public $template_array;
  public function __construct() {
    $this->name = 'tripal_genbank_parser_misc_template';
    $this->template_array = array (
      0 => array (
        'table' => 'cv',
        'record_id' => 'Sequence Ontology CV',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => '',
            'field' => 'name',
            'required' => 0,
            'constant value' => 'sequence',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      1 => array (
        'table' => 'cvterm',
        'record_id' => 'Feature Type CV Term',
        'fields' => array (
          0 => array (
            'type' => 'table field',
            'title' => 'Feature type',
            'field' => 'name',
            'required' => 0,
            'spreadsheet column' => '3',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array (
                1 => '/mobile_element/',
                2 => '/polyA_signal/',
                3 => '/sig_peptide/',
                5 => '/misc_RNA/',
                7 => '/TATA_signal/',
                8 => '/primer_bind/',
                12 => '/LTR/',
                14 => '/GC_signal/',
                15 => '/rep_origin/',
                16 => '/-35_signal/',
                17 => '/-10_signal/',
                18 => '/protein_bind/',
                19 => '/assembly_gap/'
              ),
              'replace' => array (
                1 => 'mobile_genetic_element',
                2 => 'polyA_signal_sequence',
                3 => 'signal_peptide',
                5 => 'RNA',
                7 => 'TATA_box',
                8 => 'primer_binding_site',
                12 => 'long_terminal_repeat',
                14 => 'GC_rich_promoter_region',
                15 => 'origin_of_replication',
                16 => 'minus_35_signal',
                17 => 'minus_10_signal',
                18 => 'protein_binding_site',
                19 => 'gap'
              )
            ),
            'priority' => '1',
            'field_index' => '0'
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => '',
            'field' => 'cv_id',
            'show_all_records' => 1,
            'foreign key' => 'Sequence Ontology CV',
            'foreign field' => 'cv_id',
            'required' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => 0,
        'select_optional' => 1,
        'disable' => 0,
        'optional' => 0
      ),
      2 => array (
        'table' => 'cvterm',
        'record_id' => 'Region cvterm',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'Name',
            'field' => 'name',
            'required' => 0,
            'constant value' => 'region',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'Cv_id',
            'field' => 'cv_id',
            'show_all_records' => 0,
            'foreign key' => 'Sequence Ontology CV',
            'foreign field' => 'cv_id',
            'required' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'select_once',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      3 => array (
        'table' => 'feature',
        'record_id' => 'Source Feature',
        'fields' => array (
          0 => array (
            'type' => 'table field',
            'title' => 'SrcFeature uniquename',
            'field' => 'uniquename',
            'required' => 0,
            'spreadsheet column' => '4',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '2',
            'field_index' => '0'
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'Type_id',
            'field' => 'type_id',
            'show_all_records' => 0,
            'foreign key' => 'Region cvterm',
            'foreign field' => 'cvterm_id',
            'required' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      4 => array (
        'table' => 'feature',
        'record_id' => 'Feature',
        'fields' => array (
          0 => array (
            'type' => 'table field',
            'title' => 'Feature uniquename',
            'field' => 'uniquename',
            'required' => 0,
            'spreadsheet column' => '1',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'table field',
            'title' => 'Feature name',
            'field' => 'name',
            'required' => 0,
            'spreadsheet column' => '2',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array ()
            )
          ),
          4 => array (
            'type' => 'foreign key',
            'title' => '',
            'field' => 'type_id',
            'show_all_records' => 0,
            'foreign key' => 'Feature Type CV Term',
            'foreign field' => 'cvterm_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '3',
            'field_index' => '4'
          ),
          5 => array (
            'type' => 'foreign key',
            'title' => 'organism_id',
            'field' => 'organism_id',
            'show_all_records' => 1,
            'foreign key' => 'Source Feature',
            'foreign field' => 'organism_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      ),
      5 => array (
        'table' => 'featureloc',
        'record_id' => 'Featureloc',
        'fields' => array (
          1 => array (
            'type' => 'foreign key',
            'title' => 'srcfeature_id',
            'field' => 'srcfeature_id',
            'show_all_records' => 0,
            'foreign key' => 'Source Feature',
            'foreign field' => 'feature_id',
            'required' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          2 => array (
            'type' => 'table field',
            'title' => 'fmin',
            'field' => 'fmin',
            'required' => 1,
            'spreadsheet column' => '5',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array (
                0 => '/.*\\D+.*/'
              ),
              'replace' => array (
                0 => ''
              )
            ),
            'priority' => '4',
            'field_index' => '2'
          ),
          3 => array (
            'type' => 'table field',
            'title' => 'fmax',
            'field' => 'fmax',
            'required' => 1,
            'spreadsheet column' => '6',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array (
                0 => '/.*\\D+.*/'
              ),
              'replace' => array (
                0 => ''
              )
            ),
            'priority' => '4',
            'field_index' => '3'
          ),
          4 => array (
            'type' => 'table field',
            'title' => 'is_fmin_partial',
            'field' => 'is_fmin_partial',
            'required' => 0,
            'spreadsheet column' => '7',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array (
                0 => '/TRUE/',
                1 => '/FALSE/'
              ),
              'replace' => array (
                0 => 'TRUE',
                1 => 'FALSE'
              )
            ),
            'priority' => '4',
            'field_index' => '4'
          ),
          5 => array (
            'type' => 'table field',
            'title' => 'is_fmax_partial',
            'field' => 'is_fmax_partial',
            'required' => 0,
            'spreadsheet column' => '8',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array (
                2 => '/TRUE/',
                3 => '/FALSE/'
              ),
              'replace' => array (
                2 => 'TRUE',
                3 => 'FALSE'
              )
            ),
            'priority' => '4',
            'field_index' => '5'
          ),
          6 => array (
            'type' => 'table field',
            'title' => 'strand',
            'field' => 'strand',
            'required' => 0,
            'spreadsheet column' => '9',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array ()
            )
          ),
          7 => array (
            'type' => 'foreign key',
            'title' => 'feature_id',
            'field' => 'feature_id',
            'show_all_records' => 0,
            'foreign key' => 'Feature',
            'foreign field' => 'feature_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '4',
            'field_index' => '7'
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      ),
      6 => array (
        'table' => 'db',
        'record_id' => 'Tripal Genbank Parser DB',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 1,
            'constant value' => 'tripal_genbank_parser',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '5',
            'field_index' => '0'
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      7 => array (
        'table' => 'cv',
        'record_id' => 'Tripal Genbank Parser CV',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 1,
            'constant value' => 'tripal_genbank_parser',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '6',
            'field_index' => '0'
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      8 => array (
        'table' => 'dbxref',
        'record_id' => 'Genbank Note DB xref',
        'fields' => array (
          0 => array (
            'type' => 'foreign key',
            'title' => 'DB ID',
            'field' => 'db_id',
            'required' => 1,
            'spreadsheet column' => '',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '9',
            'field_index' => '0',
            'show_all_records' => 0,
            'foreign key' => 'Tripal Genbank Parser DB',
            'foreign field' => 'db_id'
          ),
          1 => array (
            'type' => 'constant',
            'title' => 'accession',
            'field' => 'accession',
            'required' => 1,
            'constant value' => 'genbank_detail',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '9',
            'field_index' => '1'
          )
        ),
        'mode' => 'insert_once',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      9 => array (
        'table' => 'cvterm',
        'record_id' => 'Genbank Note CV term',
        'fields' => array (
          0 => array (
            'type' => 'foreign key',
            'title' => '',
            'field' => 'cv_id',
            'required' => 1,
            'spreadsheet column' => '',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '11',
            'field_index' => '0',
            'show_all_records' => 0,
            'foreign key' => 'Tripal Genbank Parser CV',
            'foreign field' => 'cv_id'
          ),
          1 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 1,
            'constant value' => 'genbank_detail',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          2 => array (
            'type' => 'foreign key',
            'title' => 'DBX ref ID',
            'field' => 'dbxref_id',
            'show_all_records' => 0,
            'foreign key' => 'Genbank Note DB xref',
            'foreign field' => 'dbxref_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert_once',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      10 => array (
        'table' => 'featureprop',
        'record_id' => 'Feature Property (genbank_note)',
        'fields' => array (
          0 => array (
            'type' => 'table field',
            'title' => '',
            'field' => 'value',
            'required' => 1,
            'spreadsheet column' => '13',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array (
                0 => '/^\\.$/'
              ),
              'replace' => array (
                0 => ''
              )
            ),
            'priority' => '9',
            'field_index' => '0'
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'Type ID',
            'field' => 'type_id',
            'show_all_records' => 0,
            'foreign key' => 'Genbank Note CV term',
            'foreign field' => 'cvterm_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '12',
            'field_index' => '1'
          ),
          2 => array (
            'type' => 'foreign key',
            'title' => 'Feature ID',
            'field' => 'feature_id',
            'show_all_records' => 0,
            'foreign key' => 'Feature',
            'foreign field' => 'feature_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '9',
            'field_index' => '2'
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      ),
      11 => array (
        'table' => 'cvterm',
        'record_id' => 'Source CV term',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 1,
            'constant value' => 'source',
            'exposed' => 0,
            'exposed_validate' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'CV ID',
            'field' => 'cv_id',
            'show_all_records' => 0,
            'foreign key' => 'Tripal Genbank Parser CV',
            'foreign field' => 'cv_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      12 => array (
        'table' => 'featureprop',
        'record_id' => 'Feature Property (source)',
        'fields' => array (
          0 => array (
            'type' => 'foreign key',
            'title' => 'Feature ID',
            'field' => 'feature_id',
            'show_all_records' => 0,
            'foreign key' => 'Feature',
            'foreign field' => 'feature_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '11',
            'field_index' => '0'
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'CV term ID',
            'field' => 'type_id',
            'show_all_records' => 0,
            'foreign key' => 'Source CV term',
            'foreign field' => 'cvterm_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          2 => array (
            'type' => 'constant',
            'title' => 'value',
            'field' => 'value',
            'required' => 0,
            'constant value' => 'genbank',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      ),
      13 => array (
        'table' => 'analysis',
        'record_id' => 'Analysis',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'program',
            'field' => 'program',
            'required' => 1,
            'constant value' => 'Tripal Genbank Parser',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '12',
            'field_index' => '0'
          ),
          1 => array (
            'type' => 'constant',
            'title' => 'programversion',
            'field' => 'programversion',
            'required' => 1,
            'constant value' => '1.0',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '12',
            'field_index' => '1'
          ),
          2 => array (
            'type' => 'constant',
            'title' => 'sourcename',
            'field' => 'sourcename',
            'required' => 1,
            'constant value' => 'NCBI',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      14 => array (
        'table' => 'analysisfeature',
        'record_id' => 'AnalysisFeature',
        'fields' => array (
          0 => array (
            'type' => 'foreign key',
            'title' => 'analysis_id',
            'field' => 'analysis_id',
            'show_all_records' => 0,
            'foreign key' => 'Analysis',
            'foreign field' => 'analysis_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '14',
            'field_index' => '0'
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'feature_id',
            'field' => 'feature_id',
            'show_all_records' => 0,
            'foreign key' => 'Feature',
            'foreign field' => 'feature_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '14',
            'field_index' => '1'
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      ),
      15 => array (
        'table' => 'cvterm',
        'record_id' => 'Product CV Term',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 0,
            'constant value' => 'product',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'cv_id',
            'field' => 'cv_id',
            'show_all_records' => 0,
            'foreign key' => 'Tripal Genbank Parser CV',
            'foreign field' => 'cv_id',
            'required' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      16 => array (
        'table' => 'featureprop',
        'record_id' => 'Featureprop (product)',
        'fields' => array (
          0 => array (
            'type' => 'table field',
            'title' => 'value',
            'field' => 'value',
            'required' => 1,
            'spreadsheet column' => '11',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array (
                0 => '/^\\.$/'
              ),
              'replace' => array (
                0 => ''
              )
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'feature_id',
            'field' => 'feature_id',
            'show_all_records' => 0,
            'foreign key' => 'Feature',
            'foreign field' => 'feature_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          2 => array (
            'type' => 'foreign key',
            'title' => 'type_id',
            'field' => 'type_id',
            'show_all_records' => 0,
            'foreign key' => 'Product CV Term',
            'foreign field' => 'cvterm_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      ),
      17 => array (
        'table' => 'cvterm',
        'record_id' => 'Function CVterm',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 0,
            'constant value' => 'function',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'cv_id',
            'field' => 'cv_id',
            'show_all_records' => 0,
            'foreign key' => 'Tripal Genbank Parser CV',
            'foreign field' => 'cv_id',
            'required' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      18 => array (
        'table' => 'featureprop',
        'record_id' => 'Featureprop (function)',
        'fields' => array (
          0 => array (
            'type' => 'table field',
            'title' => 'value',
            'field' => 'value',
            'required' => 1,
            'spreadsheet column' => '12',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array (
                0 => '/^\\.$/'
              ),
              'replace' => array (
                0 => ''
              )
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'feature_id',
            'field' => 'feature_id',
            'show_all_records' => 0,
            'foreign key' => 'Feature',
            'foreign field' => 'feature_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          2 => array (
            'type' => 'foreign key',
            'title' => 'type_id',
            'field' => 'type_id',
            'show_all_records' => 0,
            'foreign key' => 'Function CVterm',
            'foreign field' => 'cvterm_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      ),
      19 => array (
        'table' => 'dbxref',
        'record_id' => 'Genbank Gene DB xref',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'accession',
            'field' => 'accession',
            'required' => 0,
            'constant value' => 'genbank_gene',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'db_id',
            'field' => 'db_id',
            'show_all_records' => 0,
            'foreign key' => 'Tripal Genbank Parser DB',
            'foreign field' => 'db_id',
            'required' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert_once',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      20 => array (
        'table' => 'cvterm',
        'record_id' => 'Genbank Gene CV term',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 0,
            'constant value' => 'genbank_gene',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'cv_id',
            'field' => 'cv_id',
            'show_all_records' => 0,
            'foreign key' => 'Tripal Genbank Parser CV',
            'foreign field' => 'cv_id',
            'required' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          2 => array (
            'type' => 'foreign key',
            'title' => 'dbxref_id',
            'field' => 'dbxref_id',
            'show_all_records' => 0,
            'foreign key' => 'Genbank Gene DB xref',
            'foreign field' => 'dbxref_id',
            'required' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert_once',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      21 => array (
        'table' => 'featureprop',
        'record_id' => 'Featureprop (genbank_gene)',
        'fields' => array (
          0 => array (
            'type' => 'table field',
            'title' => 'value',
            'field' => 'value',
            'required' => 1,
            'spreadsheet column' => '10',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array (
                0 => '/^\\.$/'
              ),
              'replace' => array (
                0 => ''
              )
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'type_id',
            'field' => 'type_id',
            'show_all_records' => 0,
            'foreign key' => 'Genbank Gene CV term',
            'foreign field' => 'cvterm_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          2 => array (
            'type' => 'foreign key',
            'title' => 'feature_id',
            'field' => 'feature_id',
            'show_all_records' => 0,
            'foreign key' => 'Feature',
            'foreign field' => 'feature_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      )
    );
  }
}