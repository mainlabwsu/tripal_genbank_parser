<?php

namespace Drupal\tripal_genbank_parser\Loader\Template;

class GenericGeneTemplate {
  public $name;
  public $template_array;
  public function __construct() {
    $this->name = 'tripal_genbank_parser_generic_gene_template';
    $this->template_array = array (
  0 => array (
    'table' => 'cv',
    'record_id' => 'Sequence Ontology',
    'fields' => array (
      0 => array (
        'type' => 'constant',
        'title' => 'CV name',
        'field' => 'name',
        'required' => 0,
        'constant value' => 'sequence',
        'exposed' => 0,
        'exposed_validate' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
    ),
    'mode' => 'select',
    'select_if_duplicate' => 0,
    'update_if_duplicate' => NULL,
    'select_optional' => 0,
    'disable' => 0,
    'optional' => 0,
  ),
  1 => array (
    'table' => 'cvterm',
    'record_id' => 'Gene CV Term',
    'fields' => array (
      0 => array (
        'type' => 'constant',
        'title' => 'Name',
        'field' => 'name',
        'required' => 0,
        'constant value' => 'gene',
        'exposed' => 0,
        'exposed_validate' => 1,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
      1 => array (
        'type' => 'foreign key',
        'title' => 'CV ID',
        'field' => 'cv_id',
        'foreign key' => 'Sequence Ontology',
        'required' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
    ),
    'mode' => 'select',
    'select_if_duplicate' => 0,
    'update_if_duplicate' => NULL,
    'select_optional' => 0,
    'disable' => 0,
    'optional' => 0,
  ),
  2 => array (
    'table' => 'organism',
    'record_id' => 'Organism',
    'fields' => array (
      1 => array (
        'type' => 'table field',
        'title' => 'Species',
        'field' => 'species',
        'required' => 0,
        'constant value' => '',
        'exposed' => 0,
        'exposed_validate' => 0,
        'regex' => array (
          'pattern' => array (
            0 => '/^.*?\\s+(.*?)$/',
          ),
          'replace' => array (
            0 => '\\1',
          ),
        ),
        'priority' => '2',
        'field_index' => '1',
        'spreadsheet column' => '4',
        'exposed_description' => '',
      ),
      2 => array (
        'type' => 'table field',
        'title' => 'Genus',
        'field' => 'genus',
        'required' => 0,
        'constant value' => '',
        'exposed' => 0,
        'exposed_validate' => 0,
        'regex' => array (
          'pattern' => array (
            0 => '/^(.*?)\\s+.*?$/',
          ),
          'replace' => array (
            0 => '\\1',
          ),
        ),
        'priority' => '2',
        'field_index' => '2',
        'spreadsheet column' => '4',
        'exposed_description' => '',
      ),
    ),
    'mode' => 'insert',
    'select_if_duplicate' => 1,
    'update_if_duplicate' => 0,
    'select_optional' => 0,
    'disable' => 0,
    'optional' => 0,
  ),
  3 => array (
    'table' => 'feature',
    'record_id' => 'Feature',
    'fields' => array (
      0 => array (
        'type' => 'table field',
        'title' => 'Feature uniquename',
        'field' => 'uniquename',
        'required' => 1,
        'spreadsheet column' => '1',
        'exposed' => 0,
        'exposed_description' => '',
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
      1 => array (
        'type' => 'table field',
        'title' => 'Feature name',
        'field' => 'name',
        'required' => 0,
        'spreadsheet column' => '2',
        'exposed' => 0,
        'exposed_description' => '',
        'regex' => array (
          'pattern' => array (
          ),
        ),
        'priority' => '3',
        'field_index' => '1',
      ),
      2 => array (
        'type' => 'foreign key',
        'title' => 'Feature type_id',
        'field' => 'type_id',
        'foreign key' => 'Gene CV Term',
        'required' => 1,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
      3 => array (
        'type' => 'foreign key',
        'title' => 'organism_id',
        'field' => 'organism_id',
        'foreign key' => 'Organism',
        'required' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
    ),
    'mode' => 'insert',
    'select_if_duplicate' => 1,
    'update_if_duplicate' => NULL,
    'select_optional' => 0,
    'disable' => 0,
    'optional' => 0,
  ),
  4 => array (
    'table' => 'analysis',
    'record_id' => 'Analysis',
    'fields' => array (
      0 => array (
        'type' => 'constant',
        'title' => 'program',
        'field' => 'program',
        'required' => 1,
        'constant value' => 'Tripal Genbank Parser',
        'exposed' => 0,
        'exposed_validate' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
        'priority' => '8',
        'field_index' => '2',
      ),
      1 => array (
        'type' => 'constant',
        'title' => 'programversion',
        'field' => 'programversion',
        'required' => 1,
        'constant value' => '1.0',
        'exposed' => 0,
        'exposed_validate' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
        'priority' => '4',
        'field_index' => '3',
      ),
      2 => array (
        'type' => 'constant',
        'title' => 'sourcename',
        'field' => 'sourcename',
        'required' => 1,
        'constant value' => 'Tripal Genbank Parser',
        'exposed' => 0,
        'exposed_validate' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
        'priority' => '8',
        'field_index' => '4',
      ),
    ),
    'mode' => 'insert',
    'select_if_duplicate' => 1,
    'update_if_duplicate' => 0,
    'select_optional' => 0,
    'disable' => 0,
    'optional' => 0,
  ),
  5 => array (
    'table' => 'analysisfeature',
    'record_id' => 'Analysis Feature',
    'fields' => array (
      0 => array (
        'type' => 'foreign key',
        'title' => 'Analysis ID',
        'field' => 'analysis_id',
        'show_all_records' => 0,
        'foreign key' => 'Analysis',
        'foreign field' => 'analysis_id',
        'required' => 1,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
      1 => array (
        'type' => 'foreign key',
        'title' => 'Feature ID',
        'field' => 'feature_id',
        'show_all_records' => 0,
        'foreign key' => 'Feature',
        'foreign field' => 'feature_id',
        'required' => 1,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
    ),
    'mode' => 'insert',
    'select_if_duplicate' => 1,
    'update_if_duplicate' => NULL,
    'select_optional' => 0,
    'disable' => 0,
    'optional' => 1,
  ),
  6 => array (
    'table' => 'db',
    'record_id' => 'Tripal Genbank Parser DB',
    'fields' => array (
      0 => array (
        'type' => 'constant',
        'title' => 'name',
        'field' => 'name',
        'required' => 0,
        'constant value' => 'tripal_genbank_parser',
        'exposed' => 0,
        'exposed_validate' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
        'priority' => '6',
        'field_index' => '0',
      ),
    ),
    'mode' => 'insert',
    'select_if_duplicate' => 1,
    'update_if_duplicate' => 0,
    'select_optional' => 0,
    'disable' => 0,
    'optional' => 0,
  ),
  7 => array (
    'table' => 'dbxref',
    'record_id' => 'Source DB Xref',
    'fields' => array (
      0 => array (
        'type' => 'constant',
        'title' => 'Source DB Xref',
        'field' => 'accession',
        'required' => 0,
        'constant value' => 'source',
        'exposed' => 0,
        'exposed_validate' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
      1 => array (
        'type' => 'foreign key',
        'title' => 'db_id',
        'field' => 'db_id',
        'show_all_records' => 0,
        'foreign key' => 'Tripal Genbank Parser DB',
        'foreign field' => 'db_id',
        'required' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
    ),
    'mode' => 'insert',
    'select_if_duplicate' => 1,
    'update_if_duplicate' => 0,
    'select_optional' => 0,
    'disable' => 0,
    'optional' => 0,
  ),
  8 => array (
    'table' => 'cv',
    'record_id' => 'Tripal Genbank Parser CV',
    'fields' => array (
      0 => array (
        'type' => 'constant',
        'title' => 'name',
        'field' => 'name',
        'required' => 0,
        'constant value' => 'tripal_genbank_parser',
        'exposed' => 0,
        'exposed_validate' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
        'priority' => '8',
        'field_index' => '0',
      ),
    ),
    'mode' => 'insert',
    'select_if_duplicate' => 1,
    'update_if_duplicate' => 0,
    'select_optional' => 0,
    'disable' => 0,
    'optional' => 0,
  ),
  9 => array (
    'table' => 'cvterm',
    'record_id' => 'Source CV term',
    'fields' => array (
      0 => array (
        'type' => 'constant',
        'title' => 'name',
        'field' => 'name',
        'required' => 0,
        'constant value' => 'source',
        'exposed' => 0,
        'exposed_validate' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
        'priority' => '9',
        'field_index' => '0',
      ),
      1 => array (
        'type' => 'foreign key',
        'title' => 'cv_id',
        'field' => 'cv_id',
        'show_all_records' => 0,
        'foreign key' => 'Tripal Genbank Parser CV',
        'foreign field' => 'cv_id',
        'required' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
      2 => array (
        'type' => 'foreign key',
        'title' => 'dbxref_id',
        'field' => 'dbxref_id',
        'show_all_records' => 0,
        'foreign key' => 'Source DB Xref',
        'foreign field' => 'dbxref_id',
        'required' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
    ),
    'mode' => 'insert',
    'select_if_duplicate' => 1,
    'update_if_duplicate' => 0,
    'select_optional' => 0,
    'disable' => 0,
    'optional' => 0,
  ),
  10 => array (
    'table' => 'featureprop',
    'record_id' => 'Featureprop (source)',
    'fields' => array (
      0 => array (
        'type' => 'table field',
        'title' => 'Featureprop (source)',
        'field' => 'value',
        'required' => 0,
        'spreadsheet column' => '5',
        'exposed' => 0,
        'exposed_description' => '',
        'regex' => array (
          'pattern' => array (
          ),
        ),
        'priority' => '13',
        'field_index' => '0',
      ),
      1 => array (
        'type' => 'foreign key',
        'title' => 'Feature ID',
        'field' => 'feature_id',
        'show_all_records' => 0,
        'foreign key' => 'Feature',
        'foreign field' => 'feature_id',
        'required' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
      2 => array (
        'type' => 'foreign key',
        'title' => 'Type ID',
        'field' => 'type_id',
        'show_all_records' => 0,
        'foreign key' => 'Source CV term',
        'foreign field' => 'cvterm_id',
        'required' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
    ),
    'mode' => 'insert',
    'select_if_duplicate' => 1,
    'update_if_duplicate' => 0,
    'select_optional' => 0,
    'disable' => 0,
    'optional' => 0,
  ),
  12 => array (
    'table' => 'cvterm',
    'record_id' => 'evidence_for_feature Cvterm',
    'fields' => array (
      0 => array (
        'type' => 'constant',
        'title' => 'name',
        'field' => 'name',
        'required' => 0,
        'constant value' => 'evidence_for_feature',
        'exposed' => 0,
        'exposed_validate' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
      1 => array (
        'type' => 'foreign key',
        'title' => 'cv_id',
        'field' => 'cv_id',
        'show_all_records' => 0,
        'foreign key' => 'Sequence Ontology',
        'foreign field' => 'cv_id',
        'required' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
    ),
    'mode' => 'select_once',
    'select_if_duplicate' => 0,
    'update_if_duplicate' => 0,
    'select_optional' => 0,
    'disable' => 0,
    'optional' => 0,
  ),
  13 => array (
    'table' => 'featureprop',
    'record_id' => 'imported from NCBI Featureprop',
    'fields' => array (
      0 => array (
        'type' => 'constant',
        'title' => 'value',
        'field' => 'value',
        'required' => 0,
        'constant value' => 'imported from NCBI',
        'exposed' => 0,
        'exposed_validate' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
      1 => array (
        'type' => 'foreign key',
        'title' => 'feature_id',
        'field' => 'feature_id',
        'show_all_records' => 0,
        'foreign key' => 'Feature',
        'foreign field' => 'feature_id',
        'required' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
      2 => array (
        'type' => 'foreign key',
        'title' => 'type_id',
        'field' => 'type_id',
        'show_all_records' => 0,
        'foreign key' => 'evidence_for_feature Cvterm',
        'foreign field' => 'cvterm_id',
        'required' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
      3 => array (
        'type' => 'constant',
        'title' => 'rank',
        'field' => 'rank',
        'required' => 0,
        'constant value' => '5',
        'exposed' => 0,
        'exposed_validate' => 0,
        'regex' => array (
          'pattern' => array (
          ),
        ),
      ),
    ),
    'mode' => 'insert',
    'select_if_duplicate' => 1,
    'update_if_duplicate' => 0,
    'select_optional' => 0,
    'disable' => 0,
    'optional' => 1,
  ),
);
  }
}