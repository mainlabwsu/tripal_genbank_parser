<?php

namespace Drupal\tripal_genbank_parser\Loader\Template;

class ProteinTemplate {
  public $name;
  public $template_array;
  public function __construct() {
    $this->name = 'tripal_genbank_parser_protein_template';
    $this->template_array = array (
      0 => array (
        'table' => 'cv',
        'record_id' => 'Sequence Ontology CV',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 0,
            'constant value' => 'sequence',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      1 => array (
        'table' => 'cvterm',
        'record_id' => 'Feature Type CV Term',
        'fields' => array (
          0 => array (
            'type' => 'table field',
            'title' => 'Name',
            'field' => 'name',
            'required' => 1,
            'spreadsheet column' => '3',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '1',
            'field_index' => '0'
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'CV ID',
            'field' => 'cv_id',
            'show_all_records' => 0,
            'foreign key' => 'Sequence Ontology CV',
            'foreign field' => 'cv_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '1',
            'field_index' => '1'
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      2 => array (
        'table' => 'feature',
        'record_id' => 'Source Feature',
        'fields' => array (
          0 => array (
            'type' => 'table field',
            'title' => 'Uniquename',
            'field' => 'uniquename',
            'required' => 1,
            'spreadsheet column' => '8',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '2',
            'field_index' => '0'
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => NULL,
        'select_optional' => 1,
        'disable' => 0,
        'optional' => 0
      ),
      3 => array (
        'table' => 'feature',
        'record_id' => 'Feature',
        'fields' => array (
          0 => array (
            'type' => 'table field',
            'title' => 'Uniquename',
            'field' => 'uniquename',
            'required' => 1,
            'spreadsheet column' => '1',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '2',
            'field_index' => '0'
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'Organism ID',
            'field' => 'organism_id',
            'show_all_records' => 1,
            'foreign key' => 'Source Feature',
            'foreign field' => 'organism_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          2 => array (
            'type' => 'foreign key',
            'title' => 'Type ID',
            'field' => 'type_id',
            'show_all_records' => 0,
            'foreign key' => 'Feature Type CV Term',
            'foreign field' => 'cvterm_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          3 => array (
            'type' => 'table field',
            'title' => 'Name',
            'field' => 'name',
            'required' => 1,
            'spreadsheet column' => '2',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array ()
            )
          ),
          4 => array (
            'type' => 'table field',
            'title' => 'Residues',
            'field' => 'residues',
            'required' => 0,
            'spreadsheet column' => '12',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '3',
            'field_index' => '4'
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      ),
      4 => array (
        'table' => 'db',
        'record_id' => 'Protein DB',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 1,
            'constant value' => 'GI',
            'exposed' => 0,
            'exposed_validate' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '4',
            'field_index' => '0'
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      5 => array (
        'table' => 'dbxref',
        'record_id' => 'Protein DB xref',
        'fields' => array (
          0 => array (
            'type' => 'table field',
            'title' => 'accession',
            'field' => 'accession',
            'required' => 1,
            'spreadsheet column' => '4',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array (
                0 => '/^\\.$/'
              ),
              'replace' => array (
                0 => ''
              )
            ),
            'priority' => '5',
            'field_index' => '0'
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'DB ID',
            'field' => 'db_id',
            'show_all_records' => 0,
            'foreign key' => 'Protein DB',
            'foreign field' => 'db_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '5',
            'field_index' => '1'
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      6 => array (
        'table' => 'feature_dbxref',
        'record_id' => 'Feature Protein Reference',
        'fields' => array (
          0 => array (
            'type' => 'foreign key',
            'title' => 'Feature ID',
            'field' => 'feature_id',
            'required' => 1,
            'spreadsheet column' => '',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '3',
            'field_index' => '0',
            'show_all_records' => 0,
            'foreign key' => 'Feature',
            'foreign field' => 'feature_id'
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'DB xref ID',
            'field' => 'dbxref_id',
            'show_all_records' => 0,
            'foreign key' => 'Protein DB xref',
            'foreign field' => 'dbxref_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '6',
            'field_index' => '1'
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      ),
      7 => array (
        'table' => 'db',
        'record_id' => 'Tripal Genbank Parser DB',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 1,
            'constant value' => 'tripal_genbank_parser',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '7',
            'field_index' => '0'
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      8 => array (
        'table' => 'cv',
        'record_id' => 'Tripal Genbank Parser CV',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 1,
            'constant value' => 'tripal_genbank_parser',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '8',
            'field_index' => '0'
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      9 => array (
        'table' => 'dbxref',
        'record_id' => 'Genbank Note DB xref',
        'fields' => array (
          0 => array (
            'type' => 'foreign key',
            'title' => 'db_id',
            'field' => 'db_id',
            'show_all_records' => 0,
            'foreign key' => 'Tripal Genbank Parser DB',
            'foreign field' => 'db_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'constant',
            'title' => 'accession',
            'field' => 'accession',
            'required' => 1,
            'constant value' => 'genbank_note',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '9',
            'field_index' => '1'
          )
        ),
        'mode' => 'insert_once',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      10 => array (
        'table' => 'cvterm',
        'record_id' => 'Genbank Note CV term',
        'fields' => array (
          0 => array (
            'type' => 'foreign key',
            'title' => '',
            'field' => 'cv_id',
            'required' => 1,
            'spreadsheet column' => '',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '11',
            'field_index' => '0',
            'show_all_records' => 0,
            'foreign key' => 'Tripal Genbank Parser CV',
            'foreign field' => 'cv_id'
          ),
          1 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 1,
            'constant value' => 'genbank_note',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '10',
            'field_index' => '1'
          ),
          2 => array (
            'type' => 'foreign key',
            'title' => 'DBX ref ID',
            'field' => 'dbxref_id',
            'show_all_records' => 0,
            'foreign key' => 'Genbank Note DB xref',
            'foreign field' => 'dbxref_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '10',
            'field_index' => '2'
          )
        ),
        'mode' => 'insert_once',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      11 => array (
        'table' => 'featureprop',
        'record_id' => 'Feature Property (genbank_note)',
        'fields' => array (
          2 => array (
            'type' => 'foreign key',
            'title' => 'Feature ID',
            'field' => 'feature_id',
            'show_all_records' => 0,
            'foreign key' => 'Feature',
            'foreign field' => 'feature_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          3 => array (
            'type' => 'foreign key',
            'title' => 'type_id',
            'field' => 'type_id',
            'show_all_records' => 0,
            'foreign key' => 'Genbank Note CV term',
            'foreign field' => 'cvterm_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          4 => array (
            'type' => 'table field',
            'title' => 'value',
            'field' => 'value',
            'required' => 1,
            'spreadsheet column' => '7',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array (
                0 => '/^\\.$/'
              ),
              'replace' => array (
                0 => ''
              )
            ),
            'priority' => '11',
            'field_index' => '4'
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      ),
      12 => array (
        'table' => 'cv',
        'record_id' => 'Rel CV',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 0,
            'constant value' => 'relationship',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      13 => array (
        'table' => 'cvterm',
        'record_id' => 'Rel CV term',
        'fields' => array (
          0 => array (
            'type' => 'table field',
            'title' => 'name',
            'field' => 'name',
            'required' => 1,
            'spreadsheet column' => '9',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array (
                0 => '/^\\.$/'
              ),
              'replace' => array (
                0 => ''
              )
            ),
            'priority' => '14',
            'field_index' => '0'
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'CV ID',
            'field' => 'cv_id',
            'show_all_records' => 0,
            'foreign key' => 'Rel CV',
            'foreign field' => 'cv_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '14',
            'field_index' => '1'
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      14 => array (
        'table' => 'feature_relationship',
        'record_id' => 'Feature Relationship',
        'fields' => array (
          0 => array (
            'type' => 'foreign key',
            'title' => 'Subject ID',
            'field' => 'subject_id',
            'required' => 1,
            'spreadsheet column' => '',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '13',
            'field_index' => '0',
            'show_all_records' => 0,
            'foreign key' => 'Feature',
            'foreign field' => 'feature_id'
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'Object ID',
            'field' => 'object_id',
            'show_all_records' => 0,
            'foreign key' => 'Source Feature',
            'foreign field' => 'feature_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '15',
            'field_index' => '1'
          ),
          2 => array (
            'type' => 'foreign key',
            'title' => 'Type ID',
            'field' => 'type_id',
            'show_all_records' => 0,
            'foreign key' => 'Rel CV term',
            'foreign field' => 'cvterm_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      ),
      15 => array (
        'table' => 'db',
        'record_id' => 'Nucleotide DB',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 1,
            'constant value' => 'nuccore',
            'exposed' => 0,
            'exposed_validate' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'constant',
            'title' => 'URL',
            'field' => 'url',
            'required' => 0,
            'constant value' => 'http://www.ncbi.nlm.nih.gov/nuccore',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          2 => array (
            'type' => 'constant',
            'title' => '',
            'field' => 'urlprefix',
            'required' => 0,
            'constant value' => 'http://www.ncbi.nlm.nih.gov/nuccore/',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          3 => array (
            'type' => 'constant',
            'title' => 'description',
            'field' => 'description',
            'required' => 0,
            'constant value' => 'The Nucleotide database is a collection of sequences from several sources',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert_once',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      16 => array (
        'table' => 'dbxref',
        'record_id' => 'Nucleotide DB xref',
        'fields' => array (
          0 => array (
            'type' => 'table field',
            'title' => 'accession',
            'field' => 'accession',
            'required' => 1,
            'spreadsheet column' => '8',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array (
                0 => '/^(.*?\\.\\d+).*$/'
              ),
              'replace' => array (
                0 => '\\1'
              )
            ),
            'priority' => '17',
            'field_index' => '0'
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'DB ID',
            'field' => 'db_id',
            'show_all_records' => 0,
            'foreign key' => 'Nucleotide DB',
            'foreign field' => 'db_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '18',
            'field_index' => '1'
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      17 => array (
        'table' => 'feature_dbxref',
        'record_id' => 'Feature_Dbxref',
        'fields' => array (
          0 => array (
            'type' => 'foreign key',
            'title' => 'feature_id',
            'field' => 'feature_id',
            'show_all_records' => 0,
            'foreign key' => 'Feature',
            'foreign field' => 'feature_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'dbxref_id',
            'field' => 'dbxref_id',
            'show_all_records' => 0,
            'foreign key' => 'Nucleotide DB xref',
            'foreign field' => 'dbxref_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      ),
      18 => array (
        'table' => 'dbxref',
        'record_id' => 'Source DB xref',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'accession',
            'required' => 1,
            'constant value' => 'source',
            'exposed' => 0,
            'exposed_validate' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'DB ID',
            'field' => 'db_id',
            'show_all_records' => 0,
            'foreign key' => 'Tripal Genbank Parser DB',
            'foreign field' => 'db_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      19 => array (
        'table' => 'cvterm',
        'record_id' => 'Source CV term',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 1,
            'constant value' => 'source',
            'exposed' => 0,
            'exposed_validate' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'CV ID',
            'field' => 'cv_id',
            'show_all_records' => 0,
            'foreign key' => 'Tripal Genbank Parser CV',
            'foreign field' => 'cv_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          2 => array (
            'type' => 'foreign key',
            'title' => 'dbxref_id',
            'field' => 'dbxref_id',
            'show_all_records' => 0,
            'foreign key' => 'Source DB xref',
            'foreign field' => 'dbxref_id',
            'required' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      20 => array (
        'table' => 'featureprop',
        'record_id' => 'Feature Property (source)',
        'fields' => array (
          1 => array (
            'type' => 'foreign key',
            'title' => 'Feature ID',
            'field' => 'feature_id',
            'show_all_records' => 0,
            'foreign key' => 'Feature',
            'foreign field' => 'feature_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '20',
            'field_index' => '1'
          ),
          2 => array (
            'type' => 'foreign key',
            'title' => 'Type ID',
            'field' => 'type_id',
            'show_all_records' => 0,
            'foreign key' => 'Source CV term',
            'foreign field' => 'cvterm_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          3 => array (
            'type' => 'table field',
            'title' => 'value',
            'field' => 'value',
            'required' => 0,
            'spreadsheet column' => '10',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '25',
            'field_index' => '3'
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      ),
      21 => array (
        'table' => 'analysis',
        'record_id' => 'Analysis',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'program',
            'field' => 'program',
            'required' => 1,
            'constant value' => 'Tripal Genbank Parser',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '21',
            'field_index' => '0'
          ),
          1 => array (
            'type' => 'constant',
            'title' => 'programversion',
            'field' => 'programversion',
            'required' => 1,
            'constant value' => '1.0',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '21',
            'field_index' => '1'
          ),
          2 => array (
            'type' => 'constant',
            'title' => 'sourcename',
            'field' => 'sourcename',
            'required' => 1,
            'constant value' => 'NCBI',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'select',
        'select_if_duplicate' => 0,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      22 => array (
        'table' => 'analysisfeature',
        'record_id' => 'Analysis Feature',
        'fields' => array (
          0 => array (
            'type' => 'foreign key',
            'title' => 'Analysis ID',
            'field' => 'analysis_id',
            'show_all_records' => 0,
            'foreign key' => 'Analysis',
            'foreign field' => 'analysis_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'Feature ID',
            'field' => 'feature_id',
            'show_all_records' => 0,
            'foreign key' => 'Feature',
            'foreign field' => 'feature_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      ),
      23 => array (
        'table' => 'dbxref',
        'record_id' => 'Product DB xref',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'Name',
            'field' => 'accession',
            'required' => 1,
            'constant value' => 'product',
            'exposed' => 0,
            'exposed_validate' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '20',
            'field_index' => '0'
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'DB ID',
            'field' => 'db_id',
            'show_all_records' => 0,
            'foreign key' => 'Tripal Genbank Parser DB',
            'foreign field' => 'db_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert_once',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      24 => array (
        'table' => 'cvterm',
        'record_id' => 'Product CV Term',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 1,
            'constant value' => 'product',
            'exposed' => 0,
            'exposed_validate' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'CV ID',
            'field' => 'cv_id',
            'show_all_records' => 0,
            'foreign key' => 'Tripal Genbank Parser CV',
            'foreign field' => 'cv_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          2 => array (
            'type' => 'foreign key',
            'title' => 'DBX ref',
            'field' => 'dbxref_id',
            'show_all_records' => 0,
            'foreign key' => 'Product DB xref',
            'foreign field' => 'dbxref_id',
            'required' => 0,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '22',
            'field_index' => '2'
          )
        ),
        'mode' => 'insert_once',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      25 => array (
        'table' => 'featureprop',
        'record_id' => 'Feature Property (product)',
        'fields' => array (
          1 => array (
            'type' => 'foreign key',
            'title' => 'Feature ID',
            'field' => 'feature_id',
            'show_all_records' => 0,
            'foreign key' => 'Feature',
            'foreign field' => 'feature_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          2 => array (
            'type' => 'foreign key',
            'title' => 'Type ID',
            'field' => 'type_id',
            'show_all_records' => 0,
            'foreign key' => 'Product CV Term',
            'foreign field' => 'cvterm_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          3 => array (
            'type' => 'table field',
            'title' => 'value',
            'field' => 'value',
            'required' => 0,
            'spreadsheet column' => '5',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array (
                0 => '/^\\s*$/'
              ),
              'replace' => array (
                0 => 'n/a'
              )
            ),
            'priority' => '24',
            'field_index' => '3'
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => NULL,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      ),
      27 => array (
        'table' => 'dbxref',
        'record_id' => 'Function DBXref',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'accession',
            'field' => 'accession',
            'required' => 0,
            'constant value' => 'function',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'db_id',
            'field' => 'db_id',
            'show_all_records' => 0,
            'foreign key' => 'Tripal Genbank Parser DB',
            'foreign field' => 'db_id',
            'required' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert_once',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      28 => array (
        'table' => 'cvterm',
        'record_id' => 'Function CVterm',
        'fields' => array (
          0 => array (
            'type' => 'constant',
            'title' => 'name',
            'field' => 'name',
            'required' => 0,
            'constant value' => 'function',
            'exposed' => 0,
            'exposed_validate' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'cv_id',
            'field' => 'cv_id',
            'show_all_records' => 0,
            'foreign key' => 'Tripal Genbank Parser CV',
            'foreign field' => 'cv_id',
            'required' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          ),
          2 => array (
            'type' => 'foreign key',
            'title' => 'dbxref_id',
            'field' => 'dbxref_id',
            'show_all_records' => 0,
            'foreign key' => 'Function DBXref',
            'foreign field' => 'dbxref_id',
            'required' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert_once',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 0
      ),
      29 => array (
        'table' => 'featureprop',
        'record_id' => 'Featureprop (function)',
        'fields' => array (
          0 => array (
            'type' => 'table field',
            'title' => 'value',
            'field' => 'value',
            'required' => 1,
            'spreadsheet column' => '6',
            'exposed' => 0,
            'exposed_description' => '',
            'regex' => array (
              'pattern' => array (
                0 => '/^\\.$/'
              ),
              'replace' => array (
                0 => ''
              )
            )
          ),
          1 => array (
            'type' => 'foreign key',
            'title' => 'feature_id',
            'field' => 'feature_id',
            'show_all_records' => 0,
            'foreign key' => 'Feature',
            'foreign field' => 'feature_id',
            'required' => 1,
            'regex' => array (
              'pattern' => array ()
            ),
            'priority' => '29',
            'field_index' => '1'
          ),
          2 => array (
            'type' => 'foreign key',
            'title' => 'type_id',
            'field' => 'type_id',
            'show_all_records' => 0,
            'foreign key' => 'Function CVterm',
            'foreign field' => 'cvterm_id',
            'required' => 0,
            'regex' => array (
              'pattern' => array ()
            )
          )
        ),
        'mode' => 'insert',
        'select_if_duplicate' => 1,
        'update_if_duplicate' => 0,
        'select_optional' => 0,
        'disable' => 0,
        'optional' => 1
      )
    );
  }
}