<?php

namespace Drupal\tripal_genbank_parser\Loader;

/**
 * Class ProteinLoader loads protein results into a chado database
 *
 */

use Drupal\tripal_genbank_parser\Loader\Template\ProteinTemplate;

class ProteinLoader extends BulkLoader{

  public function __construct($file) {
    $this->template = new ProteinTemplate();
    $this->file = $file;
  }

}