<?php

namespace Drupal\tripal_genbank_parser\Loader;

/**
 * Class GenericGeneLoader loads generic gene results into a chado database
 *
 */

use Drupal\tripal_genbank_parser\Loader\Template\GenericGeneTemplate;

class GenericGeneLoader extends BulkLoader{

  public function __construct($file) {
    $this->template = new GenericGeneTemplate();
    $this->file = $file;
  }

}