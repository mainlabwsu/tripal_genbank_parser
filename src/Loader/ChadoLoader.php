<?php

namespace Drupal\tripal_genbank_parser\Loader;

/**
 * Class ChadoLoader loads parsed Genbank results into a chado database
 *
 */

use Drupal\tripal_genbank_parser\Parser\Format\Header;

class ChadoLoader {

  public function main ($file_prefix, $db) {
    // Prepare return values
    $exit = new stdClass();

    // Read parsing results
    $header = new Header();
    $locus = $file_prefix . $header->files->locus;
    $generic = $file_prefix . $header->files->generic;
    $gene = $file_prefix . $header->files->gene;
    $mrna = $file_prefix . $header->files->mrna;
    $cds = $file_prefix . $header->files->cds;
    $protein = $file_prefix . $header->files->protein;
    $misc = $file_prefix . $header->files->misc;

    // Load into Chado
    if (file_exists($locus)) {
      $loader = new LocusLoader ($locus, $db);
      $exit->Locus = $loader->loadBulkData();
    }
    // Stop loading if the db type is not nucleotide
    if ($db != 'nucleotide') {
      return $exit;
    }
    if (file_exists($generic) && $exit->Locus == 'Loading Completed Successfully') {
      $loader = new GenericGeneLoader ($generic);
      $exit->Generic = $loader->loadBulkData();
    } else {
      return $exit;
    }
    if (file_exists($gene) && $exit->Generic == 'Loading Completed Successfully') {
      $loader = new GeneLoader ($gene);
      $exit->Gene = $loader->loadBulkData();
    } else {
      return $exit;
    }
    if (file_exists($mrna) && $exit->Gene == 'Loading Completed Successfully') {
      $loader = new MRNALoader ($mrna);
      $exit->mRNA = $loader->loadBulkData();
    } else {
      return $exit;
    }
    if (file_exists($cds) && $exit->mRNA == 'Loading Completed Successfully') {
      $loader = new CDSLoader ($cds);
      $exit->CDS = $loader->loadBulkData();
    } else {
      return $exit;
    }
    if (file_exists($protein) && $exit->CDS == 'Loading Completed Successfully') {
      $loader = new ProteinLoader ($protein);
      $exit->Protein = $loader->loadBulkData();
    } else {
      return $exit;
    }
    if (file_exists($misc) && $exit->Protein == 'Loading Completed Successfully') {
      $loader = new MiscFeatureLoader ($misc);
      $exit->Misc = $loader->loadBulkData();
    } else {
      return $exit;
    }
    return $exit;
  }
}