<?php

namespace Drupal\tripal_genbank_parser\Loader\Fast;

class Chado {

  public $verbose;
  public $handle_log;
  public $prefix;
  public $cache_pk = array();
  public $cache_uk = array();

  function __construct($verbose = TRUE) {
    $this->verbose = $verbose;
    $schema = 'chado';
    $this->prefix = $schema . '.';
  }

  function __destruct() {
    if (is_resource($this->handle_log)) {
      fclose($this->handle_log);
    }
  }

  function setLog($file) {
    $this->handle_log = fopen($file, "w");
  }

  /**
   * Get Primary Key for a Chado table
   */
  function getPkey($table) {
    if (key_exists($table, $this->cache_pk)) {
      return $this->cache_pk[$table];
    }
    else {
      $sql = "
      SELECT C.column_name
      FROM information_schema.table_constraints TC
      JOIN information_schema.constraint_column_usage AS CCU USING (constraint_schema, constraint_name)
      JOIN information_schema.columns AS C ON C.table_schema = TC.constraint_schema
      AND TC.table_name = C.table_name AND CCU.column_name = C.column_name
      WHERE constraint_type = 'PRIMARY KEY' and TC.table_name = '$table'";
      $pk = \Drupal::database()->query($sql, array())->fetchField();
      $this->cache_pk[$table] = $pk;
      return $pk;
    }
  }

  /**
   * Get Unique Keys for a Chado table
   */
  function getUkeys($table) {
    if (key_exists($table, $this->cache_uk)) {
      return $this->cache_uk[$table];
    }
    else {
      $sql = "
      SELECT C.column_name
      FROM information_schema.table_constraints TC
      JOIN information_schema.constraint_column_usage AS CCU USING (constraint_schema, constraint_name)
      JOIN information_schema.columns AS C ON C.table_schema = TC.constraint_schema
      AND TC.table_name = C.table_name AND CCU.column_name = C.column_name
      WHERE constraint_type = 'UNIQUE' and TC.table_name = '$table'";
      $results = \Drupal::database()->query($sql, array());
      $ukeys = array();
      while ($uk = $results->fetchField()) {
        $ukeys [] = $uk;
      }
      $this->cache_uk[$table] = $ukeys;
      return $ukeys;
    }
  }

  /*
   * No fail. Pass in any function and it will not fail on Exceptions. Only error message will be shown
   */
  public function noFail ($function, $args, $rowcount = 0, $silent = FALSE) {
    try {
      return call_user_func_array(array($this, $function), $args);
    } catch (\Exception $e) {
      if ($this->verbose && !$silent) {
        if ($rowcount > 0) {
          print '[Error at Line ' . $rowcount . '] ';
        }
        print $e->getMessage() . "\n";
      }
    }
  }

  /**
   * Get ID (i.e. the primary key) from a table by passing in the unique key values
   */
  public function getId ($table, $conditions = array(), $insert_if_not_exist = FALSE) {
    $pk = $this->getPkey($table);
    $all_ukey_exists = TRUE;
    $uks = $this->getUkeys($table);
    $uniqcond = array();
    foreach ($uks AS $uk) {
      if (!key_exists($uk, $conditions)) {
        $all_ukey_exists = FALSE;
      }
      else {
        $uniqcond[$uk] = $conditions[$uk];
      }
    }

    if ($all_ukey_exists) {
      $id = $this->getFirstField($table, $pk, $uniqcond);
      if ($id) {
        if (is_resource($this->handle_log)) {
          $content = 'Found> [' . $table . ': ' . $id . '] (' . implode(', ', array_keys($uniqcond)). ') '. implode (', ', $uniqcond) . "\n";
          fwrite($this->handle_log, $content);
        }
        return $id;
      }
      else if ($insert_if_not_exist) {
        return $this->setField($table, $conditions, $pk);
      }
      else {
        return NULL;
      }
    }
    else {
      $err_uks = '(' . implode(', ', $uks) . ')';
      throw new \Exception("[Chado::getId] Unique key constraints not met. Provide all unique keys $err_uks for table '$table' to get an ID.");
    }
  }

  /**
   * Get first returned value from a table
   */
  public function getFirstField ($table, $field, $conditions = array(), $order = array(), $insert_if_not_exist = FALSE) {

    $fields = $this->getFields($table, $field, $conditions, $order, 1);
    if (count($fields) > 0) {
      return $fields [0];
    }
    else if ($insert_if_not_exist) {
        return $this->setField($table, $conditions, $field);
    }
    else {
      return NULL;
    }
  }

  /**
   * Get all returned values in an array from a table.
   */
  public function getFields ($table, $field, $conditions = array(), $order = array(), $limit = 0, $offset = 0) {
    $fields = array();
    $results = $this->getResults ($table, array($field), array(), $conditions, $order, $limit, $offset);
    while (($f = $results->fetchField()) != NULL) {
      $fields [] = $f;
    }
    return $fields;
  }

  /**
   * Get first result as an object from a table.
   */
  public function getFirstObject ($table, $select = array(), $joins = array(), $conditions = array(), $order = array()) {
    $objects = $this->getObjects($table, $select, $joins, $conditions, $order, 1);
    if (count($objects) == 1) {
      return $objects[0];
    }
    else {
      return NULL;
    }
  }

  /**
   * Get all results in an object array from a table.
   */
  public function getObjects ($table, $select = array(), $joins = array(), $conditions = array(), $order = array(), $limit = 0, $offset = 0) {
    $objects = array();
    $results = $this->getResults ($table, $select, $joins, $conditions, $order, $limit, $offset);
    while ($obj = $results->fetchObject()) {
      $objects [] = $obj;
    }
    return $objects;
  }

  /**
   * Get results from a table. The SELECT SQL generator.
   */
  public function getResults ($table, $select = array(), $joins = array(), $conditions = array(), $order = array(), $limit = 0, $offset = 0) {
    // SELECT clause
    $sql = 'SELECT ' . implode(',', $select) . ' FROM ' . $this->prefix . $table;

    // JOIN clause
    $leftjoin = isset($joins['left']) ? $joins['left'] : array();
    $innerjoin = isset($joins['inner']) ? $joins['inner'] : array();
    foreach ($leftjoin AS $t => $on) {
      $has_schema = count(explode('.', $t)) > 1 ? TRUE : FALSE;
      if (!$has_schema) {
        $t = $this->prefix . $t;
      }
      $sql .= ' LEFT JOIN ' . $t . ' ON ' . $on . ' ';
    }
    foreach ($innerjoin AS $t => $on) {
      $has_schema = count(explode('.', $t)) > 1 ? TRUE : FALSE;
      if (!$has_schema) {
        $t = $this->prefix . $t;
      }
      $sql .= ' LEFT JOIN ' . $t . ' ON ' . $on . ' ';
    }

    // WHERE clause
    $sql .= ' WHERE 1=1';
    $args = array();
    foreach ($conditions AS $column => $value) {
      if (is_array($value)) {
        $sql .= ' AND ' . $column . ' IN (' . ':' . $column . '[])';
        $args [':' . $column . '[]'] = $value;
      }
      else {
        $sql .= ' AND ' . $column . ' = ' . ':' . $column;
        $args [':' . $column] = $value;
      }
    }

    // ORDER BY clause
    if (count($order) > 0) {
      $sql .= ' ORDER BY ' . implode(',', $order);
      $order_keys = array_keys($order);
      $okey = $order_keys[0];
      if ($okey == 'desc') {
        $sql .= ' DESC';
      }
      else if ($okey == 'asc') {
        $sql .= ' ASC';
      }
    }

    // LIMIT and OFFSET
    if ($limit > 0) {
      $sql .= ' LIMIT ' . $limit;
    }
    if ($offset > 0) {
      $sql .= ' OFFSET ' . $offset;
    }
    $results = \Drupal::database()->query($sql, $args);
    return $results;
  }


  /**
   * Insert one record to a table. The INSERT SQL generator.
   */
  public function setField($table, $conditions = array(), $returning = NULL) {
    $columns = array_keys($conditions);
    $args = array();
    $holders = array();
    foreach ($conditions AS $col => $val) {
      $args[':' . $col] = $val;
      $holders [] = ':' . $col;
    }
    $sql = 'INSERT INTO ' . $this->prefix . $table . '(' . implode(',', $columns) . ') VALUES (' . implode(',', $holders) . ')';
    if ($returning) {
      $sql .= ' RETURNING ' . $returning;
    }
    $result = \Drupal::database()->query($sql, $args);
    $return_val = $result->fetchField();
    if (is_resource($this->handle_log)) {
      $content = 'Insert> ['. $table;
      if ($returning) {
        $content .= ': ' . $return_val;
      }
      $content .= '] (' . implode(', ', $columns) . ') ' . implode(', ', $conditions) . "\n";
      fwrite($this->handle_log, $content);
    }
    if ($returning) {
      return $return_val;
    }
  }

}