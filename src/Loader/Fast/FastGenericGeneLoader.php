<?php

namespace Drupal\tripal_genbank_parser\Loader\Fast;

/**
 * Class GenericGeneLoader loads generic gene results into a chado database
 *
 */

class FastGenericGeneLoader {

  public $file;

  public function __construct($file) {
    $this->file = $file;
  }

  public function loadData () {
    $loaded_without_errors = TRUE;

    print "Opening file: " . $this->file . "\n";
    $handle = fopen($this->file, 'r');
    if ($handle) {
      $chado = new Chado();
      $chado->setLog($this->file . '.log');

      // Constnat values
      // db: Tripal Genbank Parser
      $tgp_db_id = $chado->getId('db', array('name' => 'tripal_genbank_parser'), TRUE);

      // cv: Sequence Ontology
      $so_cv_id = $chado->getId('cv', array('name' => 'sequence'));

      // cv: Tripal Genbank Parser
      $tgp_cv_id = $chado->getId('cv', array('name' => 'tripal_genbank_parser'), TRUE);

      // cvterm: gene/source/evidence_for_feature
      $gene_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $so_cv_id, 'name' => 'gene'));

      $src_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'source', 'version' => ''), TRUE);
      $src_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $src_dbxref_id, 'name' => 'source', 'is_obsolete' => 0), TRUE);
      $evidence_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $so_cv_id, 'name' => 'evidence_for_feature'));

      // analysis: program, programversion, sourcename
      $tgp_analysis_id = $chado->getId('analysis', array('program' => 'Tripal Genbank Parser', 'programversion' => '4.0', 'sourcename' => 'Tripal Genbank Parser'), TRUE);


      $counter = 0;
      while (($line = fgets($handle)) !== false) {
        // Skip the header
        if ($counter == 0) {
          $counter ++;
          continue;
        }
        $cell = explode ("\t", $line);
        $c1_f_uname = $cell[0];
        $c2_f_name = $cell[1];
        $c3_f_type_id = $cell[2];
        $c4_org = explode(' ', $cell[3], 2);
        $c5_fp_value = $cell[4];

        try {
          // C4 organism: genus species
          $organism_id = $chado->getFirstField('organism', 'organism_id', ['genus' => $c4_org[0], 'species' => $c4_org[1]], [], TRUE);

          // C1, C2 feature: uniquename, name
          $feature_id = $chado->getId('feature', array('uniquename' => $c1_f_uname, 'name' => $c2_f_name, 'organism_id' => $organism_id, 'type_id' => $gene_cvterm_id), TRUE);
          $analysisfeature_id = $chado->getId('analysisfeature', array('analysis_id' => $tgp_analysis_id, 'feature_id' => $feature_id), TRUE);

          // C5 featureprop: source
          if (trim($c5_fp_value) != '.') {
            $src_fprop_id = $chado->getId('featureprop', array(
              'feature_id' => $feature_id,
              'type_id' => $src_cvterm_id,
              'value' => $c5_fp_value,
              'rank' => 0
            ), TRUE);
          }

          $fp_id = $chado->getId('featureprop', array('feature_id' => $feature_id, 'type_id' => $evidence_cvterm_id, 'value' => 'imported from NCBI', 'rank' => 0), TRUE);
        }
        catch (\Exception $e) {
          print '[Error at Line ' . ($counter + 1) . '] ';
          print $e->getMessage() . "\n";
        }

        $counter ++;
      }
      fclose($handle);
    }
    else {
      return "Can not open file $this->file\n";
    }


    // set the status of the job (in the node not the tripal jobs)
    if ($loaded_without_errors) {
      $status = 'Loading Completed Successfully';
    }
    else {
      $status = 'Errors Encountered';
    }
    return $status;
  }
}