<?php

namespace Drupal\tripal_genbank_parser\Loader\Fast;

/**
 * Class MRNALoader loads mRNA results into a chado database
 *
 */

class FastMRNALoader {

  public $file;

  public function __construct($file) {
    $this->file = $file;
  }

  public function loadData () {
    $loaded_without_errors = TRUE;

    print "Opening file: " . $this->file . "\n";
    $handle = fopen($this->file, 'r');
    if ($handle) {
      $chado = new Chado();
      $chado->setLog($this->file . '.log');

      // Constnat values
      // db: Tripal Genbank Parser
      $tgp_db_id = $chado->getId('db', array('name' => 'tripal_genbank_parser'), TRUE);
      $protein_db_id = $chado->getId('db', array(
        'name' => 'GI',
        'url' => 'http://www.ncbi.nlm.nih.gov/protein',
        'urlprefix' => 'http://www.ncbi.nlm.nih.gov/protein/',
        'description' => 'The Protein database is a collection of sequences from several sources.'),
        TRUE
      );
      $nucleotide_db_id = $chado->getId('db', array(
        'name' => 'nuccore',
        'url' => 'http://www.ncbi.nlm.nih.gov/nuccore',
        'urlprefix' => 'http://www.ncbi.nlm.nih.gov/nuccore/',
        'description' => 'The Nucleotide database is a collection of sequences from several sources.'),
        TRUE
      );

      // cv: Sequence Ontology
      $so_cv_id = $chado->getId('cv', array('name' => 'sequence'));

      // cv: Tripal Genbank Parser
      $tgp_cv_id = $chado->getId('cv', array('name' => 'tripal_genbank_parser'), TRUE);

      // cv: Relationship
      $rel_cv_id = $chado->getId('cv', array('name' => 'relationship'));

      // cvterm: gene/source/product/function/genbank_note/associate_with
      $mrna_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $so_cv_id, 'name' => 'mRNA'));
      $region_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $so_cv_id, 'name' => 'region'));
      $gene_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $so_cv_id, 'name' => 'gene'));
      $rel_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $rel_cv_id, 'name' => 'part_of'));
      $assoc_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $so_cv_id, 'name' => 'associated_with'));

      $src_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'source', 'version' => ''), TRUE);
      $src_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $src_dbxref_id, 'name' => 'source', 'is_obsolete' => 0), TRUE);

      $product_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'product', 'version' => ''), TRUE);
      $product_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $product_dbxref_id, 'name' => 'product', 'is_obsolete' => 0), TRUE);

      $func_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'function', 'version' => ''), TRUE);
      $func_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $func_dbxref_id, 'name' => 'function', 'is_obsolete' => 0), TRUE);

      $gbnote_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'genbank_note', 'version' => ''), TRUE);
      $gbnote_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $gbnote_dbxref_id, 'name' => 'genbank_note', 'is_obsolete' => 0), TRUE);

      // analysis: program, programversion, sourcename
      $tgp_analysis_id = $chado->getId('analysis', array('program' => 'Tripal Genbank Parser', 'programversion' => '4.0', 'sourcename' => 'NCBI'), TRUE);


      $counter = 0;
      while (($line = fgets($handle)) !== false) {
        // Skip the header
        if ($counter == 0) {
          $counter ++;
          continue;
        }
        $cell = explode ("\t", $line);
        $c1_f_uname = $cell[0];
        $c2_f_name = $cell[1];
        $c3_f_type_id = $cell[2];
        $c4_fl_srcf_id = $cell[3];
        $c5_fl_fmin = $cell[4];
        $c6_fl_fmax = $cell[5];
        $c7_fl_min_partial = $cell[6];
        $c8_fl_max_partial = $cell[7];
        $c9_fl_strand = $cell[8];
        $c10_fd_fd_id = $cell[9];
        $token_c10 = explode(':', $c10_fd_fd_id, 2);
        $protein_acc = key_exists(1, $token_c10) ? $token_c10[1] : NULL;
        $c11_fp_v_prod = $cell[10];
        $c12_fp_v_func = $cell[11];
        $c13_fp_v_note = $cell[12];
        $c14_fr_obj_id = $cell[13];
        $c15_fr_type_id = $cell[14];
        $c16_fp_value = $cell[15];
        $c17_f_residue = $cell[16];

        try {

          // C4
          $srcfeature_id = $chado->getFirstField('feature', 'feature_id', array('uniquename' => $c4_fl_srcf_id, 'type_id' => $region_cvterm_id));
          // Ignore genbank records that are without a sequence
          if (!$srcfeature_id) {
            continue;
          }
          $srcorg_id = $chado->getFirstField('feature', 'organism_id', array('uniquename' => $c4_fl_srcf_id, 'type_id' => $region_cvterm_id));

          // C1, C2, C17 feature: uniquename, name, residues
          $feature_id = $chado->getId('feature', array(
            'organism_id' => $srcorg_id,
            'uniquename' => $c1_f_uname,
            'name' => $c2_f_name,
            'type_id' => $mrna_cvterm_id,
            'residues' => $c17_f_residue
          ), TRUE);

          // C10 dbxref/feature_dbxref: accession
          if ($protein_acc) {
            $protein_dbxref_id = $chado->getId('dbxref', array('accession' => $protein_acc, 'db_id' => $protein_db_id, 'version' => ''), TRUE);
            $fdbxref_id = $chado->getId('feature_dbxref', array('feature_id' => $feature_id, 'dbxref_id' => $protein_dbxref_id), TRUE);
          }


          // C5, C6, C7, C8, C9 featureloc
          // Get the max rank
          $featureloc_rank = $chado->getFirstField(
            'featureloc',
            'rank',
            array(
              'feature_id' => $feature_id,
              'locgroup' => 0,
            ),
            array('desc' => 'rank')
          );
          // calculate rank accordingly
          if (is_numeric($featureloc_rank)) {
            $rank = $featureloc_rank + 1;
          }
          else {
            $rank = 0;
          }
          //Find featureloc
          $featureloc_id = $chado->getFirstField(
            'featureloc',
            'featureloc_id',
            array(
              'srcfeature_id' => $srcfeature_id,
              'feature_id' => $feature_id,
              'fmin' => $c5_fl_fmin < $c6_fl_fmax ? $c5_fl_fmin : $c6_fl_fmax,
              'fmax' => $c5_fl_fmin < $c6_fl_fmax ? $c6_fl_fmax : $c5_fl_fmin,
              'strand' => $c9_fl_strand
            )
          );
          // Add featureloc if it does not already exist
          if (!$featureloc_id && $srcfeature_id && $c5_fl_fmin && $c6_fl_fmax) {
            $featureloc_id = $chado->setField(
              'featureloc',
              array(
                'srcfeature_id' => $srcfeature_id,
                'feature_id' => $feature_id,
                'fmin' => $c5_fl_fmin < $c6_fl_fmax ? $c5_fl_fmin : $c6_fl_fmax,
                'fmax' => $c5_fl_fmin < $c6_fl_fmax ? $c6_fl_fmax : $c5_fl_fmin,
                'is_fmin_partial' => $c5_fl_fmin < $c6_fl_fmax ? $c7_fl_min_partial : $c8_fl_max_partial,
                'is_fmax_partial' => $c5_fl_fmin < $c6_fl_fmax ? $c8_fl_max_partial : $c7_fl_min_partial,
                'strand' => $c9_fl_strand,
                'rank' => $rank
              ),
              'featureloc_id'
            );
          }

          // C11 featureprop: product
          if (trim($c11_fp_v_prod) != '.') {
            $product_fp_id = $chado->getId('featureprop', array('feature_id' => $feature_id, 'type_id' => $product_cvterm_id, 'value' => $c11_fp_v_prod, 'rank' => 0), TRUE);
          }

          // C14 feature_relationship: object_id
          if (trim($c14_fr_obj_id) != '.' && trim($c14_fr_obj_id) != '') {
            $obj_id = $chado->getId('feature', array('uniquename' => $c14_fr_obj_id, 'organism_id' => $srcorg_id, 'type_id' => $gene_cvterm_id));
            $fr_id = $chado->getId('feature_relationship', array('object_id' => $obj_id, 'subject_id' => $feature_id, 'type_id' => $rel_cvterm_id, 'rank' => 0), TRUE);
          }

          // C4 as dbxref/feature_dbxref: accession
          $nucleotide_dbxref_id = $chado->getId('dbxref', array('accession' => $c4_fl_srcf_id, 'db_id' => $nucleotide_db_id, 'version' => ''), TRUE);
          $fdbxref_id = $chado->getId('feature_dbxref', array('feature_id' => $feature_id, 'dbxref_id' => $nucleotide_dbxref_id), TRUE);

          // C16 featureprop: source
          if (trim($c16_fp_value) != '.') {
            $src_fprop_id = $chado->getId('featureprop', array(
              'feature_id' => $feature_id,
              'type_id' => $src_cvterm_id,
              'value' => $c16_fp_value,
              'rank' => 0
            ), TRUE);
          }

          // analysisfeature
          $chado->getId('analysisfeature', array('analysis_id' => $tgp_analysis_id, 'feature_id' => $feature_id), TRUE);

          // C12 featureprop: function
          if (trim($c12_fp_v_func) != '.') {
            $func_fp_id = $chado->getId('featureprop', array('feature_id' => $feature_id, 'type_id' => $func_cvterm_id, 'value' => $c12_fp_v_func, 'rank' => 0), TRUE);
          }
          // C13 featureprop: genbank_note
          if (trim($c13_fp_v_note) != '.') {
            $gbnote_fp_id = $chado->getId('featureprop', array('feature_id' => $feature_id, 'type_id' => $gbnote_cvterm_id, 'value' => $c13_fp_v_note, 'rank' => 0), TRUE);
          }

          // associated_with generic_gene
          $generic_gene_id = $chado->getId('feature', array('uniquename' => $c2_f_name, 'organism_id' => $srcorg_id, 'type_id' => $gene_cvterm_id));
          if ($generic_gene_id) {
            $generic_gene_fr_id = $chado->getId('feature_relationship', array('subject_id' => $feature_id, 'object_id' => $generic_gene_id, 'type_id' => $assoc_cvterm_id, 'rank' => 0), TRUE);
          }
        }
        catch (\Exception $e) {
          print '[Error at Line ' . ($counter + 1) . '] ';
          print $e->getMessage() . "\n";
        }

        $counter ++;
      }
      fclose($handle);
    }
    else {
      return "Can not open file $this->file\n";
    }


    // set the status of the job (in the node not the tripal jobs)
    if ($loaded_without_errors) {
      $status = 'Loading Completed Successfully';
    }
    else {
      $status = 'Errors Encountered';
    }
    return $status;
  }
}