<?php

namespace Drupal\tripal_genbank_parser\Loader\Fast;

/**
 * Class CDSLoader loads CDS results into a chado database
 *
 */

class FastCDSLoader {

  public $file;

  public function __construct($file) {
    $this->file = $file;
  }

  public function loadData () {
    $loaded_without_errors = TRUE;

    print "Opening file: " . $this->file . "\n";
    $handle = fopen($this->file, 'r');
    if ($handle) {
      $chado = new Chado();
      $chado->setLog($this->file . '.log');

      // Constnat values
      // db: Tripal Genbank Parser
      $tgp_db_id = $chado->getId('db', array('name' => 'tripal_genbank_parser'), TRUE);
      $nucleotide_db_id = $chado->getId('db', array(
        'name' => 'nuccore',
        'url' => 'http://www.ncbi.nlm.nih.gov/nuccore',
        'urlprefix' => 'http://www.ncbi.nlm.nih.gov/nuccore/',
        'description' => 'The Nucleotide database is a collection of sequences from several sources.',
        TRUE)
      );

      // cv: Sequence Ontology/Relationship Ontology
      $so_cv_id = $chado->getId('cv', array('name' => 'sequence'));
      $rel_cv_id = $chado->getId('cv', array('name' => 'relationship'));

      // cv: Tripal Genbank Parser
      $tgp_cv_id = $chado->getId('cv', array('name' => 'tripal_genbank_parser'), TRUE);

      // cvterm: CDS/region/mRNA/source
      $cds_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $so_cv_id, 'name' => 'CDS'));
      $region_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $so_cv_id, 'name' => 'region'));
      $mrna_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $so_cv_id, 'name' => 'mRNA'));
      $part_of_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $rel_cv_id, 'name' => 'part_of'));

      $src_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'source', 'version' => ''), TRUE);
      $src_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $src_dbxref_id, 'name' => 'source', 'is_obsolete' => 0), TRUE);

      // analysis: program, programversion, sourcename
      $tgp_analysis_id = $chado->getId('analysis', array('program' => 'Tripal Genbank Parser', 'programversion' => '4.0', 'sourcename' => 'NCBI'), TRUE);

      $counter = 0;
      while (($line = fgets($handle)) !== false) {
        // Skip the header
        if ($counter == 0) {
          $counter ++;
          continue;
        }
        $cell = explode ("\t", $line);
        $c1_f_uname = $cell[0];
        $c2_f_name = $cell[1];
        $c3_f_type_id = $cell[2];
        $c4_fl_srcf_id = $cell[3];
        $c5_fl_fmin = $cell[4];
        $c6_fl_fmax = $cell[5];
        $c7_fl_min_partial = $cell[6];
        $c8_fl_max_partial = $cell[7];
        $c9_fl_strand = $cell[8];
        $c10_fl_phase = $cell[9];
        $c11_fr_obj_id = $cell[10];
        $c12_fr_type_id = $cell[11];
        $c13_fp_value = $cell[12];

        try {
          // C4
          $srcfeature_id = $chado->getFirstField('feature', 'feature_id', array('uniquename' => $c4_fl_srcf_id, 'type_id' => $region_cvterm_id));
          // Ignore genbank records that are without a sequence
          if (!$srcfeature_id) {
            continue;
          }
          $srcorg_id = $chado->getFirstField('feature', 'organism_id', array('uniquename' => $c4_fl_srcf_id, 'type_id' => $region_cvterm_id));

          // C1, C2 feature: uniquename, name
          $feature_id = $chado->getId('feature', array(
            'organism_id' => $srcorg_id,
            'uniquename' => $c1_f_uname,
            'name' => $c2_f_name,
            'type_id' => $cds_cvterm_id,
          ), TRUE);

          // C5, C6, C7, C8, C9 featureloc
          // Get the max rank
          $featureloc_rank = $chado->getFirstField(
            'featureloc',
            'rank',
            array(
              'feature_id' => $feature_id,
              'locgroup' => 0,
            ),
            array('desc' => 'rank')
          );
          // calculate rank accordingly
          if (is_numeric($featureloc_rank)) {
            $rank = $featureloc_rank + 1;
          }
          else {
            $rank = 0;
          }
          //Find featureloc
          $featureloc_id = $chado->getFirstField(
            'featureloc',
            'featureloc_id',
            array(
              'srcfeature_id' => $srcfeature_id,
              'feature_id' => $feature_id,
              'fmin' => $c5_fl_fmin < $c6_fl_fmax ? $c5_fl_fmin : $c6_fl_fmax,
              'fmax' => $c5_fl_fmin < $c6_fl_fmax ? $c6_fl_fmax : $c5_fl_fmin,
              'strand' => $c9_fl_strand
            )
          );
          // Add featureloc if it does not already exist
          if (!$featureloc_id && $srcfeature_id && $c5_fl_fmin && $c6_fl_fmax) {
            $featureloc_id = $chado->setField(
              'featureloc',
              array(
                'srcfeature_id' => $srcfeature_id,
                'feature_id' => $feature_id,
                'fmin' => $c5_fl_fmin < $c6_fl_fmax ? $c5_fl_fmin : $c6_fl_fmax,
                'fmax' => $c5_fl_fmin < $c6_fl_fmax ? $c6_fl_fmax : $c5_fl_fmin,
                'is_fmin_partial' => $c5_fl_fmin < $c6_fl_fmax ? $c7_fl_min_partial : $c8_fl_max_partial,
                'is_fmax_partial' => $c5_fl_fmin < $c6_fl_fmax ? $c8_fl_max_partial : $c7_fl_min_partial,
                'strand' => $c9_fl_strand,
                'phase' => $c10_fl_phase,
                'rank' => $rank
              ),
              'featureloc_id'
            );
          }

          // C11 feature_relationship: object_id
          $object_id = $chado->getId('feature', array('uniquename' => $c11_fr_obj_id, 'organism_id' => $srcorg_id, 'type_id' => $mrna_cvterm_id));
          if ($object_id) {
            $fr_id = $chado->getId('feature_relationship', array('object_id' => $object_id, 'subject_id' => $feature_id, 'type_id' => $part_of_cvterm_id, 'rank' => 0), TRUE);
          }

          // C4 as dbxref/feature_dbxref: accession
          $nucleotide_dbxref_id = $chado->getId('dbxref', array('accession' => $c4_fl_srcf_id, 'db_id' => $nucleotide_db_id, 'version' => ''), TRUE);
          $fdbxref_id = $chado->getId('feature_dbxref', array('feature_id' => $feature_id, 'dbxref_id' => $nucleotide_dbxref_id), TRUE);

          // C16 featureprop: source
          if (trim($c13_fp_value) != '.') {
            $src_fprop_id = $chado->getId('featureprop', array(
              'feature_id' => $feature_id,
              'type_id' => $src_cvterm_id,
              'value' => $c13_fp_value,
              'rank' => 0
            ), TRUE);
          }

          // analysisfeature
          $chado->getId('analysisfeature', array('analysis_id' => $tgp_analysis_id, 'feature_id' => $feature_id), TRUE);

        }
        catch (\Exception $e) {
          print '[Error at Line ' . ($counter + 1) . '] ';
          print $e->getMessage() . "\n";
        }

        $counter ++;
      }
      fclose($handle);
    }
    else {
      return "Can not open file $this->file\n";
    }


    // set the status of the job (in the node not the tripal jobs)
    if ($loaded_without_errors) {
      $status = 'Loading Completed Successfully';
    }
    else {
      $status = 'Errors Encountered';
    }
    return $status;
  }

}