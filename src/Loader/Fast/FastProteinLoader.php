<?php

namespace Drupal\tripal_genbank_parser\Loader\Fast;

/**
 * Class ProteinLoader loads protein results into a chado database
 *
 */

class FastProteinLoader {

  public $file;

  public function __construct($file) {
    $this->file = $file;
  }

  public function loadData () {
    $loaded_without_errors = TRUE;

    print "Opening file: " . $this->file . "\n";
    $handle = fopen($this->file, 'r');
    if ($handle) {
      $chado = new Chado();
      $chado->setLog($this->file . '.log');

      // Constnat values
      // db: Tripal Genbank Parser
      $tgp_db_id = $chado->getId('db', array('name' => 'tripal_genbank_parser'), TRUE);
      $protein_db_id = $chado->getId('db', array(
        'name' => 'GI',
        'url' => 'http://www.ncbi.nlm.nih.gov/protein',
        'urlprefix' => 'http://www.ncbi.nlm.nih.gov/protein/',
        'description' => 'The Protein database is a collection of sequences from several sources.'),
        TRUE
      );
      $nucleotide_db_id = $chado->getId('db', array(
        'name' => 'nuccore',
        'url' => 'http://www.ncbi.nlm.nih.gov/nuccore',
        'urlprefix' => 'http://www.ncbi.nlm.nih.gov/nuccore/',
        'description' => 'The Nucleotide database is a collection of sequences from several sources.'),
        TRUE
      );

      // cv: Sequence Ontology/Relationship Ontology
      $so_cv_id = $chado->getId('cv', array('name' => 'sequence'));
      $rel_cv_id = $chado->getId('cv', array('name' => 'relationship'));

      // cv: Tripal Genbank Parser
      $tgp_cv_id = $chado->getId('cv', array('name' => 'tripal_genbank_parser'), TRUE);

      // cvterm: gene/source/genebank_note
      $pt_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $so_cv_id, 'name' => 'polypeptide'));
      $mrna_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $so_cv_id, 'name' => 'mRNA'));

      $src_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'source', 'version' => ''), TRUE);
      $src_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $src_dbxref_id, 'name' => 'source', 'is_obsolete' => 0), TRUE);

      $derives_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('name' => 'derives_from', 'cv_id' => $rel_cv_id));

      $gb_note_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'genbank_note', 'version' => ''), TRUE);
      $gb_note_cvterm_id = $chado->getId('cvterm', array('name' => 'genbank_note', 'cv_id' => $tgp_cv_id, 'dbxref_id' => $gb_note_dbxref_id, 'is_obsolete' => 0), TRUE);

      $product_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'product', 'version' => ''), TRUE);
      $product_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $product_dbxref_id, 'name' => 'product', 'is_obsolete' => 0), TRUE);

      $func_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'function', 'version' => ''), TRUE);
      $func_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $func_dbxref_id, 'name' => 'function', 'is_obsolete' => 0), TRUE);

      // analysis: program, programversion, sourcename
      $tgp_analysis_id = $chado->getId('analysis', array('program' => 'Tripal Genbank Parser', 'programversion' => '4.0', 'sourcename' => 'NCBI'), TRUE);

      $counter = 0;
      while (($line = fgets($handle)) !== false) {
        // Skip the header
        if ($counter == 0) {
          $counter ++;
          continue;
        }
        $cell = explode ("\t", $line);
        $c1_f_uname = $cell[0];
        $c2_f_name = $cell[1];
        $c3_f_type_id = $cell[2];
        $c4_dx_acc = $cell[3];
        $c5_fp_v_prod = $cell[4];
        $c6_fp_v_func = $cell[5];
        $c7_fp_v_note = $cell[6];
        $c8_fr_obj_id = $cell[7];
        $c9_fr_type_id = $cell[8];
        $c10_fp_value = $cell[9];
        $c11_other = $cell[10];
        $c12_f_residue = $cell[11];

        try {
          // C4
          $srcorg_id = $chado->getFirstField('feature', 'organism_id', array('uniquename' => $c8_fr_obj_id, 'type_id' => $mrna_cvterm_id));
          // Ignore genbank records that are without a sequence
          if (!$srcorg_id) {
            continue;
          }

          // C1, C2, C17 feature: uniquename, name, residues
          $feature_id = $chado->getId('feature', array(
            'organism_id' => $srcorg_id,
            'uniquename' => $c1_f_uname,
            'name' => $c2_f_name,
            'type_id' => $pt_cvterm_id,
            'residues' => $c12_f_residue
          ), TRUE);

          // C4 dbxref/feature_dbxref: accession
          if ($c4_dx_acc) {
            $protein_dbxref_id = $chado->getId('dbxref', array('accession' => $c4_dx_acc, 'db_id' => $protein_db_id, 'version' => ''), TRUE);
            $fdbxref_id = $chado->getId('feature_dbxref', array('feature_id' => $feature_id, 'dbxref_id' => $protein_dbxref_id), TRUE);
          }

          // C13 featureprop: genbank_note
          if (trim($c7_fp_v_note) != '.') {
            $gb_note_fp_id = $chado->getId('featureprop', array('feature_id' => $feature_id, 'type_id' => $gb_note_cvterm_id, 'value' => $c7_fp_v_note, 'rank' => 0), TRUE);
          }

          // C8 feature_relationship (derives_from mRNA): object_id
          $object_id = $chado->getId('feature', array('uniquename' => $c8_fr_obj_id, 'type_id' => $mrna_cvterm_id, 'organism_id' => $srcorg_id));
          if ($object_id && $derives_cvterm_id) {
            $fr_id = $chado->getId('feature_relationship', array('subject_id' => $feature_id, 'object_id' => $object_id, 'type_id' => $derives_cvterm_id, 'rank' => 0), TRUE);
          }

          // C4 as dbxref/feature_dbxref: accession
          $nucleotide_dbxref_id = $chado->getId('dbxref', array('accession' => $c4_dx_acc, 'db_id' => $nucleotide_db_id, 'version' => ''), TRUE);
          $fdbxref_id = $chado->getId('feature_dbxref', array('feature_id' => $feature_id, 'dbxref_id' => $nucleotide_dbxref_id), TRUE);

          // C10 featureprop: source
          if (trim($c10_fp_value) != '.') {
            $src_fprop_id = $chado->getId('featureprop', array(
              'feature_id' => $feature_id,
              'type_id' => $src_cvterm_id,
              'value' => $c10_fp_value,
              'rank' => 0
            ), TRUE);
          }

          // analysisfeature
          $chado->getId('analysisfeature', array('analysis_id' => $tgp_analysis_id, 'feature_id' => $feature_id), TRUE);

          // C5 featureprop: product
          if (trim($c5_fp_v_prod) != '.') {
            $product_fp_id = $chado->getId('featureprop', array('feature_id' => $feature_id, 'type_id' => $product_cvterm_id, 'value' => $c5_fp_v_prod, 'rank' => 0), TRUE);
          }

          // C6 featureprop: function
          if (trim($c6_fp_v_func) != '.') {
            $func_fp_id = $chado->getId('featureprop', array('feature_id' => $feature_id, 'type_id' => $func_cvterm_id, 'value' => $c6_fp_v_func, 'rank' => 0), TRUE);
          }

        }
        catch (\Exception $e) {
          print '[Error at Line ' . ($counter + 1) . '] ';
          print $e->getMessage() . "\n";
        }

        $counter ++;
      }
      fclose($handle);
    }
    else {
      return "Can not open file $this->file\n";
    }


    // set the status of the job (in the node not the tripal jobs)
    if ($loaded_without_errors) {
      $status = 'Loading Completed Successfully';
    }
    else {
      $status = 'Errors Encountered';
    }
    return $status;
  }
}