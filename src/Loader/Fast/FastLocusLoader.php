<?php

namespace Drupal\tripal_genbank_parser\Loader\Fast;

/**
 * Class LocusLoader loads locus results into a chado database
 *
 */

class FastLocusLoader {

  public $file;

  public function __construct($file, $db) {
    $this->file = $file;
  }

  public function loadData () {
    $loaded_without_errors = TRUE;

    print "Opening file: " . $this->file . "\n";
    $handle = fopen($this->file, 'r');
    if ($handle) {
      $chado = new Chado();
      $chado->setLog($this->file . '.log');

      // Constnat values
      // db: Tripal Genbank Parser/PMID
      $tgp_db_id = $chado->getId('db', array('name' => 'tripal_genbank_parser'), TRUE);
      $pm_db_id = $chado->getId('db', array('name' => 'PMID'), TRUE);

      // cv: Tripal Genbank Parser
      $tgp_cv_id = $chado->getId('cv', array('name' => 'tripal_genbank_parser'), TRUE);

      // cv: Sequence Ontology
      $so_cv_id = $chado->getId('cv', array('name' => 'sequence'));

      // analysis: program, programversion, sourcename
      $tgp_analysis_id = $chado->getId('analysis', array('program' => 'Tripal Genbank Parser', 'programversion' => '4.0', 'sourcename' => 'NCBI'), TRUE);

      //cvterm: region/keywords/genbank_definition/comment/organelle/source/cultivar/clone
      $region_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $so_cv_id, 'name' => 'region'));

      $rtype_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'record_type', 'version' => ''), TRUE);
      $rtype_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $rtype_dbxref_id, 'name' => 'record_type', 'is_obsolete' => 0), TRUE);

      $keywords_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'keywords', 'version' => ''), TRUE);
      $keywords_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $keywords_dbxref_id, 'name' => 'keywords', 'is_obsolete' => 0), TRUE);

      $definition_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'genbank_definition', 'version' => ''), TRUE);
      $definition_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $definition_dbxref_id, 'name' => 'genbank_definition', 'is_obsolete' => 0), TRUE);

      $comment_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'comment', 'version' => ''), TRUE);
      $comment_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $comment_dbxref_id, 'name' => 'comment', 'is_obsolete' => 0), TRUE);

      $organelle_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'organelle', 'version' => ''), TRUE);
      $organelle_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $organelle_dbxref_id, 'name' => 'organelle', 'is_obsolete' => 0), TRUE);

      $src_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'source', 'version' => ''), TRUE);
      $src_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $src_dbxref_id, 'name' => 'source', 'is_obsolete' => 0), TRUE);

      $cultivar_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'cultivar', 'version' => ''), TRUE);
      $cultivar_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $cultivar_dbxref_id, 'name' => 'cultivar', 'is_obsolete' => 0), TRUE);

      $clone_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'clone', 'version' => ''), TRUE);
      $clone_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $clone_dbxref_id, 'name' => 'clone', 'is_obsolete' => 0), TRUE);

      $counter = 0;
      while (($line = fgets($handle)) !== false) {
        // Skip the header
        if ($counter == 0) {
          $counter ++;
          continue;
        }
        $cell = explode ("\t", $line);
        $c1_f_uname = $cell[0];
        $c2_f_name = $cell[1];
        $c3_fp_def = $cell[2];
        $c4_fp_kwords = $cell[3];
        $c5_f_org = explode(' ', $cell[4], 2);
        $c6_fp_ref = $cell[5];
        $c7_fp_pubmed = $cell[6];
        $c8_fp_comment = $cell[7];
        $c9_fp_organelle = $cell[8];
        $c10_fp_cultivar = $cell[9];
        $c11_lp_tissue_tyep = $cell[10];
        $c12_l_name = $cell[11];
        $c13_clone = $cell[12];
        $c14_vector_type = $cell[13];
        $c15_other = $cell[14];
        $c16_f_type = $cell[15];
        $c17_fp_value = $cell[16];
        $c18_f_residue = $cell[17];

        try {

          // C5 organism: genus species
          $organism_id = $chado->getFirstField('organism', 'organism_id', ['genus' => $c5_f_org[0], 'species' => $c5_f_org[1]], [], TRUE);

          // C1, C2, C18 feature: name, uniquename, residues
          $feature_id = $chado->getId('feature', array(
            'name' => $c2_f_name,
            'residues' => $c18_f_residue,
            'uniquename' => $c1_f_uname,
            'organism_id' => $organism_id,
            'type_id' => $region_cvterm_id),
            TRUE);
          $analysisfeature_id = $chado->getId('analysisfeature', array('analysis_id' => $tgp_analysis_id, 'feature_id' => $feature_id), TRUE);

          // C16 featureprop: record_type
          if ($c16_f_type == 'GSS' || $c16_f_type == 'EST') {
            $rtype_fp_id = $chado->getId('featureprop', array('feature_id' => $feature_id, 'type_id' => $rtype_cvterm_id, 'value' => $c16_f_type, 'rank' => 0));
          }

          // C4 featureprop: keywords
          if (trim($c4_fp_kwords) != '.') {
            $keywords_fprop_id = $chado->getId('featureprop', array(
              'feature_id' => $feature_id,
              'type_id' => $keywords_cvterm_id,
              'value' => $c4_fp_kwords,
              'rank' => 0
            ), TRUE);
          }

          // C3 featureprop: definition
          if (trim($c3_fp_def) != '.') {
            $definition_fprop_id = $chado->getId('featureprop', array(
              'feature_id' => $feature_id,
              'type_id' => $definition_cvterm_id,
              'value' => $c3_fp_def,
              'rank' => 0
            ), TRUE);
          }

          // C8 featureprop: comment
          if (trim($c8_fp_comment) != '.') {
            $comment_fprop_id = $chado->getId('featureprop', array(
              'feature_id' => $feature_id,
              'type_id' => $comment_cvterm_id,
              'value' => $c8_fp_comment,
              'rank' => 0
            ), TRUE);
          }

          // C9 featureprop: organelle
          if (trim($c9_fp_organelle) != '.') {
            $organelle_fprop_id = $chado->getId('featureprop', array(
              'feature_id' => $feature_id,
              'type_id' => $organelle_cvterm_id,
              'value' => $c9_fp_organelle,
              'rank' => 0
            ), TRUE);
          }

          // C17 featureprop: source
          if (trim($c17_fp_value) != '.') {
            $src_fprop_id = $chado->getId('featureprop', array(
              'feature_id' => $feature_id,
              'type_id' => $src_cvterm_id,
              'value' => $c17_fp_value,
              'rank' => 0
            ), TRUE);
          }

          // C10 stock: cultivar
          if (trim($c10_fp_cultivar) != '.') {
            $cultivar_stock_id = $chado->getId('stock', array(
              'organism_id' => $organism_id,
              'type_id' => $cultivar_cvterm_id,
              'uniquename' => $c10_fp_cultivar,
              'name' => $c10_fp_cultivar
            ), TRUE);
          }

          // C12 library/library_feature: clone
          if (trim($c12_l_name) != '.') {
            $clone_library_id = $chado->getId('library', array(
              'organism_id' => $organism_id,
              'type_id' => $clone_cvterm_id,
              'uniquename' => $c12_l_name,
              'name' => $c12_l_name
            ), TRUE);
            if ($clone_library_id) {
              $libfeature_id = $chado->getId('library_feature', array('library_id' => $clone_library_id, 'feature_id' => $feature_id), TRUE);
            }
          }

          // C13 featureprop: clone
          if (trim($c13_clone) != '.') {
            $clone_fprop_id = $chado->getId('featureprop', array(
              'feature_id' => $feature_id,
              'type_id' => $clone_cvterm_id,
              'value' => $c13_clone,
              'rank' => 0
            ), TRUE);
          }

          // C7 dbxref/pub_dbxref/feature_pub: pubmed_id (associate existing pub. not insert new pub)
          $pmeds = explode('|', $c7_fp_pubmed);
          foreach ($pmeds AS $pmed) {
            $token = explode(':', $pmed);
            $pmid = isset($token[1]) ? trim($token[1]) : NULL;
            if ($pmid) {
              $pm_dbxref_id = $chado->getId('dbxref', array('db_id' => $pm_db_id, 'accession' => $pmid, 'version' => ''));
              if ($pm_dbxref_id) {
                $pub_id = $chado->getFirstField('pub_dbxref', 'pub_id', array('dbxref_id' => $pm_dbxref_id));
                if ($pub_id) {
                  $featurepub_id = $chado->getId('feature_pub', array('feature_id' => $feature_id, 'pub_id' => $pub_id), TRUE);
                }
              }
            }
          }

        }
        catch (\Exception $e) {
          print '[Error at Line ' . ($counter + 1) . '] ';
          print $e->getMessage() . "\n";
        }

        $counter ++;
      }
      fclose($handle);
    }
    else {
      return "Can not open file $this->file\n";
    }

    // set the status of the job (in the node not the tripal jobs)
    if ($loaded_without_errors) {
      $status = 'Loading Completed Successfully';
    }
    else {
      $status = 'Errors Encountered';
    }
    return $status;
  }
}