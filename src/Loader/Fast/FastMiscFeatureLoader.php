<?php

namespace Drupal\tripal_genbank_parser\Loader\Fast;

/**
 * Class MiscFeatureLoader loads misc feature results into a chado database
 *
 */

class FastMiscFeatureLoader {

  public $file;

  public function __construct($file) {
    $this->file = $file;
  }

  public function loadData () {
    $loaded_without_errors = TRUE;

    print "Opening file: " . $this->file . "\n";
    $handle = fopen($this->file, 'r');
    if ($handle) {
      $chado = new Chado();
      $chado->setLog($this->file . '.log');

      // Constnat values
      // db: Tripal Genbank Parser
      $tgp_db_id = $chado->getId('db', array('name' => 'tripal_genbank_parser'), TRUE);

      // cv: Sequence Ontology
      $so_cv_id = $chado->getId('cv', array('name' => 'sequence'));

      // cv: Tripal Genbank Parser
      $tgp_cv_id = $chado->getId('cv', array('name' => 'tripal_genbank_parser'), TRUE);

      // cvterm: region/source/genbank_note/product/function
      $region_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $so_cv_id, 'name' => 'region'));

      $src_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'source', 'version' => ''), TRUE);
      $src_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $src_dbxref_id, 'name' => 'source', 'is_obsolete' => 0), TRUE);

      $gb_note_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'genbank_note', 'version' => ''), TRUE);
      $gb_note_cvterm_id = $chado->getId('cvterm', array('name' => 'genbank_note', 'cv_id' => $tgp_cv_id, 'dbxref_id' => $gb_note_dbxref_id, 'is_obsolete' => 0), TRUE);

      $gbg_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'genbank_gene', 'version' => ''), TRUE);
      $gbg_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $gbg_dbxref_id, 'name' => 'genbank_gene', 'is_obsolete' => 0), TRUE);

      $product_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'product', 'version' => ''), TRUE);
      $product_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $product_dbxref_id, 'name' => 'product', 'is_obsolete' => 0), TRUE);

      $func_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'function', 'version' => ''), TRUE);
      $func_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $func_dbxref_id, 'name' => 'function', 'is_obsolete' => 0), TRUE);


      // analysis: program, programversion, sourcename
      $tgp_analysis_id = $chado->getId('analysis', array('program' => 'Tripal Genbank Parser', 'programversion' => '4.0', 'sourcename' => 'NCBI'), TRUE);

      $counter = 0;
      while (($line = fgets($handle)) !== false) {
        // Skip the header
        if ($counter == 0) {
          $counter ++;
          continue;
        }
        $cell = explode ("\t", $line);
        $c1_f_uname = $cell[0];
        $c2_f_name = $cell[1];
        $c3_f_type_id = $cell[2];
        $type = str_replace(
          array(
            'mobile_element',
            'polyA_signal',
            'sig_peptide',
            'misc_RNA',
            'TATA_signal',
            'primer_bind',
            'LTR',
            'GC_signal',
            'rep_origin',
            '-35_signal',
            '-10_signal',
            'protein_bind',
            'assembly_gap'
          ),
          array(
            'mobile_genetic_element',
            'polyA_signal_sequence',
            'signal_peptide',
            'RNA',
            'TATA_box',
            'primer_binding_site',
            'long_terminal_repeat',
            'GC_rich_promoter_region',
            'origin_of_replication',
            'minus_35_signal',
            'minus_10_signal',
            'protein_binding_site',
            'gap'
          ),
          $c3_f_type_id
        );
        $c4_fl_srcf_id = $cell[3];
        $c5_fl_fmin = $cell[4];
        $c6_fl_fmax = $cell[5];
        $c7_fl_min_partial = $cell[6];
        $c8_fl_max_partial = $cell[7];
        $c9_fl_strand = $cell[8];
        $c10_fp_v_gene = $cell[9];
        $c11_fp_v_prod = $cell[10];
        $c12_fp_v_func = $cell[11];
        $c13_fp_v_note = $cell[12];
        $c14_fp_value = $cell[13];
        $c15_other = $cell[14];

        try {

          // feature type
          $seq_type_id = $chado->getFirstField('cvterm', 'cvterm_id', array('name' => $type, 'cv_id' => $so_cv_id));
          // skip unsupported type
          if (!$seq_type_id) {
            //print "unsupported type: $type \n";
            continue;
          }

          // C4
          $srcfeature_id = $chado->getFirstField('feature', 'feature_id', array('uniquename' => $c4_fl_srcf_id, 'type_id' => $region_cvterm_id));
          // Ignore genbank records that are without a sequence
          if (!$srcfeature_id) {
            continue;
          }
          $srcorg_id = $chado->getFirstField('feature', 'organism_id', array('uniquename' => $c4_fl_srcf_id, 'type_id' => $region_cvterm_id));

          // C1, C2 feature: uniquename, name
          $feature_id = $chado->getId('feature', array(
            'organism_id' => $srcorg_id,
            'uniquename' => $c1_f_uname,
            'name' => $c2_f_name,
            'type_id' => $seq_type_id,
          ), TRUE);

          // C5, C6, C7, C8, C9 featureloc
          // Get the max rank
          $featureloc_rank = $chado->getFirstField(
            'featureloc',
            'rank',
            array(
              'feature_id' => $feature_id,
              'locgroup' => 0,
            ),
            array('desc' => 'rank')
          );
          // calculate rank accordingly
          if (is_numeric($featureloc_rank)) {
            $rank = $featureloc_rank + 1;
          }
          else {
            $rank = 0;
          }
          //Find featureloc
          $featureloc_id = $chado->getFirstField(
            'featureloc',
            'featureloc_id',
            array(
              'srcfeature_id' => $srcfeature_id,
              'feature_id' => $feature_id,
              'fmin' => $c5_fl_fmin < $c6_fl_fmax ? $c5_fl_fmin : $c6_fl_fmax,
              'fmax' => $c5_fl_fmin < $c6_fl_fmax ? $c6_fl_fmax : $c5_fl_fmin,
              'strand' => $c9_fl_strand
            )
          );
          // Add featureloc if it does not already exist
          if (!$featureloc_id && $srcfeature_id && $c5_fl_fmin && $c6_fl_fmax) {
            $featureloc_id = $chado->setField(
              'featureloc',
              array(
                'srcfeature_id' => $srcfeature_id,
                'feature_id' => $feature_id,
                'fmin' => $c5_fl_fmin < $c6_fl_fmax ? $c5_fl_fmin : $c6_fl_fmax,
                'fmax' => $c5_fl_fmin < $c6_fl_fmax ? $c6_fl_fmax : $c5_fl_fmin,
                'is_fmin_partial' => $c5_fl_fmin < $c6_fl_fmax ? $c7_fl_min_partial : $c8_fl_max_partial,
                'is_fmax_partial' => $c5_fl_fmin < $c6_fl_fmax ? $c8_fl_max_partial : $c7_fl_min_partial,
                'strand' => $c9_fl_strand,
                'rank' => $rank
              ),
              'featureloc_id'
            );
          }

          // C13 featureprop: genbank_note
          if (trim($c13_fp_v_note) != '.') {
            $gb_note_fp_id = $chado->getId('featureprop', array('feature_id' => $feature_id, 'type_id' => $gb_note_cvterm_id, 'value' => $c13_fp_v_note, 'rank' => 0), TRUE);
          }

          // C14 featureprop: source
          if (trim($c14_fp_value) != '.') {
            $src_fprop_id = $chado->getId('featureprop', array(
              'feature_id' => $feature_id,
              'type_id' => $src_cvterm_id,
              'value' => $c14_fp_value,
              'rank' => 0
            ), TRUE);
          }

          // analysisfeature
          $chado->getId('analysisfeature', array('analysis_id' => $tgp_analysis_id, 'feature_id' => $feature_id), TRUE);

          // C10 featureprop: gene
          if (trim($c10_fp_v_gene) != '.') {
            $gbg_fp_id = $chado->getId('featureprop', array('feature_id' => $feature_id, 'type_id' => $gbg_cvterm_id, 'value' => $c10_fp_v_gene, 'rank' => 0), TRUE);
          }

          // C11 featureprop: product
          if (trim($c11_fp_v_prod) != '.') {
            $product_fp_id = $chado->getId('featureprop', array('feature_id' => $feature_id, 'type_id' => $product_cvterm_id, 'value' => $c11_fp_v_prod, 'rank' => 0), TRUE);
          }

          // C12 featureprop: function
          if (trim($c12_fp_v_func) != '.') {
            $func_fp_id = $chado->getId('featureprop', array('feature_id' => $feature_id, 'type_id' => $func_cvterm_id, 'value' => $c12_fp_v_func, 'rank' => 0), TRUE);
          }
        }
        catch (\Exception $e) {
          print '[Error at Line ' . ($counter + 1) . '] ';
          print $e->getMessage() . "\n";
        }

        $counter ++;
      }
      fclose($handle);
    }
    else {
      return "Can not open file $this->file\n";
    }


    // set the status of the job (in the node not the tripal jobs)
    if ($loaded_without_errors) {
      $status = 'Loading Completed Successfully';
    }
    else {
      $status = 'Errors Encountered';
    }
    return $status;
  }
}