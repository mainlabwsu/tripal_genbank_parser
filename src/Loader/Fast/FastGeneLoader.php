<?php

namespace Drupal\tripal_genbank_parser\Loader\Fast;

/**
 * Class GeneLoader loads gene results into a chado database
 *
 */

class FastGeneLoader {

  public $file;

  public function __construct($file) {
    $this->file = $file;
  }

  public function loadData () {
    $loaded_without_errors = TRUE;

    print "Opening file: " . $this->file . "\n";
    $handle = fopen($this->file, 'r');
    if ($handle) {
      $chado = new Chado();
      $chado->setLog($this->file . '.log');

      // Constnat values
      // db: Tripal Genbank Parser
      $tgp_db_id = $chado->getId('db', array('name' => 'tripal_genbank_parser'), TRUE);
      $geneid_db_id = $chado->getId('db', array(
        'name' => 'GeneID',
        'url' => 'http://www.ncbi.nlm.nih.gov/gene',
        'urlprefix' => 'http://www.ncbi.nlm.nih.gov/gene/',
        'description' => 'Gene integrates information from a wide range of species.'
      ), TRUE);
      $nucleotide_db_id = $chado->getId('db', array(
        'name' => 'nuccore',
        'url' => 'http://www.ncbi.nlm.nih.gov/nuccore',
        'urlprefix' => 'http://www.ncbi.nlm.nih.gov/nuccore/',
        'description' => 'The Nucleotide database is a collection of sequences from several sources.'),
        TRUE
      );

      // cv: Sequence Ontology
      $so_cv_id = $chado->getId('cv', array('name' => 'sequence'));

      // cv: Tripal Genbank Parser
      $tgp_cv_id = $chado->getId('cv', array('name' => 'tripal_genbank_parser'), TRUE);

      // cvterm: gene/region/genbank_note/source
      $gene_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $so_cv_id, 'name' => 'gene'));
      $region_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('cv_id' => $so_cv_id, 'name' => 'region'));

      $gb_note_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'genbank_note', 'version' => ''), TRUE);
      $gb_note_cvterm_id = $chado->getId('cvterm', array('name' => 'genbank_note', 'cv_id' => $tgp_cv_id, 'dbxref_id' => $gb_note_dbxref_id, 'is_obsolete' => 0), TRUE);

      $src_dbxref_id = $chado->getId('dbxref', array('db_id' => $tgp_db_id, 'accession' => 'source', 'version' => ''), TRUE);
      $src_cvterm_id = $chado->getId('cvterm', array('cv_id' => $tgp_cv_id, 'dbxref_id' => $src_dbxref_id, 'name' => 'source', 'is_obsolete' => 0), TRUE);

      // analysis: program, programversion, sourcename
      $tgp_analysis_id = $chado->getId('analysis', array('program' => 'Tripal Genbank Parser', 'programversion' => '4.0', 'sourcename' => 'NCBI'), TRUE);

      $counter = 0;
      while (($line = fgets($handle)) !== false) {
        // Skip the header
        if ($counter == 0) {
          $counter ++;
          continue;
        }
        $cell = explode ("\t", $line);
        $c1_f_uname = $cell[0];
        $c2_f_name = $cell[1];
        $c3_f_type = $cell[2];
        $c4_fl_srcf_id = $cell[3];
        $c5_fl_fmin = $cell[4] == '.' ? NULL : $cell[4];
        $c6_fl_fmax = $cell[5] == '.' ? NULL : $cell[5];
        $c7_fl_min_partial = $cell[6] == '.' ? NULL : $cell[6];
        $c8_fl_max_partial = $cell[7] == '.' ? NULL : $cell[7];
        $c9_fl_strand = $cell[8] == '.' ? NULL : $cell[8];
        $c10_fd_fd_id = explode(':', $cell[9], 2);
        $geneid = key_exists(1, $c10_fd_fd_id) ? trim($c10_fd_fd_id[1]) : NULL;
        $c11_fr_obj_id = $cell[10];
        $c12_fr_type_id = $cell[11];
        $c13_fp_value = $cell[12];
        $c14_fl_fmin_max = $cell[13];
        $c15_other = $cell[14];
        $c16_fp_value = $cell[15];
        $c17_f_residue = $cell[16];

        try {

          // C4
          $srcfeature_id = $chado->getFirstField('feature', 'feature_id', array('uniquename' => $c4_fl_srcf_id, 'type_id' => $region_cvterm_id));
          // Ignore genbank records that are without a sequence
          if (!$srcfeature_id) {
            continue;
          }
          $srcorg_id = $chado->getFirstField('feature', 'organism_id', array('uniquename' => $c4_fl_srcf_id, 'type_id' => $region_cvterm_id));

          // C1, C2, C17 feature: uniquename, name, residues
          $feature_id = $chado->getId('feature', array(
            'organism_id' => $srcorg_id,
            'uniquename' => $c1_f_uname,
            'name' => $c2_f_name,
            'type_id' => $gene_cvterm_id,
            'residues' => $c17_f_residue
          ), TRUE);

          // C10 dbxref: accession
          if ($geneid) {
            $geneid_dbxref_id = $chado->getId('dbxref', array('db_id' => $geneid_db_id, 'accession' => $geneid, 'version' => ''), TRUE);
            $feature_dbxref_id = $chado->getId('feature_dbxref', array('feature_id' => $feature_id, 'dbxref_id' => $geneid_dbxref_id), TRUE);
          }

          // C5, C6, C7, C8, C9 featureloc
          // Get the max rank
          $featureloc_rank = $chado->getFirstField(
            'featureloc',
            'rank',
            array(
              'feature_id' => $feature_id,
              'locgroup' => 0,
            ),
            array('desc' => 'rank')
          );
          // calculate rank accordingly
          if (is_numeric($featureloc_rank)) {
            $rank = $featureloc_rank + 1;
          }
          else {
            $rank = 0;
          }
          //Find featureloc
          $featureloc_id = $chado->getFirstField(
            'featureloc',
            'featureloc_id',
            array(
              'srcfeature_id' => $srcfeature_id,
              'feature_id' => $feature_id,
              'fmin' => $c5_fl_fmin < $c6_fl_fmax ? $c5_fl_fmin : $c6_fl_fmax,
              'fmax' => $c5_fl_fmin < $c6_fl_fmax ? $c6_fl_fmax : $c5_fl_fmin,
              'strand' => $c9_fl_strand
            )
          );
          // Add featureloc if it does not already exist
          if (!$featureloc_id && $srcfeature_id && $c5_fl_fmin && $c6_fl_fmax) {
            $featureloc_id = $chado->setField(
              'featureloc',
              array(
                'srcfeature_id' => $srcfeature_id,
                'feature_id' => $feature_id,
                'fmin' => $c5_fl_fmin < $c6_fl_fmax ? $c5_fl_fmin : $c6_fl_fmax,
                'fmax' => $c5_fl_fmin < $c6_fl_fmax ? $c6_fl_fmax : $c5_fl_fmin,
                'is_fmin_partial' => $c5_fl_fmin < $c6_fl_fmax ? $c7_fl_min_partial : $c8_fl_max_partial,
                'is_fmax_partial' => $c5_fl_fmin < $c6_fl_fmax ? $c8_fl_max_partial : $c7_fl_min_partial,
                'strand' => $c9_fl_strand,
                'rank' => $rank
              ),
              'featureloc_id'
            );
          }

          // C13 featureprop: genbank_note
          if (trim($c13_fp_value) != '.') {
            $gb_note_fp_id = $chado->getId('featureprop', array('feature_id' => $feature_id, 'type_id' => $gb_note_cvterm_id, 'value' => $c13_fp_value, 'rank' => 0), TRUE);
          }

          // C11, C12 feature_relationship (associated_with generic_gene): object_id
          $object_id = $chado->getId('feature', array('uniquename' => $c11_fr_obj_id, 'type_id' => $gene_cvterm_id, 'organism_id' => $srcorg_id));
          $assoc_cvterm_id = $chado->getFirstField('cvterm', 'cvterm_id', array('name' => $c12_fr_type_id, 'cv_id' => $so_cv_id));
          if ($object_id && $assoc_cvterm_id) {
            $fr_id = $chado->getId('feature_relationship', array('subject_id' => $feature_id, 'object_id' => $object_id, 'type_id' => $assoc_cvterm_id, 'rank' => 0), TRUE);
          }

          // C4 as dbxref/feature_dbxref: accession
          $nucleotide_dbxref_id = $chado->getId('dbxref', array('accession' => $c4_fl_srcf_id, 'db_id' => $nucleotide_db_id, 'version' => ''), TRUE);
          $fdbxref_id = $chado->getId('feature_dbxref', array('feature_id' => $feature_id, 'dbxref_id' => $nucleotide_dbxref_id), TRUE);

          // C16 featureprop: source
          if (trim($c16_fp_value) != '.') {
            $src_fprop_id = $chado->getId('featureprop', array(
              'feature_id' => $feature_id,
              'type_id' => $src_cvterm_id,
              'value' => $c16_fp_value,
              'rank' => 0
            ), TRUE);
          }

          // analysisfeature
          $chado->getId('analysisfeature', array('analysis_id' => $tgp_analysis_id, 'feature_id' => $feature_id), TRUE);

        }
        catch (\Exception $e) {
          print '[Error at Line ' . ($counter + 1) . '] ';
          print $e->getMessage() . "\n";
        }

        $counter ++;
      }
      fclose($handle);
    }
    else {
      return "Can not open file $this->file\n";
    }


    // set the status of the job (in the node not the tripal jobs)
    if ($loaded_without_errors) {
      $status = 'Loading Completed Successfully';
    }
    else {
      $status = 'Errors Encountered';
    }
    return $status;
  }

}