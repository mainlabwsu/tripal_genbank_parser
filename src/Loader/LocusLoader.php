<?php

namespace Drupal\tripal_genbank_parser\Loader;

/**
 * Class LocusLoader loads locus results into a chado database
 *
 */

use Drupal\tripal_genbank_parser\Loader\Template\LocusTemplate;

class LocusLoader extends BulkLoader{

  public function __construct($file, $db) {
    $this->template = new LocusTemplate($db);
    $this->file = $file;
  }

}