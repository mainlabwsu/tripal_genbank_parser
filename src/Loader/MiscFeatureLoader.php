<?php

namespace Drupal\tripal_genbank_parser\Loader;

/**
 * Class MiscFeatureLoader loads misc feature results into a chado database
 *
 */

use Drupal\tripal_genbank_parser\Loader\Template\MiscFeatureTemplate;

class MiscFeatureLoader extends BulkLoader {

  public function __construct($file) {
    $this->template = new MiscFeatureTemplate();
    $this->file = $file;
  }

}