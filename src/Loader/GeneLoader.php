<?php

namespace Drupal\tripal_genbank_parser\Loader;

/**
 * Class GeneLoader loads gene results into a chado database
 *
 */

use Drupal\tripal_genbank_parser\Loader\Template\GeneTemplate;

class GeneLoader extends BulkLoader{

  public function __construct($file) {
    $this->template = new GeneTemplate();
    $this->file = $file;
  }

}