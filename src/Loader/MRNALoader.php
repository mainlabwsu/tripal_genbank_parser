<?php

namespace Drupal\tripal_genbank_parser\Loader;

/**
 * Class MRNALoader loads mRNA results into a chado database
 *
 */

use Drupal\tripal_genbank_parser\Loader\Template\MRNATemplate;

class MRNALoader extends BulkLoader {

  public function __construct($file) {
    $this->template = new MRNATemplate();
    $this->file = $file;
  }

}