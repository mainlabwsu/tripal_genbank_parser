<?php

namespace Drupal\tripal_genbank_parser\Sequence;

/**
 * Class DNA represents a DNA sequence.
 *
 * DNA contains functions such as reverse and complement
 */

class DNA extends Sequence {

  // Constructor
  public function __construct($sequence) {
    parent::__construct($sequence);
  }

  // Complement the sequence
  public function complement() {
    $complementSequence = "";
    for ($i = 0; $i < strlen($this->sequence); $i ++) {
      $c = $this->sequence [$i];
      if ($c == 'A') {
        $c = 'T';
      }
      else if ($c == 'T') {
        $c = 'A';
      }
      else if ($c == 'G') {
        $c = 'C';
      }
      else if ($c == 'C') {
        $c = 'G';
      }
      else if ($c == 'a') {
        $c = 't';
      }
      else if ($c == 't') {
        $c = 'a';
      }
      else if ($c == 'g') {
        $c = 'c';
      }
      else if ($c == 'c') {
        $c = 'g';
      }
      $complementSequence .= $c;
    }
    return new DNA($complementSequence);
  }

  // Reverse the sequence
  public function reverse() {
    $reversedSequence = "";
    for ($i = 1; $i <= strlen($this->sequence); $i ++) {
      $reversedSequence .= $this->sequence [strlen($this->sequence) - $i];
    }
    return new DNA($reversedSequence);
  }

  // Reverse complement the sequence
  public function reverseComplement() {
    $dna = $this->reverse();
    return $dna->complement();
  }
}