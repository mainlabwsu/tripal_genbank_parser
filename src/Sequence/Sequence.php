<?php

namespace Drupal\tripal_genbank_parser\Sequence;

/**
 * Class Sequence represents a biological sequence
 *
 * Sequence is the superclass of other biological sequences
 * such as DNA or protein sequences
 */
class Sequence {
  protected $sequence;

  // Constructor
  public function __construct($sequence) {
    $this->sequence = trim($sequence);
  }

  // Get sequence length
  public function getLength() {
    return strlen($this->sequence);
  }

  // Convert Sequence to String
  public function toString() {
    return $this->sequence;
  }

  // Extract fragment of a sequence using 'start..stop' format for the location
  public static function getFragment($location, $srcSeq) {
    $token_loc = explode("..", $location, 2);
    // If it's a single base notation
    if (count($token_loc) == 1) {
      array_push($token_loc, $token_loc[0]);
    }
    return substr($srcSeq, $token_loc [0] - 1, $token_loc [1] - $token_loc [0] + 1);
  }
}
