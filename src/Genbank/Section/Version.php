<?php

namespace Drupal\tripal_genbank_parser\Genbank\Section;

/**
 * Class Version represents the VERSION section in a Genbank record
 *
 * Instantiate this class by passing in the reference of a Genbank Object.
 * Values stored in this section will be parsed and stored in the passed object.
 */
class Version {
  public function __construct($gb_obj) {
    $this->parseVersion($gb_obj);
  }
  private function parseVersion($gb_obj) {
    $ver = preg_split("/\s+/", $gb_obj->getSection('VERSION'), 2);
    // Version (i.e. Accession.ver)
    $gb_obj->setVersion($ver [0]);
    // GI is phasing out. Some records might not have GI in the future
    if (key_exists(1, $ver)) {
      $gb_obj->setGi($ver [1]);
    }
  }
}