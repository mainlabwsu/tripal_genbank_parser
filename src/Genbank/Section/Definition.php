<?php

namespace Drupal\tripal_genbank_parser\Genbank\Section;

/**
 * Class Definition represents the DEFINITION section in a Genbank record
 *
 * Instantiate this class by passing in the reference of a Genbank Object.
 * Values stored in this section will be parsed and stored in the passed object.
 */
class Definition {
  public function __construct($gb_obj) {
    $this->parseDefinition($gb_obj);
  }
  private function parseDefinition($gb_obj) {
    $value = $gb_obj->getSection('DEFINITION');
    $gb_obj->setDefinition($value);
  }
}