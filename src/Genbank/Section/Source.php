<?php

namespace Drupal\tripal_genbank_parser\Genbank\Section;

/**
 * Class Source represents the SOURCE section in a Genbank record
 *
 * Instantiate this class by passing in the reference of a Genbank Object.
 * Values stored in this section will be parsed and stored in the passed object.
 */
class Source {
  public function __construct($gb_obj) {
    $this->parseSource($gb_obj);
  }
  private function parseSource($gb_obj) {
    $value = $gb_obj->getSection('SOURCE');
    $gb_obj->setSource($value);
  }
}