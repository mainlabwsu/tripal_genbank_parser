<?php

namespace Drupal\tripal_genbank_parser\Genbank\Section;

use Drupal\tripal_genbank_parser\Genbank\Record\Feature;
use Drupal\tripal_genbank_parser\Util\Str;

/**
 * Class Features represents the FEATURES section in a Genbank record
 *
 * Instantiate this class by passing in the reference of a Genbank Object.
 * Values stored in this section will be parsed and stored in the passed object.
 */
class Features {
  public function __construct($gb_obj) {
    $this->parseFeatures($gb_obj);
  }
  private function parseFeatures($gb_obj) {
    $lines = explode("\n", $gb_obj->getSection('FEATURES'));
    $results = array ();
    $featureType = "";
    $f = null;
    $newFeatures = null;
    foreach ($lines as $line) {
      if ($line == "Location/Qualifiers") { // ignore "Location/Qualifiers" line
        continue;
      }
      $featureKey = trim(substr($line, 5, 15));  // Feature key starts from position 6 - 20
      $featureData = trim(substr($line, 21));  // Other information starts from position 22
      if ($featureKey != "") { // This is a feature
        if ($featureType != "") { // process previous feature
          if (key_exists($featureType, $results)) {
            array_push($results [$featureType], $f);
            $results [$featureType] = $results [$featureType];
          }
          else {
            $newFeatures = array ();
            array_push($newFeatures, $f);
            $results [$featureType] = $newFeatures;
          }
        }
        $featureType = $featureKey;
        $f = new Feature(); // Create a new feature
        $f->setFeatureLoc($featureData);
        $stack = array ();
        $f->setFeatureProp($stack);
      }
      else { // This is either a extended line or a FeatureProp
        if (! Str::startsWith($featureData, "/")) { // for extended line: append lines that are not started with '/'
          if ($f->getFeatureProp() == null || count($f->getFeatureProp()) == 0) { // append to FeatureLoc
            $f->setFeatureLoc($f->getFeatureLoc() . " " . trim($featureData));
          }
          else { // append to FeatureProp
            $prop = $f->getFeatureProp();
            $obj = array_pop($prop) . " " . trim($featureData);
            array_push($prop, $obj);
            $f->setFeatureProp($prop);
          }
        }
        else { // for a new FeatureProp
          $prop = $f->getFeatureProp();
          array_push($prop, $featureData);
          $f->setFeatureProp($prop);
        }
      }
    }
    // For the last feature
    if (key_exists($featureType, $results)) {
      array_push($results [$featureType], $f);
      $results [$featureType] = $results [$featureType];
    }
    else {
      $newFeatures = array ();
      array_push($newFeatures, $f);
      $results [$featureType] = $newFeatures;
    }
    $gb_obj->setFeatures($results);
  }
}