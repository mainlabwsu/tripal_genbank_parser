<?php

namespace Drupal\tripal_genbank_parser\Genbank\Section;

/**
 * Class Accessions represents all accesssions in the ACCESSION section of a Genbank record
 *
 * Instantiate this class by passing in the reference of a Genbank Object.
 * Values stored in this section will be parsed and stored in the passed object.
 */
class Accessions {
  public function __construct($gb_obj) {
    $this->parseAccessions($gb_obj);
  }
  private function parseAccessions($gb_obj) {
    $value = $gb_obj->getSection('ACCESSION');
    $gb_obj->setAccessions($value);
  }
}