<?php

namespace Drupal\tripal_genbank_parser\Genbank\Section;

/**
 * Class Comment represents the COMMENT section in a Genbank record
 *
 * Instantiate this class by passing in the reference of a Genbank Object.
 * Values stored in this section will be parsed and stored in the passed object.
 */
class Comment {
  public function __construct($gb_obj) {
    $this->parseComment($gb_obj);
  }
  private function parseComment($gb_obj) {
    $value = $gb_obj->getSection('COMMENT');
    $gb_obj->setComment($value);
  }
}