<?php

namespace Drupal\tripal_genbank_parser\Genbank\Section;

/**
 * Class Organism represents the ORGANISM section in a Genbank record
 *
 * Instantiate this class by passing in the reference of a Genbank Object.
 * Values stored in this section will be parsed and stored in the passed object.
 */
class Organism {
  public function __construct($gb_obj) {
    $this->parseOrganism($gb_obj);
  }
  private function parseOrganism($gb_obj) {
    $line = preg_split("/\n/", $gb_obj->getSection('ORGANISM'), 2);
    // Organism.
    $gb_obj->setOrganism(trim($line [0]));
    // Organism tree.
    $otree = key_exists(1, $line)? trim(preg_replace("/\s+/", " ", $line [1])) : '';
    $gb_obj->setOrganismTree($otree);
  }
}