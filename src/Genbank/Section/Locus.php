<?php

namespace Drupal\tripal_genbank_parser\Genbank\Section;

/**
 * Class Locus represents the LOCUS section in a Genbank record
 *
 * Instantiate this class by passing in the reference of a Genbank Object.
 * Values stored in this section will be parsed and stored in the passed object.
 */
class Locus {
  public function __construct($gb_obj) {
    $this->parseLocus($gb_obj);
  }
  private function parseLocus($gb_obj) {
    $fields = array ();
    $fields = preg_split("/\s+/", $gb_obj->getSection('LOCUS'));
    // Locus name.
    $gb_obj->setLocusName($fields [0]);
    // Sequence length.
    $gb_obj->setSequenceLength($fields [1] . " bp");
    // Molecule type.
    $gb_obj->setMoleculeType($fields [3]);

    // Sequence type (optional), Division, and Modification date.
    if (count($fields) == 6) {
      $gb_obj->setGenbankDivision($fields [4]);
      $gb_obj->setModificationDate($fields [5]);
    }
    else if (count($fields) == 7) {
      $gb_obj->setSequenceType($fields [4]);
      $gb_obj->setGenbankDivision($fields [5]);
      $gb_obj->setModificationDate($fields [6]);
    }
  }
}