<?php

namespace Drupal\tripal_genbank_parser\Genbank\Section;

use Drupal\tripal_genbank_parser\Parser\StringParser;

/**
 * Class References represents all REFERENCE sections in a Genbank record
 *
 * Instantiate this class by passing in the reference of a Genbank Object.
 * Values stored in this section will be parsed and stored in the passed object.
 */
class References {

  // Parse the REFERENCEs by these keywords
  private $ref_keys = array (
      "CONSRTM",
      "AUTHORS",
      "TITLE",
      "JOURNAL",
      "PUBMED",
      "REMARK"
  );
  public function __construct($gb_obj) {
    $this->parseReferences($gb_obj);
  }
  private function parseReferences($gb_obj) {
    $refs = preg_split("/REFERENCE/", $gb_obj->getSection('REFERENCE'));
    $results = array ();
    $pt = '/(\(bases \d+ to \d+\))/';
    foreach ($refs as $ref) {

      $info = StringParser::parseKeywords($ref, $this->ref_keys);
      $m = preg_match($pt, $ref, $matches);
      if ($m != 0) {
        $info ['BASES'] = $matches [1];
      }
      array_push($results, $info);
    }
    $gb_obj->setReferences($results);
  }
}