<?php

namespace Drupal\tripal_genbank_parser\Genbank\Section;

/**
 * Class Origin represents the ORIGIN section in a Genbank record
 *
 * Instantiate this class by passing in the reference of a Genbank Object.
 * Values stored in this section will be parsed and stored in the passed object.
 */
class Origin {
  public function __construct($gb_obj) {
    $this->parseOrigin($gb_obj);
  }
  private function parseOrigin($gb_obj) {
    // Remove text between 'ORIGIN' and the real sequence. Also remove number and spaces in the sequence.
    $origin = $gb_obj->getSection('ORIGIN');
    if ($origin) {
      $seq = explode('1 ', $origin, 2);
      // Residues
      $gb_obj->setSequence(preg_replace("/\d+|\s+/", "", $seq [1]));
    }
  }
}