<?php

namespace Drupal\tripal_genbank_parser\Genbank\Section;

/**
 * Class Keyword represents the KEYWORDS section in a Genbank record
 *
 * Instantiate this class by passing in the reference of a Genbank Object.
 * Values stored in this section will be parsed and stored in the passed object.
 */
class Keyword {
  public function __construct($gb_obj) {
    $this->parseKeyword($gb_obj);
  }
  private function parseKeyword($gb_obj) {
    $value = $gb_obj->getSection('KEYWORDS');
    $gb_obj->setKeyword($value);
  }
}