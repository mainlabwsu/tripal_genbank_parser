<?php

namespace Drupal\tripal_genbank_parser\Genbank\Section;

/**
 * Class Dblink represents the DBLINK section in a Genbank record
 *
 * Instantiate this class by passing in the reference of a Genbank Object.
 * Values stored in this section will be parsed and stored in the passed object.
 */
class Dblink {
  public function __construct($gb_obj) {
    $this->parseDblink($gb_obj);
  }
  private function parseDblink($gb_obj) {
    $value = $gb_obj->getSection('DBLINK');
    $gb_obj->setDblink($value);
  }
}