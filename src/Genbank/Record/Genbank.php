<?php

namespace Drupal\tripal_genbank_parser\Genbank\Record;

/**
 * Class Genbank represents a Genbank record
 *
 * Pass a genbank text record (i.e. from LOCUS to //) to instantiate the class.
 * The genbank record will be parsed and stored in class variables. All parsers
 * that convert a Genbank flat-file format into a Genbank object are stored in
 * the '/api/genbank/section' directory which includes classes of singular
 * form representing a single value or plural form representing multiple values
 */

use Drupal\tripal_genbank_parser\Parser\StringParser;
use Drupal\tripal_genbank_parser\Genbank\Section\Locus;
use Drupal\tripal_genbank_parser\Genbank\Section\Definition;
use Drupal\tripal_genbank_parser\Genbank\Section\Accessions;
use Drupal\tripal_genbank_parser\Genbank\Section\Version;
use Drupal\tripal_genbank_parser\Genbank\Section\Dblink;
use Drupal\tripal_genbank_parser\Genbank\Section\Keyword;
use Drupal\tripal_genbank_parser\Genbank\Section\Source;
use Drupal\tripal_genbank_parser\Genbank\Section\Organism;
use Drupal\tripal_genbank_parser\Genbank\Section\References;
use Drupal\tripal_genbank_parser\Genbank\Section\Comment;
use Drupal\tripal_genbank_parser\Genbank\Section\Features;
use Drupal\tripal_genbank_parser\Genbank\Section\Origin;
use Drupal\tripal_genbank_parser\Genbank\Record\Feature;
use Drupal\tripal_genbank_parser\Sequence\DNA;

class Genbank {

  // Parse the Genbank record by these feild keywords
  private $field_key = array (
      "LOCUS",
      "DEFINITION",
      "ACCESSION",
      "VERSION",
      "DBLINK",
      "KEYWORDS",
      "SEGMENT",
      "SOURCE",
      "ORGANISM[!APPEND]",
      "REFERENCE",
      "COMMENT",
      "FEATURES[!APPEND]",
      "WGS    ",
      "WGS_SCAFLD",
      "CONTIG   ",
      "TSA   ",
      "ORIGIN"
  );

  // Genbank major fields as an associated array
  private $gb_flat;

  // In LOCUS Section
  private $locusName;
  private $sequenceLength;
  private $moleculeType;
  private $sequenceType;
  private $genbankDivision;
  private $modificationDate;
  // In DEFINITION Section
  private $definition;
  // In ACCESSION Section
  private $accessions;
  // In DBLINK Section
  private $dblink;
  // In KEYWORDS Section
  private $keyword;
  // In SOURCE Section
  private $source;
  // In VERSION Section
  private $version;
  private $gi;
  // In ORGANISM Section
  private $organism;
  private $organismTree;
  // In REFERENCE Section
  private $references; // consisting of "CONSRTM", "AUTHORS", "TITLE", "JOURNAL", "PUBMED", and "REMARK"
                       // In COMMENT Section
  private $comment;
  // In FEATURES Section
  private $features; // A 2D associated array. Primary key consisting of "source", "gene", "mRNA', "CDS", "exon", "intron", etc. Secondary key is the qualifier starting with a slash '/'. The value is a Featureprop.
                     // In ORIGIN Section
  private $sequence;

  // Constructor
  public function __construct($content) {
    $this->gb_flat = StringParser::parseKeywords($content, $this->field_key);
    new Locus($this);
    new Definition($this);
    new Accessions($this);
    new Version($this);
    new Keyword($this);
    new Source($this);
    new Dblink($this);
    new Organism($this);
    new References($this);
    new Comment($this);
    new Features($this);
    new Origin($this);
  }

  // Retrive the specified section using a keyword
  public function getSection($sectionKey) {
    if (key_exists($sectionKey, $this->gb_flat)) {
      return $this->gb_flat [$sectionKey];
    }
    else {
      return null;
    }
  }

  // Retrieve all available feature types for this genbank record
  public function getFeatureTypes() {
    return array_keys($this->features);
  }

  // Getters and Setters
  public function getLocusName() {
    return $this->locusName;
  }
  public function setLocusName($locusName) {
    $this->locusName = $locusName;
  }
  public function getSequenceLength() {
    return $this->sequenceLength;
  }
  public function setSequenceLength($sequenceLength) {
    $this->sequenceLength = $sequenceLength;
  }
  public function getMoleculeType() {
    return $this->moleculeType;
  }
  public function setMoleculeType($moleculeType) {
    $this->moleculeType = $moleculeType;
  }
  public function getSequenceType() {
    return $this->sequenceType;
  }
  public function setSequenceType($sequenceType) {
    $this->sequenceType = $sequenceType;
  }
  public function getGenbankDivision() {
    return $this->genbankDivision;
  }
  public function setGenbankDivision($genbankDivision) {
    $this->genbankDivision = $genbankDivision;
  }
  public function getModificationDate() {
    return $this->modificationDate;
  }
  public function setModificationDate($modificationDate) {
    $this->modificationDate = $modificationDate;
  }
  public function getDefinition() {
    return $this->definition;
  }
  public function setDefinition($definition) {
    $this->definition = $definition;
  }
  public function getAccessions() {
    return explode(" ", $this->accessions);
  }
  public function setAccessions($accessions) {
    $this->accessions = $accessions;
  }
  public function getVersion() {
    return $this->version;
  }
  public function setVersion($version) {
    $this->version = $version;
  }
  public function getGi() {
    return $this->gi;
  }
  public function setGi($gi) {
    $this->gi = $gi;
  }
  public function getDbLink() {
    return $this->dblink;
  }
  public function setDbLink($dbLink) {
    $this->dblink = $dbLink;
  }
  public function getKeyword() {
    return $this->keyword;
  }
  public function setKeyword($keyword) {
    $this->keyword = $keyword;
  }
  public function getSource() {
    return $this->source;
  }
  public function setSource($source) {
    $this->source = $source;
  }
  public function getOrganism() {
    return $this->organism;
  }
  public function setOrganism($organism) {
    $this->organism = $organism;
  }
  public function getOrganismTree() {
    return $this->organismTree;
  }
  public function setOrganismTree($organismTree) {
    $this->organismTree = $organismTree;
  }
  public function getReferences() {
    return $this->references;
  }
  public function setReferences($references) {
    $this->references = $references;
  }
  public function getFeatures($featureType) {
    if (key_exists($featureType, $this->features)) {
      return $this->features [$featureType];
    }
    else {
      return null;
    }
  }
  public function setFeatures($features) {
    $this->features = $features;
  }
  public function getComment() {
    return $this->comment;
  }
  public function setComment($comment) {
    $this->comment = $comment;
  }
  public function getSequence() {
    return $this->sequence;
  }
  public function setSequence($sequence) {
    $this->sequence = $sequence;
  }
}
