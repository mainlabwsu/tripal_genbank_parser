<?php

namespace Drupal\tripal_genbank_parser\Genbank\Record;

use Drupal\tripal_genbank_parser\Sequence\DNA;

/**
 * Class Feature represents a sequence feature in a Genbank record
 *
 * A feature such as source, gene, CDS, tRNA, misc_feature, etc is a
 * sub-record in the 'FEATURES' section of a Genbank record. It usually
 * starts with coordinates and may contains properties identified by
 * qualifiers such as /organism, /db_xref, /gene etc.
 */
class Feature {
  private $featureLoc;
  private $featureProp;
  public function getFeatureLoc() {
    return $this->featureLoc;
  }
  public function setFeatureLoc($featureLoc) {
    $this->featureLoc = $featureLoc;
  }
  public function getFeatureProp() {
    return $this->featureProp;
  }
  public function setFeatureProp($featureProp) {
    $this->featureProp = $featureProp;
  }

  // Note: if a featureprop tag has more than one values, only the last one will be kept if you call this function
  public static function featurepropToHashMap($featureprops) {
    $result = array ();
    foreach ($featureprops as $str) {
      if (preg_match("/=/", $str)) {
        $token_str = explode("=", $str, 2);
        $result [$token_str [0]] = preg_replace("/\"/", "", $token_str [1]);
      }
    }
    return $result;
  }

  // Compute the sequence for a Feature using its FeatureLoc which contains operators like
  // base1..base2, complement(loc1), join(loc1, loc2), <, > (i.e. partial sequence).
  // Note: order(loc1, loc2) is not handled, nor is the carat (^) operator
  public static function computeSeq($featureLoc, $srcSeq) {
    try {
      $fragments = explode(",", preg_replace("/join|complement|<|>|\(|\)/", "", $featureLoc));
      $k_seq = array ();
      for ($i = 0; $i < count($fragments); $i ++) {
        $k_seq [$fragments [$i]] = $i;
      }
      $seq = preg_replace("/,|<|>/", "", preg_replace("/complement/", "^", preg_replace("/join/", "&", $featureLoc)));
      foreach ($k_seq as $k => $v) {
        $pattern = preg_replace('/\.\./', '\.\.', $k);
        $seq = preg_replace('/' . $pattern . '/', DNA::getFragment($k, $srcSeq), $seq);
      }

      $left_p = array ();
      $right_p = array ();
      for ($i = 0; $i < strlen($seq); $i ++) {
        if ($seq [$i] == '(') {
          array_push($left_p, $i);
        }
        else if ($seq [$i] == ')') {
          array_unshift($right_p, $i);
        }
      }

      // Reverse complement the sequences
      for ($i = 0; $i < count($left_p); $i ++) {
        $pos_left_p = $left_p [$i];
        $pos_right_p = $right_p [$i];
        $op = $seq [$pos_left_p - 1];
        if ($op == '^') {
          $before = substr($seq, $pos_left_p + 1, $pos_right_p - $pos_left_p - 1);
          $dna = new DNA($before);
          $after = $dna->reverseComplement();
          $seq = substr($seq, 0, $pos_left_p + 1) . $after->toString() . substr($seq, $pos_right_p, strlen($seq));
        }
      }
      return preg_replace("/\^|\(|\)|&/", "", $seq);
    }
    catch (Exception $e) {
      print $e;
    }
  }
}