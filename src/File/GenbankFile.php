<?php

namespace Drupal\tripal_genbank_parser\File;

/**
 * Class GenbankFile is a SegmentedFile with records separated by "LOCUS" and "//"
 *
 * Specify the file name to instantiate the class. This class provides methods to read
 * a Genbank flat-file record by record which is represented by a Genbank object. The
 * definition of a Genbank object can be found in 'includes/genbank/record/Genbank.php'.
 */

use Drupal\tripal_genbank_parser\Genbank\Record\Genbank;

class GenbankFile extends SegmentedFile {
  public function __construct($filepath) {
    parent::__construct($filepath, "LOCUS", "//");
  }
  public function readNextGenbank() {
    $record = $this->readNextSegment();
    if ($record != null) {
      return new Genbank($record);
    }
    else {
      return null;
    }
  }
}