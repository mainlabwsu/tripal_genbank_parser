<?php

namespace Drupal\tripal_genbank_parser\File;

/**
 * Class SegmentedFile contains multiple records identified by a start and a stop tag
 *
 * File name, the start tag, and the stop tag should be specified when instantiating
 * a SegmentedFile. This class provides methods to read from a segmented file
 * record by record.
 */

use Drupal\tripal_genbank_parser\Util\Str;

class SegmentedFile {
  protected $segfile;
  protected $segstart;
  protected $segstop;
  protected $content;

  // Constructor.
  public function __construct($filepath, $segstart, $segstop) {
    $this->segfile = fopen($filepath, "r") or die("can't open file\n");
    $this->segstart = $segstart;
    $this->segstop = $segstop;
  }

  // Read next record
  public function readNextSegment() {
    $read = false;
    while (($line = fgets($this->segfile, 4096)) !== false) {
      if (trim($line) != "") { // Ignore empty lines
        if (Str::startsWith(trim($line), $this->segstart)) { // Record starts
          $this->content = ""; // Remove old content
          $read = true;
        }
        else if (Str::startsWith(trim($line), $this->segstop)) {
          $read = false;
          break;
        }
        if ($read) {
          $this->content .= $line;
        }
      }
    }
    if ($line == false) {
      fclose($this->segfile);
      return null;
    }

    if ($this->content == null) {
      return "";
    }
    else {
      $str = trim($this->content);
      $this->content = null;
      return $str;
    }
  }
}
