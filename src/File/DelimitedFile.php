<?php

namespace Drupal\tripal_genbank_parser\File;

/**
 * Class DelimitedFile represents a tab-delimited or any special character delimited file
 *
 * File name, the row of header, and the delimiter should be specified when instantiating
 * a DelimitedFile. This class provides methods to read from/write to a delimited file
 * line by line.
 */
class DelimitedFile {
  private $inputFile = null;
  private $delimiter;
  private $linesBeforeHeader = "";
  private $header = "";

  // Constructor: the lines before header are stored in the class variable $linesBeforeHeader
  public function __construct($inputFile, $headerRow = 0, $delimiter = "\t") {
    $this->delimiter = $delimiter;
    $this->inputFile = fopen($inputFile, "r") or die("can't open file\n");

    for ($i = 0; $i < $headerRow - 1; $i ++) {
      $this->linesBeforeHeader .= trim(fgets($this->inputFile, 4096));
      if ($i != $headerRow - 2) {
        $this->linesBeforeHeader .= "\n";
      }
    }
    if ($headerRow != 0) {
      $this->header = trim(fgets($this->inputFile, 4096));
    }
  }

  // Return the header
  public function getHeader() {
    return $this->header;
  }

  // Return lines before header
  public function getLinesBeforeHeader() {
    return $this->linesBeforeHeader;
  }

  // Read next row
  public function readNextRow() {
    $line = "";
    while (($line = fgets($this->inputFile, 4096)) !== false) {
      if (trim($line) != "") { // Ignore empty lines
        break;
      }
    }
    if ($line == false) {
      fclose($this->inputFile);
      return null;
    }

    return $line;
  }

  // Read next row as a String array. Optionally remove double quotes used in
  // such as Excel processed delimited file.
  public function readNextRowAsArray($removeDoubleQuotes = false) {
    $line = "";
    while (($line = fgets($this->inputFile, 4096)) !== false) {
      if (trim($line) != "") { // Ignore empty lines
        break;
      }
    }
    if ($line == false) {
      fclose($this->inputFile);
      return null;
    }

    if ($removeDoubleQuotes) {
      $token_line = explode($this->delimiter, $line);
      $result = array ();
      for ($i = 0; $i < count($token_line); $i ++) {
        $result [$i] = preg_replace('/"/', "", $token_line [$i]);
      }
      return $result;
    }
    else {
      return explode($this->delimiter, $line);
    }
  }

  // Write an array of values (i.e. $row) to a file (i.e. $out) with specified delimiter
  public static function writeRow($out, $row, $delimiter = "\t") {
    for ($i = 0; $i < count($row); $i ++) {
      fwrite($out, $row [$i]);
      if ($i != count($row) - 1) {
        fwrite($out, $delimiter);
      }
      else {
        fwrite($out, "\n");
      }
    }
  }
}
