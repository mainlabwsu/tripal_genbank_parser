<?php

namespace Drupal\tripal_genbank_parser\Util;

/**
 * Class String is a class with collected methods for String operation
 *
 */
class Str {

  // Return TRUE if the $haystack starts with $needle
  public static function startsWith($haystack, $needle) {
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
  }

  // Return TRUE if the $haystack ends with $needle
  public static function endsWith($haystack, $needle) {
    $length = strlen($needle);
    if ($length == 0) {
      return true;
    }
    return (substr($haystack, - $length) === $needle);
  }

  // Remove chars that cause problem for a GFF3 parser
  public static function gff3Escape($in) {
    $search = array (
        ';',
        '=',
        '%',
        '&',
        ',',
        ' '
    );
    $replace = array (
        '_',
        '_',
        '_',
        '_',
        '_',
        '_'
    );
    $out = str_replace($search, $replace, $in);
    return $out;
  }

  // Return the value for a hash with 'key=value' format
  public static function getValue($key_value_pair) {
    $token = explode("=", $key_value_pair, 2);
    return preg_replace("/\"/", "", $token [1]);
  }
}