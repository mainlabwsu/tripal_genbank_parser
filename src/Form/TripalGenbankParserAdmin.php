<?php

/**
 * @file
 * Contains \Drupal\tripal_genbank_parser\Form\TripalGenbankParserAdmin.
 */

namespace Drupal\tripal_genbank_parser\Form;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class TripalGenbankParserAdmin extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tripal_genbank_parser_admin';
  }

  public function jobAction ($action, $job_id) {
    // DELETE a job. Do not really delete ths job from the table, set it to a negative status instead.
    if(($action == 'delete' || $action == 'restore') && is_numeric($job_id)) {
      $status = \Drupal::database()->query("SELECT status FROM tripal_genbank_jobs WHERE job_id = :job_id", array(':job_id' => $job_id))->fetchField();
      $sql = "UPDATE tripal_genbank_jobs SET status = :status WHERE job_id = :job_id";
      \Drupal::database()->query($sql, array(':status' => (0 - $status), ':job_id' => $job_id));
    }
    else if ($action == 'purge') {
      $sql = "DELETE FROM tripal_genbank_jobs WHERE job_id = :job_id";
      \Drupal::database()->query($sql, array(':job_id' => $job_id));
    }
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $action = '', $job_id = '') {

    // Do jobAction. If action not valid, reroute to the URL without the action parameter
    if ($action) {
      $this->jobAction($action, $job_id);
      return new RedirectResponse(\Drupal\Core\Url::fromRoute('tripal_genbank_parser.admin')->toString());
    }

    $keyword = "";
    $days = "";
    $db = 'nucleotide';
    $chado = FALSE;
    if ($form_state->getValues()) {
      $keyword = $form_state->getValue(['keyword']);
      $days = $form_state->getValue(['days']);
      $db = $form_state->getValue(['db']);
      $chado = $form_state->getValue(['chado']);
    }

    $form = [];

    $form['parser'] = [
      '#type' => 'fieldset',
      '#title' => t('NCBI Entrez E-utilities Settings'),
      '#collapsible' => TRUE,
    ];
    $form['parser']['description'] = [
      '#type' => 'item',
      '#value' => t("Download and parse genbank records using a keyword.
          Use the 'Count' button to estimate the number of sequences and time for download.
          Click on the 'Submit' button to submit a Tripal job for downloading and parsing the Genbank records."),
      '#weight' => 0,
    ];
    $form['parser']['keyword'] = [
      '#type' => 'textfield',
      '#title' => t("Keyword"),
      '#description' => t("The keyword to search the NCBI nucleotide database. e.g. 'Malus x domestica [ORGN]'"),
      '#default_value' => $keyword,
      '#maxlength' => 1024,
      '#weight' => 1,
    ];
    $form['parser']['days'] = [
      '#type' => 'textfield',
      '#title' => t("Days since record modified"),
      '#description' => t("Limit the search to include records that have been added no more than this many days before today."),
      '#default_value' => $days,
      '#size' => 6,
      '#weight' => 2,
    ];
    $form['parser']['db'] = [
      '#type' => 'select',
      '#title' => t("Entrez database"),
      '#description' => t("The NCBI Entrez database to search against, currently supporting 'Nucleotde', 'EST', or 'GSS' databases."),
      '#options' => [
        'nucleotide' => 'Nucleotide',
        //'nucest' => 'EST',
        //'nucgss' => 'GSS',
      ],
      '#default_value' => $db,
      '#weight' => 3,
    ];
    $form['parser']['chado'] = [
      '#type' => 'checkbox',
      '#title' => t("Load into the Chado database"),
      '#description' => t("Check this box to load the parsing results into the Chado database. You can load the data later using the 'Chado Loader' below."),
      '#default_value' => $chado,
      '#weight' => 4,
    ];
    $form['parser']['count'] = [
      '#type' => 'submit',
      '#value' => t('Count'),
      '#weight' => 5,
    ];
    $form['parser']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
      '#weight' => 6,
    ];
    $form['parser']['reset'] = [
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#weight' => 7,
    ];

    $num_jobs = $form_state->getValue('number_jobs');
    $num_jobs = $num_jobs > 0 ? $num_jobs : 30;
    $form['jobs'] = [
      '#type' => 'fieldset',
      '#title' => t("Last $num_jobs Jobs"),
      '#collapsible' => TRUE,
    ];

    $form['jobs']['number_jobs'] = [
      '#type' => 'textfield',
      '#default_value' => $num_jobs,
      '#title' => 'Number of jobs to list',
      '#size' => 5,
      '#maxlength' => 5,
      '#prefix' => '<div id=tripal-genbank_parser-jobs_numbers>',
      '#suffix' => '</div>'
    ];

    $form['jobs']['view'] = [
      '#type' => 'button',
      '#value' => t('View'),
      '#prefix' => '<div id=tripal-genbank_parser-jobs_view>',
      '#suffix' => '</div>'
    ];

    $list = [];
    $db = \Drupal::database();
    $sql = "SELECT job_id, num_records, keyword, days, db, status, timestamp, uid, path
      FROM tripal_genbank_jobs ORDER BY job_id DESC
      LIMIT $num_jobs
      ";
    $results = $db->query($sql);
    while ($job = $results->fetchObject()) {
      $list[$job->job_id] = $job;
    }

    $status = array(
      -1 => 'In Recycle Bin',
      -2 => 'In Recycle Bin',
      -3 => 'In Recycle Bin',
      -4 => 'In Recycle Bin',
      1 => 'Created',
      2 => 'Downloaded',
      3 => 'Parsed',
      4 => 'Loaded'
    );

    if (count($list) != 0) {
      $index = 1;
      foreach ($list AS $k => $o) {
        $delete = \Drupal\Core\Link::createFromRoute('Delete', 'tripal_genbank_parser.admin',['action' => 'delete', 'job_id' => $o->job_id])->toString();
        $restore = \Drupal\Core\Link::createFromRoute('Restore', 'tripal_genbank_parser.admin',['action' => 'restore', 'job_id' => $o->job_id])->toString();
        $purge = \Drupal\Core\Link::createFromRoute('Purge', 'tripal_genbank_parser.admin',['action' => 'purge', 'job_id' => $o->job_id])->toString();
        $link = $delete;
        if ($o->status < 0) {
          $link = $restore . ' | ' . $purge;
        }
        $submitter = \Drupal\user\Entity\User::load($o->uid);
        $form ['jobs'] ['item' . $index] = [
          '#markup' =>
            '<td>' . $k .
            '</td><td>' . $o->num_records .
            '</td><td>' . $o->keyword .
            '</td><td>' . $o->days .
            '</td><td>' . $o->db .
            '</td><td nowrap=nowrap>' . $status[$o->status] .
            '</td><td>' . date('m/d/Y H-i-s', $o->timestamp) .
            '</td><td>' . $submitter->getDisplayName() .
            '</td><td>' . $o->path .
            '</td><td nowrap=nowrap>' . $link.
            '</td></tr>',
          '#prefix' => '<tr>',
          '#sufix' => '</tr>',
        ];
        $index ++;
      }
      $form ['jobs'] ['item1']['#prefix'] =
        "<table><tr>
          <th>Job ID</th>
          <th>#Records</th>
          <th>Keyword</th>
          <th>Days</th>
          <th>EntrezDB</th>
          <th>Status</th>
          <th>Time</th>
          <th>Submitter</th>
          <th>Path</th>
          <th>Action</th>
          </tr>";
      if (isset($form ['jobs'] ['item' . $index])) {
        $form ['jobs'] ['item' . $index]['#suffix'] .= "</table>";
      }
      $form['jobs']['hint'] = array(
          '#markup' => t("Note: To load data for a job that has been downloaded and parsed, use the drush command 'drush trp-gbload --job_id=[job_id]'"),
          '#prefix' => '<div id=tripal-genbank_parser-jobs_note>',
          '#suffix' => '</div>'
      );
    }
    else {
      $form['jobs']['description'] = [
        '#markup' => t("No Genbank job available."),
        '#weight' => 100
      ];
    }

    // Attach CSS
    $form['#attached']['library'] = ['tripal_genbank_parser/tripal_genbank_parser'];
    return $form;
  }

  public function validateForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {

    // If job view button is clicked, no need to validate, just return
    if ($form_state->getValue('op') == 'View') {
      return;
    }

    $form_state->setRebuild(TRUE); // Rebuild the form so the user input such as 'Keyword', 'Days', or 'NCBI Database' can be remembered

    $keyword = $form_state->getValue('keyword');
    if (!trim($keyword) && $form_state->getValue('op') != 'Reset' && $form_state->getValue('op') != 'Load') {
      $form_state->setErrorByName('', t('Keyword is required.'));
      return;
    }
    $days = $form_state->getValue('days');
    if ($days && !is_numeric($days) && $form_state->getValue('op') != 'Reset' && $form_state->getValue('op') != 'Load') {
      $form_state->setErrorByName('', t('Days should be numeric.'));
      return;
    }
    $db = $form_state->getValue('db');
    $chado = $form_state->getValue('chado');

    if ($form_state->getValue('op') == 'Count' && $keyword) {
      $ncbifetcher = new \Drupal\tripal_genbank_parser\Fetcher\NCBIFetcher();
      $result = $ncbifetcher->eSearch($keyword, NULL, $days, NULL, $db);
      \Drupal::messenger()->addStatus($result['count'] . ' record(s) match the search criteria.');
    }
    else {
      if ($form_state->getValue('op') == 'Submit') {
        $ncbifetcher = new \Drupal\tripal_genbank_parser\Fetcher\NCBIFetcher();
        $result = $ncbifetcher->eSearch($keyword, NULL, $days, NULL, $db);
        $num_records = $result['count'];
        $user = \Drupal::currentUser();
        $datatbase = \Drupal::database();
        $now = time();
        $success = $datatbase->query(
          "INSERT INTO tripal_genbank_jobs (uid, keyword, days, db, load, status, timestamp, num_records)
           VALUES (:uid, :keyword, :days, :db, :load, 1, :timestamp, :num_records)",
           array(
             ':uid' => $user->id(),
             ':keyword' => $keyword,
             ':days' => $days,
             ':db' => $db,
             ':load' => $chado,
             ':timestamp' => $now,
             ':num_records' => $num_records
           )
        );
        if ($success) {
          \Drupal::messenger()->addStatus('Genbank Job created. Use the \'drush trp-gbjob\' command to start the job.');
        }
      }
      else if ($form_state->getValue('op') == 'Reset') {
        $form_state->setRebuild(FALSE);
      }
    }
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  }
}
?>
