<?php

namespace Drupal\tripal_genbank_parser\Parser;

use Drupal\tripal_genbank_parser\Util\Str;

/**
 * Class MiscFeatureParser parses all genbank misc features such as promoter, exon, intron, tRNA, rRNA. etc.
 */
class MiscFeatureParser {
  public function parseFeatures($record, $feature_key, $out_misc) {
    $features = $record->getFeatures($feature_key);
    $ver = $record->getVersion();

    if ($features != null) {
      for ($i = 0; $i < count($features); $i ++) {
        if (! $features [$i]) {
          continue;
        }
        $featureprops = $features [$i]->getFeatureProp();
        $loc = $features [$i]->getFeatureLoc();
        $gene = "";
        $product = "";
        $function = "";
        $note = "";
        $other = "";
        foreach ($featureprops as $str) {
          if (Str::startsWith($str, "/gene")) {
            $gene = Str::getValue($str);
          }
          else if (Str::startsWith($str, "/product")) {
            $product = Str::getValue($str);
          }
          else if (Str::startsWith($str, "/function")) {
            $function = Str::getValue($str);
          }
          else if (Str::startsWith($str, "/note")) {
            $note = Str::getValue($str);
          }
          else {
            if ($other == "") {
              $other .= $str;
            }
            else {
              $other .= "|" . $str;
            }
          }
        }
        $uniquename = $ver . "-" . $feature_key;
        $count = 0;
        if (key_exists($uniquename, GBParser::$miscFeature_hash)) {
          $count = GBParser::$miscFeature_hash [$uniquename];
        }
        if ($count == 0) {
          GBParser::$miscFeature_hash [$uniquename] = 1;
        }
        else {
          $count ++;
          GBParser::$miscFeature_hash [$uniquename] = $count;
        }

        // Find FL.min and FL.max
        $fragments = explode(",", preg_replace("/join|complement|order|<|>|\(|\)/", "", $loc));
        $frag0 = explode("..", $fragments [0]);
        if (count($frag0) == 1) { // Sometimes, genbank file contains records with 'fmin^fmax', instead of 'fmin..fmax'
          $frag0 = explode("^", $fragments [0]);
        }
        $fmin = $frag0 [0];
        $fmax = $fmin;
        if (key_exists(1, $frag0)) {
          $fmax = $frag0 [1];
        }
        foreach ($fragments as $fragment) {
          $positions = explode("..", $fragment);
          $start = $positions [0];
          $stop = $start;
          if (key_exists(1, $positions)) {
            $stop = $positions [1];
            if ($positions [1] < $positions [0]) {
              $start = $positions [1];
              $stop = $positions [0];
            }
          }
          if ($start < $fmin) {
            $fmin = $start;
          }
          if ($stop > $fmax) {
            $fmax = $stop;
          }
        }

        // Determine the orientation and if the coordinate is partial
        $strand = 1;
        $fmin_partial = "FALSE";
        $fmax_partial = "FALSE";
        if (Str::startsWith($loc, "complement")) {
          $strand = - 1;
        }
        if (preg_match('/</', $loc)) {
          $fmin_partial = "TRUE";
        }
        if (preg_match('/>/', $loc)) {
          $fmax_partial = "TRUE";
        }
        $misc = $uniquename . GBParser::$miscFeature_hash [$uniquename];
        $uniq_misc = Str::gff3Escape($misc); // convert the uniquename to _ if it contains ; = % & ,
        $row = $uniq_misc . "\t";
        $row .= $uniquename . GBParser::$miscFeature_hash [$uniquename] . "\t";
        $row .= $feature_key . "\t";
        $row .= $ver . "\t";
        $row .= ($fmin - 1) . "\t";
        $row .= $fmax . "\t";
        $row .= $fmin_partial . "\t";
        $row .= $fmax_partial . "\t";
        $row .= $strand . "\t";
        if ($gene != "") {
          $row .= $gene . "\t";
        } else {
          $row .= ".\t";
        }
        if ($product != "") {
          $row .= $product . "\t";
        } else {
          $row .= ".\t";
        }
        if ($function != "") {
          $row .= $function . "\t";
        }
        else {
          $row .= ".\t";
        }
        if ($note != "") {
          $row .= $note . "\t";
        }
        else {
          $row .= ".\t";
        }
        $row .= "genbank\t";
        if ($other != "") {
         $row .= $other;
        } else {
          $row .= ".";
        }
        // Tripal bulk loader can't load Locus unless there is a sequence, -t option ignores the records that don't have a sequence
        if ($record->getSequence() || ! GBParser::$seq_required) {
          fwrite($out_misc, $row . "\n");
        }
      }
    }
  }
}