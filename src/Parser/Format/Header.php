<?php

namespace Drupal\tripal_genbank_parser\Parser\Format;

/**
 * Class Header stores the headers for the output files
 */
class Header {

  public $files;

  public function __construct() {
    $files = new \stdClass();
    $files->locus = '.locus.txt';
    $files->generic = '.generic.txt';
    $files->gene = '.gene.txt';
    $files->mrna = '.mrna.txt';
    $files->cds = '.cds.txt';
    $files->protein = '.protein.txt';
    $files->misc = '.misc.txt';
    $this->files = $files;
  }

  public $locus = array (
      "F.uniquename(Version)",
      "F.name(Locus)",
      "FP.value(Definition)",
      "FP.value(Keywords)",
      "F.organism_id(Organism)",
      "FP.value(References)",
      "FP.value(Pubmed)",
      "FP.value(Comment)",
      "FP.value(/organelle)",
      "FP.value(/cultivar)",
      "LP.value(/tissue_type)",
      "L.name(/clone_lib)",
      "clone(/clone)",
      "vector_type(@/note)",
      "Other(/*)",
      "F.type_id",
      "FP.value",
      "F.residue(ORIGIN)"
  );
  public $gene = array (
      "F.uniquename",
      "F.name",
      "F.type",
      "FL.srcfeature_id",
      "FL.fmin",
      "FL.fmax",
      "FL.is_fmin_partial",
      "FL.is_fmax_partial",
      "FL.strand",
      "FD.feature_dbxref_id",
      "FR.object_id",
      "FR.type_id",
      "FP.value(/note)",
      "FL.fmin&fmax(/trans_splicing)",
      "Other(/*)",
      "FP.value",
      "F.residue"
  );
  public $mrna = array (
      "F.uniquename",
      "F.name",
      "F.type_id",
      "FL.srcfeature_id",
      "FL.fmin",
      "FL.fmax",
      "FL.is_fmin_partial",
      "FL.is_fmax_partial",
      "FL.strand",
      "FD.feature_dbxref_id",
      "FP.value(/product)",
      "FP.value(/function)",
      "FP.value(/note)",
      "FR.object_id",
      "FR.type_id",
      "FP.value",
      "F.residue"
  );
  public $cds = array (
      "F.uniquename",
      "F.name",
      "F.type_id",
      "FL.srcfeature_id",
      "FL.fmin",
      "FL.fmax",
      "FL.is_fmin_partial",
      "FL.is_fmax_partial",
      "FL.strand",
      "FL.phase",
      "FR.object_id",
      "FR.type_id",
      "FP.value"
  );
  public $protein = array (
      "F.uniquename",
      "F.name",
      "F.type_id",
      "DX.accession",
      "FP.value(/product)",
      "FP.value(/function)",
      "FP.value(/note)",
      "FR.object_id",
      "FR.type_id",
      "FP.value",
      "Other(/*)",
      "F.residue"
  );
  public $generic = array (
      "F.uniquename",
      "F.name",
      "F.type_id",
      "Organism",
      "FP.value"
  );
  public $misc = array (
      "F.uniquename",
      "F.name",
      "F.type_id",
      "FL.srcfeature_id",
      "FL.fmin",
      "FL.fmax",
      "FL.is_fmin_partial",
      "FL.is_fmax_partial",
      "FL.strand",
      "FP.value(/gene)",
      "FP.value(/product)",
      "FP.value(/function)",
      "FP.value(/note)",
      "FP.value",
      "Other(/*)"
  );
}
