<?php

namespace Drupal\tripal_genbank_parser\Parser\Format;

use Drupal\tripal_genbank_parser\Parser\GBParser;
use Drupal\tripal_genbank_parser\Util\Str;

/**
 * Class GenericGene creates a generic gene per species
 */
class GenericGene {
  public function writeGenericGenes($out_generic) {

    // Output generic genes. We create a generic gene for each species
    if (! GBParser::$only_EST_GSS) {
      foreach (GBParser::$generic_genes as $k => $v) {
        $vgeneric = explode('__', $v);
        $genegeneric = $vgeneric [0];
        $orggeneric = $vgeneric [1];
        preg_match("/(\w).+? +(.+)/", $orggeneric, $matches);
        $genus = "";
        $species = "";
        if (key_exists(1, $matches)) {
          $genus = strtoupper($matches [1]); // First letter of the genus
        }
        if (key_exists(2, $matches)) {
          $species = $matches [2]; // The full species
          /* The following rules are applied when creating the sysnonym */
          // Rule 1: If species start with 'x ', use the first letter after 'x '
          preg_match("/^x (\w).+/", $species, $mat);
          if (key_exists(1, $mat)) {
            $species = $mat [1];
            // Rule 2: Don't make synonym for the species name starting with 'sp.', 'cf.', 'aff.', or containing ' x '.
          }
          else if (preg_match("/^sp\.|cf\.|aff\./", $species) || preg_match("/^.+? x .+/", $species)) {
            $species = "";
            // Otherwise, use the first letter for synonym
          }
          else {
            $species = substr($species, 0, 1);
          }
        }

        $uniq_generic = Str::gff3Escape($genegeneric); // convert the uniquename to _ if it contains ; = % & ,
        $synonym = "";
        if ($species != "") {
          // Rule 3: Don't make synonym if the gene name already has the 'Genus' + 'species' prefix
          if (! preg_match("/^$genus$species/", $genegeneric)) {
            $synonym = $genus . strtolower($species) . strtoupper($uniq_generic);
          }
        }
        fwrite($out_generic, "$uniq_generic\t$genegeneric\tgene\t$orggeneric\ttripal_genbank_parser_generic_gene\n");
      }
    }
  }
}