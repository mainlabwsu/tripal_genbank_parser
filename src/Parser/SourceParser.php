<?php

namespace Drupal\tripal_genbank_parser\Parser;

use Drupal\tripal_genbank_parser\Genbank\Record\Feature;
use Drupal\tripal_genbank_parser\Util\Str;

/**
 * Class SourceParser parses the first genbank 'source' section
 */
class SourceParser {
  public function parseFeatureSources($srcs, $result) {
    if ($srcs != null) {
      for ($i = 0; $i < 1; $i ++) { // Note: we parse only the first source which covers the whole sequence region
        $featureprops = $srcs [$i]->getFeatureProp();
        $k_v_featureprop = Feature::featurepropToHashMap($featureprops);

        if (key_exists('/organelle', $k_v_featureprop)) {
          array_push($result, $k_v_featureprop ['/organelle']);
        }
        else {
          array_push($result, ".");
        }

        if (key_exists('/cultivar', $k_v_featureprop)) {
          array_push($result, $k_v_featureprop ['/cultivar']);
        }
        else {
          array_push($result, ".");
        }

        if (key_exists('/tissue_type', $k_v_featureprop)) {
          array_push($result, $k_v_featureprop ['/tissue_type']);
        }
        else {
          array_push($result, ".");
        }

        if (key_exists('/clone_lib', $k_v_featureprop)) {
          array_push($result, $k_v_featureprop ['/clone_lib']);
        }
        else {
          array_push($result, ".");
        }

        if (key_exists('/clone', $k_v_featureprop)) {
          array_push($result, $k_v_featureprop ['/clone']);
        }
        else {
          array_push($result, ".");
        }

        if (key_exists('/note', $k_v_featureprop)) {
          $note = $k_v_featureprop ['/note'];
          preg_match("/V-type:\s+(.+?);/", $note, $vectype);
          if (key_exists(1, $vectype)) {
            array_push($result, $vectype [1]);
          }
          else {
            array_push($result, ".");
          }
        }
        else {
          array_push($result, ".");
        }
        $other = "";
        $counter = 0;
        foreach ($featureprops as $str) {
          if (! Str::startsWith($str, "/organelle") && ! Str::startsWith($str, "/mol_type") && ! Str::startsWith($str, "/cultivar") && ! Str::startsWith($str, "/tissue_type") && ! Str::startsWith($str, "/clone_lib") && ! Str::startsWith($str, "/clone")) {
            if ($other == "") {
              $other .= $str;
            }
            else {
              $other .= "|" . $str;
            }
          }
          $counter ++;
        }
        array_push($result, $other);
      }
    }
    return $result;
  }
}