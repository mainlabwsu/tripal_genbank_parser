<?php

namespace Drupal\tripal_genbank_parser\Parser;

/**
 * ReferenceParser
 *
 * A class that parses Genbank References and generates one-line
 * string output.
 */
class ReferenceParser {

  // An optional associated array for looking up reference codes.
  public static $ref_codes = NULL;

  /*
   * Read in the optional Reference Code conversion table and store it in the
   * class variable $ref_codes. The user can optionally pass in a Reference
   * Code file in which first column is the Journal Name and the second
   * column is the Journal Code. The Reference Parser will look up the Journal
   * Name and return its Code. An extra column RefCode will be added to the
   * locus parsing results.
   */
  static public function populateRefCode($file) {
    $df = new DelimitedFile($file, 1);
    $line = "";
    ReferenceParser::$ref_codes = array ();
    while (($row = $df->readNextRowAsArray()) != null) {
      ReferenceParser::$ref_codes [strtoupper($row [0])] = $row [1];
    }
  }

  /*
   * Parse REFERENCE's.
   *
   * Output format: Ref1 (base1 to base2): REF TEXT. Ref2 (base1 to base2): REF TEXT.
   * Optionally ignore Direct Submission/In press/Unpublished pubs
   */
  public function parseReferences($refs, $ignorePub = false) {
    $final_ref = "";
    $final_pubmed = "";
    $final_code = "";
    if ($refs != null) {
      $counter = 1;
      for ($i = 0; $i < count($refs); $i ++) {
        $memory = $final_ref;
        $memoryp = $final_pubmed;
        $memoryc = $final_code;
        $memoryn = $counter;

        if ($i != 0) {
          if ($final_ref != "") {
            $final_ref .= " | ";
          }
          if ($final_pubmed != "") {
            $final_pubmed .= " | ";
          }
          if ($final_code != "") {
            $final_code .= " | ";
          }
        }

        // Append CONSRTM, AUTHOR, TITLE, JOURNAL, PUBMED, REMARK for each reference
        $ref = $refs [$i];
        $final_ref .= "Ref" . $counter . " ";
        if (key_exists('BASES', $ref)) {
          $final_ref .= $ref ['BASES'] . ": ";
        }
        if (key_exists('CONSRTM', $ref)) {
          $final_ref .= $ref ['CONSRTM'] . " ";
        }
        if (key_exists('AUTHORS', $ref)) {
          $final_ref .= $ref ['AUTHORS'] . " ";
        }
        if (key_exists('TITLE', $ref)) {
          $title = $ref ['TITLE'];
          if ($ignorePub) {
            if ($title == "Direct Submission") {
              $final_ref = $memory;
              $final_pubmed = $memoryp;
              $final_code = $memoryc;
              $counter = $memoryn;
              continue;
            }
          }
          $final_ref .= $title . ". ";
        }

        if (key_exists('JOURNAL', $ref)) {
          $journal = $ref ['JOURNAL'];
          if ($ignorePub) {
            if (preg_match("/Unpublished/i", $journal) || preg_match("/In press/i", $journal)) {
              $final_ref = $memory;
              $final_pubmed = $memoryp;
              $final_code = $memoryc;
              $counter = $memoryn;
              continue;
            }
          }
          $final_ref .= $journal . " ";

          $pattern = preg_match('/^(Patent:[\s|\w|\d]+)/', $journal, $pat);
          if ($pattern) {
            $final_code .= "Ref" . $counter . ": " . $pat [1];
          }
          else {
            preg_match('/^(.+?) (\d+).*?,\s*(e?\d+)/', $journal, $matches);
            $code = null;
            if (key_exists(1, $matches) && ReferenceParser::$ref_codes) {
              if (key_exists(strtoupper($matches [1]), ReferenceParser::$ref_codes)) {
                $code = ReferenceParser::$ref_codes [strtoupper($matches [1])];
              }
            }
            if (! key_exists(1, $matches)) {
              $code = "not_available";
            }
            if (key_exists(2, $matches) && $matches [2] != "") {
              $final_code .= "Ref" . $counter . ": " . $code . "-" . $matches [2] . "-" . $matches [3];
            }
            else {
              $final_code .= "Ref" . $counter . ": " . $code;
            }
          }
        }

        if (key_exists('PUBMED', $ref)) {
          $pubmed = $ref ['PUBMED'];
          $final_ref .= " Pubmed " . $pubmed . ".";
          $final_pubmed .= "Ref" . $counter . ": " . $pubmed;
        }
        if (key_exists('REMARK', $ref)) {
          $final_ref .= " Remark " . $ref ['REMARK'] . ".";
        }
        $counter ++;
      }
    }
    return array (
        $final_ref,
        $final_pubmed,
        $final_code
    );
  }
}