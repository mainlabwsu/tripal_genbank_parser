<?php

namespace Drupal\tripal_genbank_parser\Parser;

use Drupal\tripal_genbank_parser\File\DelimitedFile;

/**
 * Class LocusParser parse a LOCUS record.
 *
 * General information about a locus as well as its source and references
 * were printed to the output
 *
 */
class LocusParser {
  public function parseLocus($record, $ref_parser, $src_parser, $out_locus) {
    $locus = array (); // This variable stores the LOCUS feature output for each record

    // VERSION = feature.uniquename
    $ver = $record->getVersion();
    array_push($locus, $ver);

    // LOCUS = feature.name
    $locusname = $record->getLocusName();
    array_push($locus, $locusname);

    // DEFINITION = featureprop.value
    $def = $record->getDefinition();
    array_push($locus, $def);

    // KEYWORDS = featuureprop.value
    $keywords = $record->getKeyword();
    array_push($locus, $keywords);

    // ORGANISM = feature.organism_id
    $organism = $record->getOrganism();
    // $organism = ucfirst(strtolower($organism)); // Ensure the organism to be upper case for the first letter
    array_push($locus, $organism);

    // REFERENCE, PUBMED, REF_CODE (Optional) =featureprop.value
    $references = $ref_parser->parseReferences($record->getReferences(), true);
    $ref_r = $references [0];
    $ref_p = $references [1];
    $ref_c = $references [2];
    if ($ref_r != "") {
      array_push($locus, $ref_r);
    }
    else {
      array_push($locus, ".");
    }
    if ($ref_p != "") {
      array_push($locus, $ref_p);
    }
    else {
      array_push($locus, ".");
    }
    if (ReferenceParser::$ref_codes != NULL) {
      if ($ref_c != "") {
        array_push($locus, $ref_c);
      }
      else {
        array_push($locus, ".");
      }
    }

    // COMMENT =featureprop.value
    $comment = $record->getComment() ? $record->getComment() : ".";
    array_push($locus, $comment);

    // Parse first 'source' section which covers the whole sequence region and has more information for this LOCUS
    $locus = $src_parser->parseFeatureSources($record->getFeatures("source"), $locus);

    // Add a remark to the featureprop
    if ($keywords == "EST.") {
      array_push($locus, "EST");
    }
    else if ($keywords == "GSS.") {
      array_push($locus, "GSS");
    }
    else {
      array_push($locus, "region");
      GBParser::$only_EST_GSS = false;
    }
    array_push($locus, "genbank");

    // ORIGIN = feature.residue
    $seq = $record->getSequence();
    array_push($locus, $seq);

    // Tripal bulk loader can't load Locus unless there is a sequence, -t option ignores the records that don't have a sequence
    if ($seq || ! GBParser::$seq_required) {
      // Finally, write to LOUCUS feature output
      DelimitedFile::writeRow($out_locus, $locus);
    }
  }
}