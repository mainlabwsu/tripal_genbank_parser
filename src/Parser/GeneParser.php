<?php

namespace Drupal\tripal_genbank_parser\Parser;

use Drupal\tripal_genbank_parser\Genbank\Record\Feature;
use Drupal\tripal_genbank_parser\Util\Str;

/**
 * Class GeneParser parses all genbank 'gene' feature sections
 */
class GeneParser {
  public function parseGenes($record, $out_gene) {
    $genes = $record->getFeatures("gene");
    $ver = $record->getVersion();
    $organism = $record->getOrganism();

    if ($genes != null) {
      for ($i = 0; $i < count($genes); $i ++) {
        $featureprops = $genes [$i]->getFeatureProp();
        $loc = $genes [$i]->getFeatureLoc();
        $gene = "";
        $db_xref = "";
        $locus_tag = "";
        $note = "";
        $trans_splicing = FALSE;
        $other = "";
        foreach ($featureprops as $str) {
          if (Str::startsWith($str, "/gene")) {
            $gene = Str::getValue($str);
          }
          else if (Str::startsWith($str, "/locus_tag")) {
            $locus_tag = Str::getValue($str);
          }
          else if (Str::startsWith($str, "/db_xref")) {
            $db_xref = Str::getValue($str);
          }
          else if (Str::startsWith($str, "/note")) {
            if ($note != "") {
              $note .= "|" . Str::getValue($str);
            }
            else {
              $note = Str::getValue($str);
            }
          }
          else if (Str::startsWith($str, "/trans_splicing")) {
            $trans_splicing = TRUE;
          }
          else if (Str::startsWith($str, "/")) {
            if ($other != "") {
              $other .= "|" . $str;
            }
            else {
              $other = $str;
            }
          }
        }
        if ($gene == "") {
          $gene = $locus_tag;
        }

        // Find FL.min and FL.max
        $fragments = explode(",", preg_replace("/join|complement|order|<|>|\(|\)/", "", $loc));
        $frag0 = explode("..", $fragments [0]);
        if (count($frag0) == 1) { // Sometimes, genbank file contains records with 'fmin^fmax', instead of 'fmin..fmax'
          $frag0 = explode("^", $fragments [0]);
        }
        $fmin = $frag0 [0];
        $fmax = key_exists(1, $frag0)? $frag0 [1] : $fmin;
        foreach ($fragments as $fragment) {
          $positions = explode("..", $fragment);
          if (count($positions) == 1) { // Sometimes, genbank file contains records with 'fmin^fmax', instead of 'fmin..fmax'
            $positions = explode("^", $fragment);
          }
          $start = $positions [0];
          $stop = key_exists(1, $positions)? $positions [1] : $start;
          if ($stop < $start) {
            $start = $positions [1];
            $stop = $positions [0];
          }
          if ($start < $fmin) {
            $fmin = $start;
          }
          if ($stop > $fmax) {
            $fmax = $stop;
          }
        }

        // Determine the orientation and if the coordinate is partial
        $strand = 1;
        $fmin_partial = "FALSE";
        $fmax_partial = "FALSE";
        if (Str::startsWith($loc, "complement")) {
          $strand = - 1;
        }
        if (preg_match('/</', $loc)) {
          $fmin_partial = "TRUE";
        }
        if (preg_match('/>/', $loc)) {
          $fmax_partial = "TRUE";
        }

        // Make sure fmin, fmax values are valid
        if ($fmin > $fmax || ! is_numeric($fmin) || ! is_numeric($fmax)) {
          $fmin = 0;
          $fmax = 0;
          $fmin_partial = "FALSE";
          $fmax_partial = "FALSE";
        }
        $fmin = $trans_splicing ? '.' : $fmin - 1;
        $fmax = $trans_splicing ? '.' : $fmax;
        $fmin_partial = $trans_splicing ? '.' : $fmin_partial;
        $fmax_partial = $trans_splicing ? '.' : $fmax_partial;
        $strand = $trans_splicing ? '.' : $strand;
        $uniq_gene = Str::gff3Escape("$ver-$gene"); // convert the uniquename to _ if it contains ; = % & ,
        $row = "";
        $row .= $uniq_gene . "\t" . $gene . "\tgene" . "\t" . $ver . "\t" . $fmin . "\t" . $fmax . "\t" . $fmin_partial . "\t" . $fmax_partial . "\t" . $strand;
        if (trim($db_xref) != "") {
          $row .= "\t" . $db_xref;
        }
        else {
          $row .= "\t.";
        }
        if ($gene != "") {
          // Store the gene as generic gene if it's not already stored (use lower case gene name as key)
          $kgeneric = $organism . "_" . strtolower($gene);
          if (! key_exists($kgeneric, GBParser::$generic_genes)) {
            GBParser::$generic_genes [$kgeneric] = $gene . "__" . $organism;
            // If it already exists, change the case of the gene name to match the generic gene
          }
          else {
            $saved = explode("__", GBParser::$generic_genes [$kgeneric]);
            $gene = $saved [0];
          }
          $row .= "\t" . Str::gff3Escape($gene) . "\tassociated_with";
        }
        else {
          $row .= "\t.\t.";
        }
        if ($note != "") {
          $row .= "\t" . $note;
        }
        else {
          $row .= "\t.";
        }
        if ($trans_splicing) {
          $row .= "\t" . $loc;
        } else {
          $row .= "\t.";
        }
        if ($other != "") {
          $row .= "\t" . $other;
        }
        else {
          $row .= "\t.";
        }
        $row .= "\tgenbank";
        $seq = Feature::computeSeq($loc, $record->getSequence());
        $row .= "\t$seq\n";
        fwrite($out_gene, $row);
      }
    }
  }
}
