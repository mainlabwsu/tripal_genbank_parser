<?php

namespace Drupal\tripal_genbank_parser\Parser;

use Drupal\tripal_genbank_parser\Genbank\Record\Feature;
use Drupal\tripal_genbank_parser\Util\Str;

/**
 * Class TranscriptParser parses all 'mRNA/CDS' features
 */
class TranscriptParser {

  /*
   * Parse 'CDS' sections
   */
  public function parseTranscripts($record, $out_mrna, $out_CDS, $out_protein) {
    $mrnas = $record->getFeatures("mRNA");
    $CDSes = $record->getFeatures("CDS");
    $ver = $record->getVersion();
    $organism = $record->getOrganism();
    $srcSeq = $record->getSequence();

    $stored_mrna_loc = array ();

    // Store the mRNA location in a hash
    if ($mrnas != null) {
      for ($i = 0; $i < count($mrnas); $i ++) {
        $mprops = $mrnas [$i]->getFeatureProp();
        $mgene = "";
        $mlocus_tag = "";
        foreach ($mprops as $str) {
          if (Str::startsWith($str, "/gene")) {
            $mgene = Str::getValue($str);
          }
          else if (Str::startsWith($str, "/locus_tag")) {
            $mlocus_tag = Str::getValue($str);
          }
        }
        if ($mgene == "") {
          $mgene = $mlocus_tag;
        }

        $mloc = $mrnas [$i]->getFeatureLoc();
        if ($mgene != "") {
          $stored_mrna_loc [$mgene] = $mloc;
          if (! key_exists($organism . "_" . strtolower($mgene), GBParser::$generic_genes)) {
            GBParser::$generic_genes [$organism . "_" . strtolower($mgene)] = $mgene . "__" . $organism; // Store the gene as generic gene
          }
        }
      }
    }

    // Parse the CDS, use the mRNA information if there is one
    if ($CDSes != null) {
      for ($i = 0; $i < count($CDSes); $i ++) {
        $featureprops = $CDSes [$i]->getFeatureProp();
        $loc = $CDSes [$i]->getFeatureLoc();
        $seq = Feature::computeSeq($loc, $srcSeq);
        $gene = "";
        $locus_tag = "";
        $db_xref = "";
        $product = "";
        $function = "";
        $note = "";
        $protein_id = "";
        $translation = "";
        $codon_start = - 1;
        $fmin_partial = "FALSE";
        $fmax_partial = "FALSE";
        $other = "";
        foreach ($featureprops as $str) {
          if (Str::startsWith($str, "/gene")) {
            $gene = Str::getValue($str);
          }
          else if (Str::startsWith($str, "/locus_tag")) {
            $locus_tag = Str::getValue($str);
          }
          else if (Str::startsWith($str, "/db_xref")) {
            $db_xref = Str::getValue($str);
          }
          else if (Str::startsWith($str, "/product")) {
            $product = Str::getValue($str);
          }
          else if (Str::startsWith($str, "/function")) {
            if ($function != "") {
              $function .= "|" . Str::getValue($str);
            }
            else {
              $function = Str::getValue($str);
            }
          }
          else if (Str::startsWith($str, "/note")) {
            if ($note != "") {
              $note .= "|" . Str::getValue($str);
            }
            else {
              $note = Str::getValue($str);
            }
          }
          else if (Str::startsWith($str, "/protein_id")) {
            $protein_id = Str::getValue($str);
          }
          else if (Str::startsWith($str, "/translation")) {
            $translation = preg_replace("/\s+/", "", Str::getValue($str));
          }
          else if (Str::startsWith($str, "/codon_start")) {
            $codon_start = Str::getValue($str);
          }
          else if (Str::startsWith($str, "/")) {
            if ($other != "") {
              $other .= "|" . $str;
            }
            else {
              $other = $str;
            }
          }
        }

        if ($gene == "") {
          $gene = $locus_tag;
        }

        if ($gene != "") {
          $uniquename = $ver . "-" . $gene;
          if (! key_exists($organism . "_" . strtolower($gene), GBParser::$generic_genes)) {
            GBParser::$generic_genes [$organism . "_" . strtolower($gene)] = $gene . "__" . $organism; // Store the gene as generic gene
          }
        }
        else {
          $uniquename = $ver;
        }

        $mrna_count = 0;
        if (key_exists($ver, GBParser::$mRNA_hash)) {
          $mrna_count = GBParser::$mRNA_hash [$ver];
        }
        if ($mrna_count == 0) {
          GBParser::$mRNA_hash [$ver] = 1;
        }
        else {
          $mrna_count ++;
          GBParser::$mRNA_hash [$ver] = $mrna_count;
        }
        $row = "";
        $mRNA = $uniquename . ".m" . GBParser::$mRNA_hash [$ver];

        // CDS output
        $cds_loc = preg_replace('/join|complement|order|<|>|\(|\)/', "", $loc);
        $fragments = explode(",", $cds_loc);
        $frag0 = explode("..", $fragments [0]);
        if (count($frag0) == 1) { // Sometimes, genbank file contains records with 'fmin^fmax', instead of 'fmin..fmax'
          $frag0 = explode("^", $fragments [0]);
        }
        $fmin = $frag0 [0];
        $fmax = key_exists(1, $frag0)? $frag0 [1] : $fmin;
        $cdscounter = 0;
        $row_c = "";
        foreach ($fragments as $fragment) {
          $cdscounter ++;
          $cds = $mRNA . "-cds" . $cdscounter;
          $positions = explode("..", $fragment);
          if (count($positions) == 1) { // Sometimes, genbank file contains records with 'fmin^fmax', instead of 'fmin..fmax'
            $frag0 = explode("^", $fragment);
          }
          $start = $positions [0];
          $stop = $positions [0];  // If it's single base, make stop=start.
          if (count($positions) == 2) { // Otherwise, get the stop position.
            $stop = $positions [1];
          }
          if ($stop < $start) {
            $start = $positions [1];
            $stop = $positions [0];
          }
          if ($start < $fmin) {
            $fmin = $start;
          }
          if ($stop > $fmax) {
            $fmax = $stop;
          }
          $strand = 1;
          $phase = 0;
          if (preg_match("/^complement/", $loc)) {
            $strand = - 1;
          }
          else if (preg_match("/complement\($fragment/", $loc)) {
            $strand = - 1;
          }
          if ($codon_start != - 1 && $cdscounter == 1) {
            $phase = $codon_start - 1;
          }
          if ($cdscounter == 1 && preg_match("/</", $loc)) {
            $fmin_partial = "TRUE";
          }
          if ($cdscounter == count($fragments) && preg_match("/>/", $loc)) {
            $fmax_partial = "TRUE";
          }
          $uniq_cds = Str::gff3Escape($cds); // convert the uniquename to _ if it contains ; = % & ,
          $row_c .= $uniq_cds . "\t";
          $row_c .= $cds . "\t";
          $row_c .= "CDS\t";
          $row_c .= Str::gff3Escape($ver) . "\t";
          $row_c .= ($start - 1) . "\t";
          $row_c .= $stop . "\t";
          $row_c .= $fmin_partial . "\t";
          $row_c .= $fmax_partial . "\t";
          $row_c .= $strand . "\t";
          $row_c .= $phase . "\t";
          $row_c .= Str::gff3Escape($mRNA) . "\tpart_of\t";
          $row_c .= "genbank\n";
        }

        // UTR output If there is a mRNA section for this gene
        $orig_mloc = null;
        if (key_exists($gene, $stored_mrna_loc)) {
          $orig_mloc = $stored_mrna_loc [$gene];
        }
        $mrna_loc = trim(preg_replace("/join|complement|order|<|>|\(|\)/", "", $orig_mloc));
        $mfmin_partial = "FALSE";
        $mfmax_partial = "FALSE";
        $mstrand = 1;
        $row_u5 = "";
        $row_u3 = "";
        $exon_min = - 1;
        $exon_max = - 1;
        if ($mrna_loc != null) {
          if ($mrna_loc != $cds_loc) {
            // print "$ver\t$gene\t$cds_loc\t$mrna_loc\n";
            $exons = explode(",", $mrna_loc);
            $counter_utr5 = 1;
            $counter_utr3 = 1;
            $exon0 = explode("..", $exons [0]);
            if (count($exon0) == 1) { // Sometimes, genbank file contains records with 'fmin^fmax', instead of 'fmin..fmax'
              $exon0 = explode("^", $exons [0]);
            }
            $exon_min = $exon0 [0];
            $exon_max = key_exists(1, $exon0)? $exon0 [1]: $exon_min;
            foreach ($exons as $exon) {
              $exon_positions = explode("..", $exon);
              if (count($exon_positions) == 1) { // Sometimes, genbank file contains records with 'fmin^fmax', instead of 'fmin..fmax'
                $exon_positions = explode("^", $exon);
              }
              $exon_start = $exon_positions [0];
              $exon_stop = key_exists(1, $exon_positions)? $exon_positions [1]: $exon_start;
              if ($exon_stop < $exon_start) {
                $exon_stop = $exon_positions [0];
                $exon_start = key_exists(1, $exon_positions)? $exon_positions [1] : $exon_stop;
              }
              if ($exon_start < $exon_min) {
                $exon_min = $exon_start;
              }
              if ($exon_stop > $exon_max) {
                $exon_max = $exon_stop;
              }
              // 5' UTR
              if ($exon_stop < $fmin) { // This whole exon is a 5' UTR
                $utr5 = $mRNA . "-5utr" . $counter_utr5;
                $uniq_utr5 = Str::gff3Escape($utr5); // convert the uniquename to _ if it contains ; = % & ,
                $row_u5 .= $uniq_utr5 . "\t" . $utr5 . "\tfive_prime_UTR\t" . Str::gff3Escape($mRNA) . "\t" . ($exon_start - 1) . "\t" . $exon_stop . "\tFALSE\tFALSE\t1\t0\t" . Str::gff3Escape($mRNA) . "\tpart_of" . "\tgenbank\n";
                $counter_utr5 ++;
              }
              else if ($exon_start < $fmin) { // Part of this exon is a 5' UTR
                $utr5 = $mRNA . "-5utr" . $counter_utr5;
                $uniq_utr5 = Str::gff3Escape($utr5); // convert the uniquename to _ if it contains ; = % & ,
                $row_u5 .= $uniq_utr5 . "\t" . $utr5 . "\tfive_prime_UTR\t" . Str::gff3Escape($mRNA) . "\t" . ($exon_start - 1) . "\t" . ($fmin - 1) . "\tFALSE\tFALSE\t1\t0\t" . Str::gff3Escape($mRNA) . "\tpart_of" . "\tgenbank\n";
                $counter_utr5 ++;
              }

              // 3' UTR
              if ($exon_start > $fmax) { // This whole exon is a 3' UTR
                $utr3 = $mRNA . "-3utr" . $counter_utr3;
                $uniq_utr3 = Str::gff3Escape($utr3); // convert the uniquename to _ if it contains ; = % & ,
                $row_u3 .= $uniq_utr3 . "\t" . $utr3 . "\tthree_prime_UTR\t" . Str::gff3Escape($mRNA) . "\t" . ($exon_start - 1) . "\t" . $exon_stop . "\tFALSE\tFALSE\t1\t0\t" . Str::gff3Escape($mRNA) . "\tpart_of" . "\tgenbank\n";
                $counter_utr3 ++;
              }
              else if ($exon_stop > $fmax) { // Part of this exon is a 3' UTR
                $utr3 = $mRNA . "-3utr" . $counter_utr3;
                $uniq_utr3 = Str::gff3Escape($utr3); // convert the uniquename to _ if it contains ; = % & ,
                $row_u3 .= $uniq_utr3 . "\t" . $utr3 . "\tthree_prime_UTR\t" . Str::gff3Escape($mRNA) . "\t" . $fmax . "\t" . $exon_stop . "\tFALSE\tFALSE\t1\t0\t" . Str::gff3Escape($mRNA) . "\tpart_of" . "\tgenbank\n";

                $counter_utr3 ++;
              }
            }
          }

          // Determine the strand and if the fmin/fmax is partial
          if ($orig_mloc) {
            if (Str::startsWith($orig_mloc, "complement")) {
              $mstrand = - 1;
            }
            if (preg_match('/</', $orig_mloc)) {
              $mfmin_partial = "TRUE";
            }
            if (preg_match('/>/', $orig_mloc)) {
              $mfmax_partial = "TRUE";
            }
          }
        }
        else {
          if (Str::startsWith($loc, "complement")) {
            $mstrand = - 1;
          }
          if (preg_match('/</', $loc)) {
            $mfmin_partial = "TRUE";
          }
          if (preg_match('/>/', $loc)) {
            $mfmax_partial = "TRUE";
          }
        }

        fwrite($out_CDS, $row_u5);
        fwrite($out_CDS, $row_c);
        fwrite($out_CDS, $row_u3);

        // mRNA output
        $uniq_mrna = Str::gff3Escape($mRNA); // convert the uniquename to _ if it contains ; = % & ,
        $row .= $uniq_mrna . "\t";
        if ($gene != "") {
          $row .= $gene;
        }
        else {
          $row .= $mRNA;
        }
        $row .= "\tmRNA";
        $row .= "\t" . $ver;
        if ($exon_min != - 1 && $fmin > $exon_min) {
          $row .= "\t" . ($exon_min - 1);
        }
        else {
          $row .= "\t" . ($fmin - 1);
        }
        if ($exon_max != - 1 && $fmax < $exon_max) {
          $row .= "\t" . $exon_max;
        }
        else {
          $row .= "\t" . $fmax;
        }
        $row .= "\t" . $mfmin_partial . "\t" . $mfmax_partial . "\t" . $mstrand;
        if ($db_xref != "") {
          $row .= "\t" . $db_xref . "\t";
        }
        else {
          $row .= "\t.\t";
        }
        $row .= $product . "\t";
        if ($function != "") {
          $row .= $function . "\t";
        }
        else {
          $row .= ".\t";
        }
        if ($note != "") {
          $row .= $note . "\t";
        }
        else {
          $row .= ".\t";
        }
        if ($gene != "" && $record->getMoleculeType() != 'mRNA') {
          $row .= $ver . "-" . Str::gff3Escape($gene) . "\tpart_of\t";
        }
        else {
          $row .= ".\t.\t";
        }
        $row .= "genbank\t";
        $row .= $seq . "\n";
        fwrite($out_mrna, $row);

        // Protein output
        $row_p = "";
        $prot = $uniquename . ".p" . GBParser::$mRNA_hash [$ver];
        $uniq_prot = Str::gff3Escape($prot); // convert the uniquename to _ if it contains ; = % & ,
        if (trim($protein_id) != "") { // Generate protein feature only if there is a protein_id
          $row_p .= $uniq_prot;
          if ($gene != "") {
            $row_p .= "\t" . $gene;
          }
          else {
            $row_p .= "\t" . $prot;
          }
          $row_p .= "\tpolypeptide";
          $row_p .= "\t" . $protein_id;
          $row_p .= "\t" . $product;
          if ($function != "") {
            $row_p .= "\t" . $function;
          }
          else {
            $row_p .= "\t.";
          }
          if ($note != "") {
            $row_p .= "\t" . $note;
          }
          else {
            $row_p .= "\t.";
          }
          $row_p .= "\t" . Str::gff3Escape($mRNA);
          $row_p .= "\tderives_from";
          $row_p .= "\tgenbank";
          if ($other != "") {
            $row_p .= "\t" . $other;
          }
          else {
            $row_p .= "\t.";
          }
          $row_p .= "\t" . $translation;
          $row_p .= "\n";
          fwrite($out_protein, $row_p);
        }
      }
    }
  }
}
