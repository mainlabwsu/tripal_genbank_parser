<?php

namespace Drupal\tripal_genbank_parser\Parser;

/**
 * Class GBParser
 *
 * The entry point for the parser. The GBParser invokes other parsers
 * that are required for parsing a Genbank file.
 */
use Drupal\tripal_genbank_parser\File\GenbankFile;
use Drupal\tripal_genbank_parser\File\DelimitedFile;
use Drupal\tripal_genbank_parser\Parser\Format\Header;
use Drupal\tripal_genbank_parser\Parser\Format\GenericGene;

class GBParser {

  // Global variables
  public static $mRNA_hash = array (); // This variable keeps track of the /gene property for CDS feature output. This property is used to create uniquename for the mRNAs
  public static $miscFeature_hash = array (); // This variable keeps track of the name for misc features so their uniquename can be created.
  public static $generic_genes = array (); // This variable keeps track of the unique generic genes
  public static $seq_required; // Tripal bulk loader doesn't like features that don't have a sequence. This option does not output features without a sequence
  public static $only_EST_GSS = true; // Read through each Genbank record and test if the file only contains EST/GSS records

  /**
   * Execute Genbank Parser
   * @param string $input
   * @param string $out_prefix
   * @param string $header
   * @return number, 1) succeed. 2) succeeed but only containing EST or GSS results
   */
  public function main($input, $out_prefix, $header = NULL) {
    $header = $header != NULL ? $header : new Header();
    // Read input Genbank files
    $gf = new GenbankFile($input);

    // Create output files
    $out_locus = fopen($out_prefix . $header->files->locus, 'w') or die("can't open file\n");
    $out_gene = fopen($out_prefix . $header->files->gene, 'w') or die("can't open file\n");
    $out_mrna = fopen($out_prefix . $header->files->mrna, 'w') or die("can't open file\n");
    $out_cds = fopen($out_prefix . $header->files->cds, 'w') or die("can't open file\n");
    $out_protein = fopen($out_prefix . $header->files->protein, 'w') or die("can't open file\n");
    $out_misc = fopen($out_prefix . $header->files->misc, 'w') or die("can't open file\n");
    $out_generic = fopen($out_prefix . $header->files->generic, 'w') or die("can't open file\n");

    // Write headers to the output
    DelimitedFile::writeRow($out_locus, $header->locus);
    DelimitedFile::writeRow($out_gene, $header->gene);
    DelimitedFile::writeRow($out_mrna, $header->mrna);
    DelimitedFile::writeRow($out_cds, $header->cds);
    DelimitedFile::writeRow($out_protein, $header->protein);
    DelimitedFile::writeRow($out_generic, $header->generic);
    DelimitedFile::writeRow($out_misc, $header->misc);

    // Parse Genbank file record by record. Output parsed results while reading the input.
    $this->parseRecord($gf, $out_locus, $out_gene, $out_mrna, $out_cds, $out_protein, $out_misc);

    // Create generic genes
    $generic = new GenericGene();
    $generic->writeGenericGenes($out_generic);

    // Close ouptut files
    fclose($out_locus);
    fclose($out_gene);
    fclose($out_mrna);
    fclose($out_cds);
    fclose($out_protein);
    fclose($out_generic);
    fclose($out_misc);

    // If the file only contains EST/GSS, delete all files except for the locus
    if (GBParser::$only_EST_GSS) {
      unlink($out_prefix . $header->files->gene);
      unlink($out_prefix . $header->files->mrna);
      unlink($out_prefix . $header->files->cds);
      unlink($out_prefix . $header->files->protein);
      unlink($out_prefix . $header->files->generic);
      unlink($out_prefix . $header->files->misc);
      return 2;
    }
    else {
      return 1;
    }
  }

  private function parseRecord($gf, $out_locus, $out_gene, $out_mrna, $out_cds, $out_protein, $out_misc) {
    // Parsers
    $locus_parser = new LocusParser();
    $src_parser = new SourceParser();
    $gene_parser = new GeneParser();
    $transc_parser = new TranscriptParser();
    $misc_parser = new MiscFeatureParser();
    $ref_parser = new ReferenceParser();

    $record = null;
    while (($record = $gf->readNextGenbank()) != null) {
      // Parse 'LOCUS' along with REFERENCE and SOURCE
      $locus_parser->parseLocus($record, $ref_parser, $src_parser, $out_locus);

      $keywords = $record->getKeyword();
      if ($keywords == "EST." || $keywords == "GSS.") {
        continue;
      }
      // Parse 'gene' in FEATURES. Note: the uniquename we created still doesn't generate a unique gene list. Be aware when writing a chado loader.
      if ($record->getMoleculeType() != 'mRNA') { // Do not create a gene if the record is a 'mRNA'
        $gene_parser->parseGenes($record, $out_gene);
      }

      // Parse 'mRNA' and 'CDS' in FEATURES. Note: the uniquename of mRNA/CDS/UTRs we created generates a unique list for each feature type
      $transc_parser->parseTranscripts($record, $out_mrna, $out_cds, $out_protein);

      // Parse Misc Features (e.g. exon/intron/tRNA/rRNA). Note: the uniquename for misc. feature is unique across the parsing genbank file as well.
      $feature_keys = $record->getFeatureTypes();
      foreach ($feature_keys as $feature_key) {
        if ($feature_key != 'source' && $feature_key != 'gene' && $feature_key != 'mRNA' && $feature_key != 'CDS' && $feature_key != '5\'UTR' && $feature_key != '3\'UTR') {
          $misc_parser->parseFeatures($record, $feature_key, $out_misc);
        }
      }
    }
  }
}
