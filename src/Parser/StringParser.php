<?php

namespace Drupal\tripal_genbank_parser\Parser;

use Drupal\tripal_genbank_parser\Util\Str;

/**
 * StringParser is a class with methods to parses a subject string
 */
class StringParser {

  /**
   * A method to tokenize a string by using a set of keywords.
   *
   * As a result, an associated array will be returned in which
   * the key is the keyword and the value is the text associated
   * with the keyword. By default, the continuous spaces in the
   * value will be replaced by just one space. However, if a
   * keyword ends with '[!APPEND]', the value of said keyword
   * will not be converted.
   */
  public static function parseKeywords($subject, $keywords) {
    // Find the position of each keywords
    $start = - 1;
    $stop = - 1;
    $position = - 1;
    $key = "";
    $results = array ();
    $switchOff = false;
    foreach ($keywords as $k) {
      $append = true;
      if (Str::endsWith($k, "[!APPEND]")) {
        $k = preg_replace("/\[\!APPEND\]/", "", $k);
        $append = false;
      }
      $offset = $start < 0 ? 0 : $start;
      $position = strpos($subject, $k, $offset);
      if ($position >= 0 && $position !== false) {
        $stop = $position;
        if ($start != - 1 && $stop > $start) {
          if ($switchOff) {
            $results [$key] = trim(substr($subject, $start, ($stop - $start)));
            $switchOff = false;
          }
          else {
            $results [$key] = trim(preg_replace("/\s+/", " ", substr($subject, $start, ($stop - $start))));
          }
          $key = "";
        }
        $key = $k;
        $start = $position + strlen($k);
      }
      if (! $append) {
        $switchOff = true;
      }
    }

    // Everything else to the last belongs to the last keyword
    if ($key != "" && $start < strlen($subject) - 1) {
      $value = "";
      if ($switchOff) {
        $value = trim(substr($subject, $start));
      }
      else {
        $value = trim(preg_replace("/\s+/", " ", substr($subject, $start)));
      }
      if ($value != "") {
        $results [$key] = $value;
      }
    }
    return $results;
  }
}