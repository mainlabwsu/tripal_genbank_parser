<?php

//require_once 'fetcher/NCBIFetcher.php';

/**
 * Tripal Genbank Parser admin page.
 */
function tripal_genbank_parser_admin ($form_state = NULL) {

  $keyword = "";
  $days = "";
  $db = 'nucleotide';
  $chado = FALSE;
  if (array_key_exists('values', $form_state)) {
    $keyword = $form_state['values']['keyword'];
    $days = $form_state['values']['days'];
    $db = $form_state['values']['db'];
    $chado = $form_state['values']['chado'];
  }
  
  $form = array();
    
  $form['parser'] = array(
    '#type' => 'fieldset',
    '#title' => t('NCBI Entrez E-utilities Settings'),
      '#collapsible' => TRUE,
  );
  $form['parser']['description'] = array(
    '#type' => 'item',
    '#value' => 
      t("Download and parse genbank records using a keyword. 
          Use the 'Count' button to estimate the number of sequences and time for download. 
          Click on the 'Submit' button to submit a Tripal job for downloading and parsing the Genbank records."),
    '#weight' => 0,
  );
  $form['parser']['keyword'] = array(
    '#type' => 'textfield',
    '#title' => t("Keyword"),
    '#description' => t("The keyword to search the NCBI nucleotide database. e.g. 'Malus x domestica [ORGN]'"),
    '#default_value' => $keyword,
    '#maxlength' => 1024,
    '#weight' => 1,
  );
  $form['parser']['days'] = array(
      '#type' => 'textfield',
      '#title' => t("Days since record modified"),
      '#description' => t("Limit the search to include records that have been added no more than this many days before today."),
      '#default_value' => $days,
      '#size' =>6,
      '#weight' => 2,
  );
  $form['parser']['db'] = array(
      '#type' => 'select',
      '#title' => t("Entrez database"),
      '#description' => t("The NCBI Entrez database to search against, currently supporting 'Nucleotde', 'EST', or 'GSS' databases."),
      '#options' => array('nucleotide' => 'Nucleotide', 'nucest' => 'EST', 'nucgss' => 'GSS'),
      '#default_value' => $db,
      '#weight' => 3,
  );
  $form['parser']['chado'] = array(
      '#type' => 'checkbox',
      '#title' => t("Load into the Chado database"),
      '#description' => t("Check this box to load the parsing results into the Chado database. You can load the data later using the 'Chado Loader' below."),
      '#default_value' => $chado,
      '#weight' => 4,
  );
  $form['parser']['count'] = array(
    '#type' => 'submit',
    '#value' => t('Count'),
    '#weight' => 5,
  );
  $form['parser']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 6,
  );
  $form['parser']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#weight' => 7,
  );
  
  $form['loader'] = array(
      '#type' => 'fieldset',
      '#title' => t('Chado Loader'),
      '#collapsible' => TRUE
  );
  $sql = 
  "SELECT job_id, job_name, arguments
    FROM tripal_jobs 
    WHERE modulename = 'tripal_genbank_parser' 
    AND callback = 'tripal_genbank_parser_job' 
    AND status = 'Completed'";
  $results = \Drupal::database()->query($sql);
  $list = array();
  while ($job = $results->fetchObject()) {
    $args = isset($job->arguments) ? unserialize($job->arguments) : array();
    if ($args[3] == 0) {
      $list[$job->job_id] = "Job ID " . $job->job_id . ". " . $job->job_name;
    }
  }
  if (count($list) != 0) {
    $form ['loader'] ['parsedjob'] = array (
        '#type' => 'select',
        '#title' => t("Finished Jobs"),
        '#description' => t("Select a finished parsing job to load into Chado. A qualified job for loading should have a 'Completed' status and was not already loaded into Chado."),
        '#options' => $list,
        '#weight' => 8 
    );
    $form ['loader'] ['submit'] = array (
        '#type' => 'submit',
        '#value' => t('Load'),
        '#weight' => 9 
    );
  }
  else {
    $form['loader']['description'] = array(
        '#type' => 'item',
        '#value' =>
        t("No previous parsing job available for loading. A qualified job for loading should have a 'Completed' status and was not already loaded into Chado."),
        '#weight' => 10,
    );
  }
  return $form;
}

/**
 * Validate the Genbank Parser adminitraion form.
 */
function  tripal_genbank_parser_admin_validate($form, &$form_state) {
  
  $form_state['rebuild'] =TRUE; // Rebuild the form so the user input such as 'Keyword', 'Days', or 'NCBI Database' can be remembered
  
  $keyword = $form_state['values']['keyword'];
  if (!trim($keyword) && $form_state['values']['op'] != t('Reset') && $form_state['values']['op'] != t('Load')) {
    form_set_error('', t('Keyword is required.'));
    return;
  }
  $days = $form_state['values']['days'];
  if ($days && !is_numeric($days) && $form_state['values']['op'] != t('Reset') && $form_state['values']['op'] != t('Load')) {
    form_set_error('', t('Days should be numeric.'));
    return;
  }
  $db = $form_state['values']['db'];
  $chado = $form_state['values']['chado'];
  if ($form_state['values']['op'] == t('Count') && $keyword) {
    module_load_include('php', 'tripal_genbank_parser', 'api/fetcher/NCBIFetcher');
    $ncbifetcher = new NCBIFetcher();
    $result = $ncbifetcher->eSearch($keyword, NULL, $days, NULL, $db);
    \Drupal::messenger()->addMessage($result['count'] . ' record(s) match the search criteria.');
  }
  else if ($form_state['values']['op'] == t('Submit')) {
    // Use the date to create a sub direcotry for holding downloaded data.
    $dir = tripal_get_files_dir('tripal_genbank_parser') . '/' . date('Y-m-d');
    file_prepare_directory($dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    // Use the time as the output file prefix
    $prefix = date('H-i-s');
    // Submit a job
    $desc = substr($keyword, 0, 200);
    $job_name = "Parse Genbank: db=$db; keyword=$desc";
    if ($days) {
      $job_name .= "; days=$days";
    }
    if ($chado) {
      $job_name .= "; chado=load"; 
    }
    $arguments = array($keyword, $days, $db, $chado, $dir, $prefix);
    $user = \Drupal::currentUser();
    tripal_add_job($job_name, 'tripal_genbank_parser', 'tripal_genbank_parser_job', $arguments, $user->uid);
  }
  else if ($form_state['values']['op'] == t('Reset')) {
    $form_state['rebuild'] =FALSE;
  }
  else if ($form_state['values']['op'] == t('Load')) {
    $user = \Drupal::currentUser();
    $job_id = $form_state['values']['parsedjob'];
    $job_name = "Load Genbank results from Job ID: $job_id";
    $sql = "SELECT arguments FROM tripal_jobs WHERE job_id = :job_id";
    $result = \Drupal::database()->query($sql, array(':job_id' => $job_id))->fetchObject();
    $args = unserialize($result->arguments);
    $db = $args[2];
    $file = $args[4] . '/' . $db . '_' . $args[5];
    $arguments = array($file, $db, $job_id);
    tripal_add_job($job_name, 'tripal_genbank_parser', 'tripal_genbank_parser_job_load', $arguments, $user->uid);
  }
}