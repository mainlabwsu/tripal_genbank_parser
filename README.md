# Tripal Genbank Parser
Tripal Genbank Parser is a tool to download data from NCBI and populate the Chado database. It connects to the NCBI nucleotide database and retrieves Genbank flat files using the Entraz E-Utilities. The flat files are then parsed into tab-delimited files containing locus, generic gene, gene, mRNA, CDS, protein, and other miscllaneous features such as tRNA/rRNA/UTR etc. It also has a loader to load the tab-delimited files into the Chado database.

Tripal Genbank Parser is created by Main Bioinformatics Lab (Main Lab) at Washington State University. Information about the Main Lab can be found at: https://www.bioinfo.wsu.edu

## Screenshot
![Image](screenshot.png "screenshot")

## Requirement
 - Drupal 10.x or 11.x
 - PostgreSQL (Drupal in 'public' schema whereas Chado in 'chado' schema)

## Version
4.3.0

## Download
The Tripal Genbank Parser module can be downloaded from GitLab:

https://gitlab.com/mainlabwsu/tripal_genbank_parser

## Installation
1. After downloading the module, extract it into your site's module directory

2. Enable it from the Drupal Extend adminitrative interface [https://your.site/admin/modules].

3. Go to the module entry point to start. [https://your.site/admin/tripal_genbank_parser]

## Usage
1. Input keywords for finding data of interests to download from NCBI

2. Use the 'Days since record modified' field to limit the date returned.

3. Click on the 'Count' button to confirm the size of data is appropriate. If it is, click on the 'Submit' button to submit a gnebank parser job. After the job is created, it'll be listed at the bottom of the page for future management.

## Job Pipeline
The following drush commands can be used to start the job pipeline.

1. Start the oldest job with 'Created' status from the job queue
```
drush trp-gbjob
```

2. Load data for a Tripal GenbankParser Job. (You'll be able to find the job_id from the job list at the bottom of the module entry page)

```
drush trp-gbload --job_id=<job_id>
```

## Problems/Suggestions
Tripal Genbank Parser module is still under active development. For questions or bug
report, please contact the developers at the Main Bioinformatics Lab by emailing to:
dev@bioinfo.wsu.edu
